summary.write("\\begin{table*}[t] \n\\begin{center} \n\label{tab:summary} \n\caption{ \n A representative summary of all our simulations at 2 spatial resolutions. $\Lambda_i$s are the respective tidal deformabilites of the individual stars, $t_{merger}$ is the time of merger, $t_{BH}$ is the time after merger when the system collapses to a black hole, $t_{coll}$ is the time when the gravitational radiation from the 2,2 mode shuts down and $t_{end}$ is the final time of the simulation. $f_{peak}$ represents the dominant postmerger peak of the 2,2 mode, $\Delta f_{peak}$ represents the difference between these postmerger peak frequencies between the 2 EOS and $\Delta$FT represents the numerical uncertainty in the Fourier Transform. $M_{Disk}^{end}$ represents the disk mass at the end of our simulation.\n} \n} \n\scalebox{1.15}{ \n \\begin{tabular}{c c c c c c c c c c c c c c c c} \n \hline\hline \n EOS & $M_1$ & $M_2$ & M & q & Resolution$ & $\Lambda_1$ & $\Lambda_2$ & $t_{\\text{merger}}$ & $t_{\\text{BH}}$ & $t_{\\text{coll}}$ & $t_{\\text{end}}$ & $f_{\\text{peak}}$  & $M_{\\text{disk}} ^{\\text{end}}$ & $M_{\\text{ej}}$ & $\langle v_{\infty}\\rangle$ & $\langle Y_e \\rangle_{ej}$ & $\langle s \\rangle_{ej}$ \\\ \n &   & $[M_{\odot}]$ &$[M_{\odot}]$ & ([m]) &  &  & [ms] & [KHz]   & $[10^{-2} M_{\odot}]$ &$[10^{-2} M_{\odot}]$ & $[c]$ & & $[k_B]$   \\\ \n \hline \n \hline \n")



    # Total Ejected Mass
    M_ej_array = np.loadtxt(dpath + "/outflow_0/total_flux.dat")
    M_ej_avg = M_ej_array[-1,2]
    # Average Asymptotic velocity of the ejecta
    v_inf, M =  np.loadtxt(dpath + "/outflow_0/hist_vel_inf.dat", usecols=(0, 1), unpack=True)
        v_inf_avg = np.sum(v_inf * M)/(M_ej_avg)
        # Average Ye of the ejecta
        Ye, M =  np.loadtxt(dpath + "/outflow_0/hist_ye.dat", usecols=(0, 1), unpack=True)
        Ye_avg = np.sum(Ye * M)/(M_ej_avg)
        # Average entropy of the ejecta
        s, M =  np.loadtxt(dpath + "/outflow_0/hist_entropy.dat", usecols=(0, 1), unpack=True)
        s_avg = np.sum(s * M)/(M_ej_avg)
