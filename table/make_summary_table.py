# The python script generates a tex file corresponding to a summary of our simulations

import numpy as np
import math
import sys
import os

import models_SR
import models_LR

import units as ut
import utils as util

########################### CONVERSION FACTORS ########################
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # g/cc
ulength = ut.conv_length(ut.cactus, ut.metric, 1)/1.e3 #km
utime = ut.conv_time(ut.cactus, ut.cgs, 1) *1.e3 # ms
#######################################################################

summary = open("summary_table.tex", "w")

######################### TEX file template ###########################
summary.write("\\begin{table*}[t] \n\\begin{center} \n\label{tab:summary} \n\caption{ \n A representative summary of all our simulations at 2 spatial resolutions. $\Lambda_i$s are the respective tidal deformabilites of the individual stars, $t_{merger}$ is the time of merger, $t_{BH}$ is the time after merger when the system collapses to a black hole, $t_{coll}$ is the time when the gravitational radiation from the 2,2 mode shuts down and $t_{end}$ is the final time of the simulation. $f_{peak}$ represents the dominant postmerger peak of the 2,2 mode, $\Delta f_{peak}$ represents the difference between these postmerger peak frequencies between the 2 EOS and $\Delta$FT represents the numerical uncertainty in the Fourier Transform. $M_{Disk}^{end}$ represents the disk mass at the end of our simulation.\n} \n} \n\scalebox{1.0}{ \n \\begin{tabular}{c c c c c c c c c c c c c c c c} \n \hline\hline \n EOS & $M_1$ & $M_2$ & M & q & Resolution & $\Lambda_1$ & $\Lambda_2$ & $t_{\\text{merger}}$ & $t_{\\text{BH}}$ & $t_{\\text{coll}}$ & $t_{\\text{end}}$ & $f_{\\text{peak}}$  & \Delta f_{\\text{peak}} & \Delta FT & $M_{\\text{disk}} ^{\\text{end}}$ \\\ \n & [M_{\odot}] & [M_{\odot}] & [M_{\odot}] & & & & & [ms] & [$t_{merg} + ms$] & [$t_{merg} + ms$] & [$t_{merg} + ms$] & [KHz] & [KHz] & [KHz] & $[10^{-2} M_{\odot}]$   \\\ \n \hline \n \hline \n")
######################### Loading SR data from simulations ##################
for m1m2 in models_SR.masses:
    for eos in models_SR.EOSs:
        mod = models_SR.get_model(eos, m1m2)
        # Binary Masses
        M1 = float(mod["M1"])
        M2 = float(mod["M2"])
        M = M1 + M2 # Total Mass
        if(M1>M2): # Ensuring q>1 ---Mass Ratio
            q = M1/M2
        else:
            q = M2/M1
        cpath = "../data/SR/"
        dpath = "../data/SR/" + models_SR.get_name(eos, m1m2)
        t, h_p, h_c = np.loadtxt(dpath + "/waveforms/strain_l2_m2.dat", usecols=(0, 1, 2), unpack = True)
        h_22 = h_p + 1j * h_c # Strain as a complex number with 2 polarizations.
        A_22 = np.abs(h_22) # Amplitude of the 2, 2 strain
        # Characteristic times: Read all in cactus units. Convert later to t - tmerger in ms.
        # Merger time
        t_merger = t[np.argmax(A_22)]
        t_merger_retarded = util.get_retarded_time(t_merger, M1 + M2) * utime # in ms
        t_merger *= utime # in ms
        #t_end
        t_end = t[-1] * utime - t_merger
        tt, alp = np.loadtxt(dpath + "/collated/alp.minimum.asc", usecols=(1, 2), unpack = True) # t and tt are same but still for consistency.
        # f_peak for (2,2) mode
        f_peak = float(open(dpath + "/waveforms/f_peak_22.dat", "r").readline())/1.e3 # in KHz
        # Terminal Disk Mass
        Mdisk_array = np.loadtxt(dpath + "/Disk/disk_mass_r3.asc", usecols=(2), unpack=True)
        Mdisk = Mdisk_array[-1]*1.e2
        
        if (np.logical_and(alp.min()<0.30, eos=="BLQ")):  # When an AH forms for all BLQ cases
            t_BH = (tt[alp<0.30][0] * utime - t_merger_retarded) # The time when the lapse dips below 0.30 i.e. when the AH forms. Units of t_merger_retarded+ms
            mask = t[A_22 <= 0.005 * A_22.max()]
            mask_1 = mask[(mask*utime - t_merger) >= t_BH] # Criteria for GW shutting down is that amplitude dips down below 0.5%  of the maximum amplitude & lies ahead of t_BH
            t_coll = (mask_1[0]*utime - t_merger) # Collapse time in units of t_merger + ms
            D_FT = (1./(t_coll)) # The uncertainty in the Fourier transform ~ Reciprocal length of the postmerger signal in KHz
            D_f_peak = (float(open(cpath + models_SR.get_name("BLQ", m1m2) + "/waveforms/f_peak_22.dat", "r").readline())/1.e3) - (float(open(cpath + models_SR.get_name("BLh", m1m2) + "/waveforms/f_peak_22.dat", "r").readline())/1.e3) # Shift in postmereger peaks. Remember to manually impose (in the tex file) \xmark where difference is zero and for the case of BLh/BLQ_M145-145_M0_SR.
        
            summary.write(str(eos) + " & " + str(M1) + " & " + str(M2) + " & " + str(round(M, 2)) + " & " + str(round(q, 2)) + " & " + "SR" + " & " + " " + " & " + " " + " & " + str(round(t_merger, 2)) + " & " + str(round(t_BH, 2)) + " & " + str(round(t_coll, 2)) + " & " + str(round(t_end, 2)) + " & " + str(round(f_peak, 2)) + " & " + str(round(D_f_peak, 2)) + " & " + str(round(D_FT, 2)) + " & " + str(round(Mdisk, 2)) +  " \\\\")
        elif (np.logical_and(alp.min()<0.30, eos=="BLh")):
        
            t_BH = (tt[alp<0.30][0] * utime - t_merger_retarded) # The time when the lapse dips below 0.30 i.e. when the AH forms. Units of t_merger_retarded+ms
            mask = t[A_22 <= 0.005 * A_22.max()]
            mask_1 = mask[(mask*utime - t_merger) >= t_BH] # Criteria for GW shutting down is that amplitude dips down below 0.5%  of the maximum amplitude & lies ahead of t_BH
            t_coll = (mask_1[0]*utime - t_merger) # Collapse time in units of t_merger + ms
        
            summary.write(str(eos) + " & " + str(M1) + " & " + str(M2) + " & " + str(round(M, 2)) + " & " + str(round(q, 2)) + " & " + "SR" + " & " + " " + " & " + " " + " & " + str(round(t_merger, 2)) + " & " + str(round(t_BH, 2)) + " & " + str(round(t_coll, 2)) + " & " + str(round(t_end, 2)) + " & " + str(round(f_peak, 2)) + " & " + " " + " & " + " " + " & " + str(round(Mdisk, 2)) +  " \\\\")
        
        else: # Minimum lapse is > 0.30 i.e. The remnant is a massive NS exhibitted in all light and intermediate BLh systems
        # Here there is no BH formation or collapse so t_BH > t_end and t_coll > t_end
        
            summary.write(str(eos) + " & " + str(M1) + " & " + str(M2) + " & " + str(round(M, 2)) + " & " + str(round(q, 2)) + " & " + "SR" + " & " + " " + " & " + " " + " & " + str(round(t_merger, 2)) + " & " + "$>$" + str(round(t_end, 2)) + " & " + "$>$" + str(round(t_end, 2)) + " & " + str(round(t_end, 2)) + " & " + str(round(f_peak, 2)) + " & " + " " + " & " + " " + " & " + str(round(Mdisk, 2)) +  " \\\\")
        
        summary.write("\n")
    summary.write("\hline \n")
summary.write("\hline \n")
######################### Loading LR data from simulations ##################


summary.write("\end{tabular} \n}%scalebox \n\end{center} \n\end{table*} \n")

summary.close()



