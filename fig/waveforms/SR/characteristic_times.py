# The script computes Characteristic times in a BNS simulation viz. t_merger, t_BH, t_coll, t_end
#   t_merger = The time when the amplitude (sqrt(h_p**2 + h_c**2)) of 2, 2 mode hits a maximum. => Reference time scale.
#   t_BH = Time when an apparent horizon forms i.e. when the minimum lapse dips below 0.3
#   t_coll = Time when the GW radiation from 2, 2 mode shuts off. Defined as the time greater than t_BH when GW amplitude dips to <=0.5% of maximum amplitude and is more than t_BH.
#   t_end = The terminal time of the simulation.


# The real motivation behind constructing this table is to make characterisic times handy for the table constructing script.



#!/usr/bin/env python


import matplotlib
import matplotlib.pyplot as plt
import models
import numpy as np

import units as ut
import utils as util # to get the retarded time

# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)
##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

###############################################################

for m1m2 in models.masses:
    for eos in models.EOSs:
        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        dpath = "../../../data/SR/" + models.get_name(eos,m1m2)
        t, h_p, h_c = np.loadtxt(dpath + "/waveforms/strain_l2_m2.dat", usecols=(0, 1, 2), unpack = True)
        
        h_22 = h_p + 1j * h_c # Strain as a complex number with 2 polarizations
        A_22 = np.abs(h_22) # Amplitude of the 2, 2 strain
        t_merger = t[np.argmax(A_22)] # Merger time is defined as the maximum of the 2,2 strain's amplitude.
        t_end = t[-1] # let us keep all times in cactus units. We can convert them accordingly.
        
        tt, alp = np.loadtxt(dpath + "/collated/alp.minimum.asc", usecols=(1,2),
                unpack=True)
        t_merger_retarded = util.get_retarded_time(t_merger, float(mod["M1"])+float(mod["M2"]))
        
        # This conversion of t/M is essential to map the time evolution of a hydro qty to that of a GW qty which is delayed

        if (np.logical_and(alp.min()<0.30, eos=="BLQ")):  # When an AH forms for all BLQ cases
            t_BH = tt[alp<0.30][0] # The time when the lapse dips below 0.30 i.e. when the AH forms
            mask = t[A_22 <= 0.005 * A_22.max()]
            mask_1 = mask[mask >= t_BH] # Criteria for GW shutting down is that amplitude dips down below 0.5%  of the maximum amplitude
            t_coll = mask_1[0]
        
            plt.plot((t - t_merger)*utime, h_p, 'r',(t - t_merger)*utime, A_22, 'k--')
            plt.axvline(x = 0) # Merger time
            plt.axvline(x = (t_BH - t_merger_retarded)*utime)
            plt.axvline(x = (t_coll - t_merger)*utime)
            plt.axvline(x = (t_end - t_merger)*utime)
            plt.savefig(sys.argv[0].replace(".py","") + str(eos) + "_m" + m1m2 + ".pdf")
            plt.close()
        
        elif (np.logical_and(alp.min()<0.30, eos=="BLh")): # When an AH forms for all BLh cases (Heavy + Prompt Collapse)
            t_BH = tt[alp<0.30][0] # The time when the lapse dips below 0.30 i.e. when the AH forms
            mask = t[A_22 <= 0.005 * A_22.max()]
            mask_1 = mask[mask >= t_BH] # Criteria for GW shutting down is that amplitude dips down below 0.5%  of the maximum amplitude
            t_coll = mask_1[0]
            
            plt.plot((t - t_merger)*utime, h_p, 'r',(t - t_merger)*utime, A_22, 'k--')
            plt.axvline(x = 0) # Merger time
            plt.axvline(x = (t_BH - t_merger_retarded)*utime)
            plt.axvline(x = (t_coll - t_merger)*utime)
            plt.axvline(x = (t_end - t_merger)*utime)
            plt.savefig(sys.argv[0].replace(".py","") + str(eos) + "_m" + m1m2 + ".pdf")
            plt.close()
        
        else: # Minimum lapse is > 0.30 i.e. The remnant is a massive NS exhibitted in all light and intermediate BLh systems
            # Here there is no BH formation or collapse so t_BH > t_end and t_coll > t_end
            plt.plot((t - t_merger)*utime, h_p, 'r',(t - t_merger)*utime, A_22, 'k--')
            plt.axvline(x = 0) # Merger time
            plt.axvline(x = (t_end - t_merger)*utime)
            plt.savefig(sys.argv[0].replace(".py","") + str(eos) + "_m" + m1m2 + ".pdf")
            plt.close()


