#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, FixedLocator, NullFormatter, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
from utils import get_retarded_time
import sys

#mev_to_g = 1e6*1.782662e-33
#cm_to_fm = 1e-13
rho_nuc = 2.70 * 1.e14

color_cycle = itertools.cycle([plotsettings.color_list[1],
    plotsettings.color_list[3]])
dashes_cycle = itertools.cycle([{}, {"dashes": (4,2)}, {"dashes": (1,1)}])

fig, ax = plt.subplots(nrows=2, figsize=(5.0, 3.0))
fig.subplots_adjust(left=0.1, bottom=0.15, top=0.97, right=0.98, hspace=0)

han, lab = [], []
#for m1m2 in ["135135", "140140", "150150"]:
for m1m2 in models.masses:
    dashes = next(dashes_cycle)
    for idx, eos in enumerate(models.EOSs):
        lstyle = next(plotsettings.style_cycle)
        color = next(color_cycle)
        lstyle.update(dashes)

        mod = models.get_model(eos,m1m2)
        M1 = float(mod["M1"])
        M2 = float(mod["M2"])
        M = M1 + M2
        MADM = float(mod["MADM"])
        Mb = float(mod["Mb"])
        nu = (M1*M2)/(M**2)

        dpath = "../../../data/SR/" + models.get_name(eos,m1m2)
        t, EGW = np.loadtxt(dpath + "/waveforms/EJ.dat", usecols=(0,2),
                unpack=True)
        tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
        tt = 1e3*ut.conv_time(ut.cactus, ut.cgs, t - tmerger)
        EGW += M - MADM

        ax[1].plot(tt, 1e2*EGW, color=color, **lstyle)
        if idx == 0:
            han.append(plt.Line2D([], [], color="k", **lstyle))
            lab.append(r"${}\, M_\odot + {}\, M_\odot$".format(
                mod["M1"], mod["M2"]))

        t, rho = np.loadtxt(dpath + "/collated/rho.maximum.asc", usecols=(1,2),
                unpack=True)
        tmerger = get_retarded_time(tmerger, float(mod["M1"])+float(mod["M2"]))
        # This scaling to t/M is necessaary as you are comparing a hydro qty rho's evolution to a GW qty Eb's evolution
        # which is supposed to be delayed. This is also the reason we use get retarded time.
        tt = 1e3*ut.conv_time(ut.cactus, ut.cgs, t - tmerger)
        rho = ut.conv_dens(ut.cactus, ut.cgs, rho) / rho_nuc
        #nb = rho/((models.mbs[eos]*mev_to_g)/cm_to_fm**3)
        imax = np.argmax(rho) # Time/index when rho reaches a max

        ax[0].plot(tt[:imax], rho[:imax], color=color, **lstyle)
for i in range(2):
    ax[i].set_xlim(xmin=-5, xmax=10)
    ax[i].xaxis.set_major_locator(MultipleLocator(5))
    ax[i].xaxis.set_minor_locator(MultipleLocator(1))
    ax[i].get_yaxis().set_label_coords(-0.06, 0.5)
ax[0].xaxis.set_major_formatter(NullFormatter())
ax[1].set_xlabel(r"$t - t_{\rm mrg}\ [\mathrm{ms}]$")

ax[0].set_ylim(ymin=1.5, ymax=7.0)
#ax[0].set_ylim(ymin=1.5, ymax=6.5)
ax[0].yaxis.set_major_locator(MultipleLocator(1.0))
ax[0].yaxis.set_minor_locator(MultipleLocator(0.2))
ax[0].set_ylabel(r"$\rho_{\rm{max}}/\rho_{\rm {nuc}}$")

line_c = plt.Line2D([], [], color=plotsettings.color_list[1])
line_d = plt.Line2D([], [], color=plotsettings.color_list[3], lw=0.5)
ax[0].add_artist(ax[0].legend([line_c,line_d], [r"$\rm{BLh}$",r"$\rm{BLQ}$"],
    loc="upper left", ncol=1))

ax[1].set_ylim(ymin=1.5, ymax=10.0)
#ax[1].set_ylim(ymin=1.5, ymax=11.0)
ax[1].yaxis.set_major_locator(MultipleLocator(2))
ax[1].yaxis.set_minor_locator(MultipleLocator(0.5))
ax[1].set_ylabel(r"$-E_b\ [10^{-2}\, M_\odot c^2]$")

#han, lab = ax[1].get_legend_handles_labels()
#ax[1].add_artist(ax[1].legend([han[0]], [lab[0]], loc="upper left"))
#ax[1].add_artist(ax[1].legend(han[1:], lab[1:], loc="lower right", ncol=2))
ax[1].legend(han, lab, loc="lower right", ncol=1)

plt.savefig(sys.argv[0].replace(".py","") + ".pdf")
