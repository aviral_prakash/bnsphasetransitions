import itertools
import matplotlib
import matplotlib.pyplot as plt

def axes():
    return plt.axes([0.15, 0.2, 0.95-0.15, 0.95-0.2])

color_list  = matplotlib.rcParams["axes.prop_cycle"].by_key()["color"]
color_cycle = itertools.cycle(color_list)

#### Keep iterating colors in color_list derived from the rc file for each successive plot.

style_list = [
    {"linestyle": "-", "linewidth": 1.0},
    {"linestyle": "-", "linewidth": 0.5}, # "dashes": (3,3)}
]
style_cycle = itertools.cycle(style_list)

#### Keep Iterating the plot style from style_list defined here for each successive plot.

#### Good plotsettings when a properties from a pair of EOSs are to be compared


