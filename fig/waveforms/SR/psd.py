#!/usr/bin/env python

import itertools
from math import sqrt
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut

from scidata.windows import exponential as window

from scipy.fftpack import fft
from scipy.fftpack.helper import fftfreq
from scipy.interpolate import InterpolatedUnivariateSpline as interp1d
import sys

Msun_s  = ut.conv_time(ut.cactus, ut.cgs, 1) # Mass of sun in seconds
Msun_cm = ut.conv_length(ut.cactus, ut.cgs, 1) # Mass of sun in cm
Mpc_cm = 3.086e+24 # cm # Conversion fator from Mpc to cm

# Analysis parameters
Dist    = 40        # Dostance to the source in Mpc
fmin    = 1024       # Hz
fmax    = 4096       # Hz
smpl    = 16384      # Hz ---Sampling / Nyquist Frequency
tmin    = -1.0/1024  # s
tmax    = 20.0/1024  # s
twin    = 1.0/8192   # s

def get_ligo_noise(f):
  fl, S = np.loadtxt("../../../data/ZERO_DET_high_P.txt", usecols=(0,1),
      unpack=True)
  S = np.interp(f, fl, S)
  return S**2

def get_et_noise(f):
  fe, S = np.loadtxt("../../../data/ET_D.txt", usecols=(0,3), unpack=True)
  S = np.interp(f, fe, S)
  return S**2

def make_window(t, t0, t1, twin):    # exponential window. I used a Tukey window hmm 
  """
  Creates a window

  * t          : array of times
  * t0, t1     : time window
  * win        : windowing amplitude
  """
  return window((t - t0)/(t1 - t0), twin/(t1 - t0))

dt = 1.0/smpl
t = dt*np.arange(0, 4096)
f = fftfreq(t.shape[0], dt)
idx = f >= 0

S_ligo = get_ligo_noise(f)
S_et = get_et_noise(f)

for m1m2 in models.masses:
    ax = plt.axes([0.2, 0.2, 0.95-0.2, 0.95-0.2])
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        dpath = "../../../data/SR/" + models.get_name(eos,m1m2)
        tw, hp = np.loadtxt(dpath + "/waveforms/strain_l2_m2.dat", usecols=(0,1),
                unpack=True)
        tw = ut.conv_time(ut.cactus, ut.cgs, tw)
        tmrg = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
        tmrg = ut.conv_time(ut.cactus, ut.cgs, tmrg)

        # Resample and zero-pad the data
        tevl = np.arange(0, tw.max() + dt, dt)
        hp  = interp1d(tw, hp, k=3, ext=1)(tevl)*(Msun_cm/(Mpc_cm*Dist))
        hpp = np.zeros_like(t)
        hpp[:hp.shape[0]] = hp[:]
        del hp

        # Compute PSD
        win = make_window(t, tmrg + tmin - twin, tmrg + tmax + twin, twin)
        # Assume optimal orientation (multiply for Y[-2,2,2](phi=0, mu=1))
        heff = sqrt(0.4)*np.abs(fft(hpp*win))*dt

        ax.plot(f[idx], np.sqrt(f[idx])*heff[idx], color=next(color_cycle),
                label=models.get_eos_name(eos), **lstyle)

    ax.plot(f[idx], np.sqrt(S_ligo[idx]), color="black", label="Adv. LIGO")
    ax.plot(f[idx], np.sqrt(S_et[idx]), color="black", dashes=(3,3), label="ET")

    ax.set_xlabel(r"$f\, [{\rm Hz}]$")
    ax.set_xlim(xmin=1000, xmax=4000)
    ax.xaxis.set_major_locator(MultipleLocator(500))
    ax.xaxis.set_minor_locator(MultipleLocator(100))

    ax.set_ylabel(r'$\sqrt{f}\ |h_+(f)|,\ \sqrt{S_n}$ ${\rm [Hz^{-1/2}]}$')
    ax.set_yscale("log")
    ax.set_ylim(ymin=1e-25, ymax=1e-22)

    ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2),
            xy=(1100, 1.5e-25))
    ax.annotate(r"$D = 40\, {\rm Mpc}$", xy=(1000,1.1e-23))

    plt.legend(loc="upper left", ncol=2)

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
