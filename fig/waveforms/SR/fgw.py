#!/usr/bin/env python

import itertools
from math import pi
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
from utils import get_advanced_time

# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)

for m1m2 in models.masses:
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        dpath = "../../../data/SR/" + models.get_name(eos,m1m2)

        t, rho = np.loadtxt(dpath + "/collated/rho.maximum.asc", usecols=(1,2), unpack=True)
        tcoll = t[np.argmax(rho)]
        tcoll = get_advanced_time(tcoll, float(M1) + float(M2))

        t, omega = np.loadtxt(dpath + "/waveforms/strain_l2_m2.dat", usecols=(0,4),
                unpack=True)
        tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
        tt = 1e3*ut.conv_time(ut.cactus, ut.cgs, t - tmerger)
        omega = ut.conv_frequency(ut.cactus, ut.cgs, omega)/1e3

        idx = t < tcoll
        ax.plot(tt[idx], omega[idx]/(2*pi), color=next(color_cycle),
                label=models.get_eos_name(eos), **lstyle)
    ax.set_xlim(xmin=-5, xmax=9)
    ax.xaxis.set_major_locator(MultipleLocator(5))
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$t - t_{\rm mrg}\ [\rm{ms}]$")

    ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2),
            xy=(0.0, 0.5), fontsize="large")

    ax.set_ylim(ymin=0, ymax=6)
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax.set_ylabel(r"$f_{\rm GW}\ [\rm{kHz}]$")

    plt.legend(loc="upper right", ncol=2)

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
