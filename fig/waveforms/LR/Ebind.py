#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)


##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

# Rescaling is necessary because data from the simulations is in geometrical units where G = c = 1 and
# everything is expressed in Solar mass ! time in t/M
MSun = 1.9889 * 10**(30) # 1 solar mass in kg
MTSun = 4.92549 * 10**(-6) # The conversion factor for one solar mass to seconds *)
#M = 2.7 # Total mass of the binary in solar mass units
MLSun = 1.47670 * 10**(3) # The conversion factor for one solar mass to length in m
mpc = 3.24078 * 10**(-17) # One meter to parsec
D = 40 # Distance to the source in Mpc

###############################################################

for m1m2 in models.masses:
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1 = float(mod["M1"])
        M2 = float(mod["M2"])
        M = M1 + M2
        MADM = float(mod["MADM"])
        Mb = float(mod["Mb"])
        nu = (M1*M2)/(M**2)
        
        dpath = "../../../data/LR/" + models.get_name(eos,m1m2)
        t, E_GW = np.loadtxt(dpath + "/waveforms/EJ.dat", usecols=(0,2),
                unpack=True)
        tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline()) * utime
        tt = (t * utime) - tmerger # Converting time - t_merg to ms
        E_GW += M - MADM

        ax.plot(tt, 1.e2*E_GW, color=next(color_cycle), label=models.get_eos_name(eos),
                **lstyle)
    #ax.set_xlim(xmin=-5, xmax=7)
    ax.xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major => after every 5 units
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$t - t_{\rm merg}\ [\rm{ms}]$")

    #ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2), xy=(1.0, -8.00), fontsize="medium")

    #ax.set_ylim(ymin=-10, ymax=10)
    ax.yaxis.set_major_locator(MultipleLocator(4))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    ax.set_ylabel(r"$-E_b\ [10^{-2}\, M_\odot c^2]$")

    plt.legend(loc="upper right", ncol=2)

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
