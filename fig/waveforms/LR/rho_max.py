#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
import utils as util # to get the retarded time


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)


##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

###############################################################

for m1m2 in models.masses:
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        dpath = "../../../data/LR/" + models.get_name(eos,m1m2)
        t, rho = np.loadtxt(dpath + "/collated/rho.maximum.asc", usecols=(1,2),
                unpack=True)
        tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
        tmerger = util.get_retarded_time(tmerger, float(mod["M1"])+float(mod["M2"])) * utime
        # This conversion is essential to map the time evolution of a hydro qty to that of a GW qty which is delayed
        tt = (t * utime) - tmerger # Converting time - t_merg to ms
        rho = rho * (udens / rho_nuc)

        ax.plot(tt, rho, color=next(color_cycle), label=models.get_eos_name(eos),
                **lstyle)
    ax.set_xlim(xmin=-5, xmax=12.5)
    ax.xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major tick  => after every 5 units
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$t - t_{\rm merg}\ [\rm{ms}]$")

    ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2),
            xy=(3.6, 2.5), fontsize="medium")

    ax.set_ylim(ymin=2, ymax=6)
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_minor_locator(MultipleLocator(0.5))
    ax.set_ylabel(r"$\rho_{\rm{max}} / \rho_{\rm{nuc}}$")

    plt.legend(loc="upper right", ncol=2)

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
