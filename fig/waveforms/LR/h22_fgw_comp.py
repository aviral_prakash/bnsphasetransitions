#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
import utils as util


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)

##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

# Rescaling is necessary because data from the simulations is in geometrical units where G = c = 1 and
# everything is expressed in Solar mass ! time in t/M
MSun = 1.9889 * 10**(30) # 1 solar mass in kg
MTSun = 4.92549 * 10**(-6) # The conversion factor for one solar mass to seconds *)
#M = 2.7 # Total mass of the binary in solar mass units
MLSun = 1.47670 * 10**(3) # The conversion factor for one solar mass to length in m
mpc = 3.24078 * 10**(-17) # One meter to parsec
D = 40 # Distance to the source in Mpc

###############################################################

for m1m2 in models.masses:
    
    fig, ax = plt.subplots(nrows=2, figsize=(5.2, 3.5))
    fig.subplots_adjust(left=0.1, bottom=0.15, top=0.97, right=0.98, hspace=0)
    
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])

    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]
        
        dpath0 = "../../../data/LR/" + models.get_name(eos,m1m2)
        t0, h = np.loadtxt(dpath0 + "/waveforms/strain_l2_m2.dat", usecols=(0,1),
                unpack=True)
        tmerger0 = float(open(dpath0 + "/waveforms/tmerger.dat", "r").readline()) * utime
        tt0 = (t0 * utime) - tmerger0 # Converting time - t_merg to ms
        h = h * ((MLSun) / (D*(10.**6.)/mpc)) * 10**22 # Remember 100 'M'pc was the source's distance
    
        # Plotting strain
        ax[0].plot(tt0, h, color=next(color_cycle), label=models.get_eos_name(eos),
               **lstyle)


    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)
        dpath = "../../../data/LR/" + models.get_name(eos, m1m2)
        t, rho = np.loadtxt(dpath + "/collated/rho.maximum.asc", usecols=(1,2), unpack=True)
        tcoll = t[np.argmax(rho)]
        tcoll = util.get_advanced_time(tcoll, float(M1) + float(M2))
        # This scaling to t/M is necessary as you are comparing a hydro qty rho's evolution to a GW qty
        # fGW's evolution which is advanced. 
        dpath1 = "../../../data/LR/" + models.get_name(eos, m1m2)
        t1, omega = np.loadtxt(dpath1 + "/waveforms/strain_l2_m2.dat", usecols=(0, 4), unpack = True)
        tmerger1 = float(open(dpath1 + "/waveforms/tmerger.dat", "r").readline()) * utime
        tt1 = (t1 * utime) - tmerger1 # Converting time - t_merg to ms
        omega = ut.conv_frequency(ut.cactus, ut.cgs, omega)/1e3 # in KHz
        
        
        # Plotting fGW
        idx = t<tcoll
        ax[1].plot(tt1[idx], omega[idx]/(2*np.pi), color=next(color_cycle),
                label=models.get_eos_name(eos), **lstyle)
    
    
    ax[0].set_xlim(xmin=-5, xmax=20)
    ax[0].xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major => after every 5 units
    ax[0].xaxis.set_minor_locator(MultipleLocator(1))
    #ax[0].set_xlabel(r"$t - t_{\rm merg}\ [\rm{ms}]$")

    plt.legend(loc="upper right", ncol=2)

    ax[1].set_xlim(xmin=-5, xmax=20)
    ax[1].xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major => after every 5 units
    ax[1].xaxis.set_minor_locator(MultipleLocator(1))
    ax[1].set_xlabel(r"$t - t_{\rm merg}\ [\rm{ms}]$")

    #ax[0].annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2),
    #     xy=(6.5, -6.25), fontsize="large")

    ax[0].set_ylim(ymin=-11, ymax=12)
    ax[0].yaxis.set_major_locator(MultipleLocator(4))
    #ax[0].yaxis.set_minor_locator(MultipleLocator(1))
    ax[0].set_ylabel(r"$10^{22} \times h_+ \; (D = 40 \;\rm{Mpc})$")

    ax[1].set_ylim(ymin=0, ymax=4.9)
    ax[1].yaxis.set_major_locator(MultipleLocator(1))
    #ax[0].yaxis.set_minor_locator(MultipleLocator(1))
    ax[1].set_ylabel(r"$f_{GW}\;[\rm{KHz}]$")
    ax[1].annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2),
         xy=(10.0, 1.0), fontsize="large")

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
