#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
import utils as util # to get the retarded time


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)


##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

###############################################################

for m1m2 in models.masses:    # As these are the LR runs done with tracers
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
        
    mod = models.get_model("BLQ",m1m2)
        
    M1, M2 = mod["M1"], mod["M2"]
    ############ Plotting tracer density ##########
    lstyle = next(plotsettings.style_cycle)
    dpath = "../../../data/SR/" + models.get_name("BLQ",m1m2)
    t, rho_tracer = np.loadtxt(dpath + "/collated/tracers_id10_rho.asc", usecols=(0,1), unpack=True)
    # t here is in ms because of the script and rho is in x 10**15
    tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
    tmerger = util.get_retarded_time(tmerger, float(mod["M1"])+float(mod["M2"])) * utime
    tt = t - tmerger # Converting time - t_merg to ms
    rho_tracer = (rho_tracer/10) - 0.3
    ax.plot(tt, rho_tracer, color=next(color_cycle), label=r"$10^{-1} \times \rho / \rho_{\rm{nuc}} - 0.3$",
                **lstyle)
    ################ Plotting tracer Yq #############
    lstyle = next(plotsettings.style_cycle)
    dpath = "../../../data/SR/" + models.get_name("BLQ",m1m2)
    t, Yq_tracer = np.loadtxt(dpath + "/collated/tracers_id10_Yq.asc", usecols=(0,1), unpack=True)
    tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
    tmerger = util.get_retarded_time(tmerger, float(mod["M1"])+float(mod["M2"])) * utime
    tt = t - tmerger # Converting time - t_merg to ms
    ax.plot(tt, Yq_tracer, color=next(color_cycle), label=r"$Yq$",
                                    **lstyle)
    ax.set_xlim(xmin=-1, xmax=4.0)
    ax.xaxis.set_major_locator(MultipleLocator(2)) # Location/frequency of major tick  => after every 5 units
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$t - t_{\rm merg}\ [\rm{ms}]$")

    ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2), xy=(-3.0, 0.35), fontsize="medium")

    ax.set_ylim(ymin=-0.1, ymax=0.3)
    #ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_minor_locator(MultipleLocator(0.1))
    ax.set_ylabel(r"$\rm{Dimensionless \; Units}$")

    plt.legend(loc="upper left")

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
