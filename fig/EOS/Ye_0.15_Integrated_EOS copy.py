################### Computes the integrated EOS (Yq) over all Ye's in the range [0, 0.15] as Ye in the core does not go beyond this point ########################
#### Yq = Yq(rho, T, Ye). However to 1st order, Yq will depend on rho. Not even at T. Even at low T you will get Yq at sufficiently high density. 

import numpy as np
import h5py as h5
from scipy.interpolate import RegularGridInterpolator

import os
import sys
sys.path.append(os.curdir)

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
                           'figure.figsize'            : [5., 4.0],
                           'image.cmap'                : 'gist_stern',
                           'image.interpolation'       : 'none',
                           })

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pylab import *
import tqdm # Managing / Monitoring time

rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

####################### Plot Options ########################
mpl.rcParams['figure.figsize'] = 8, 6
mpl.rcParams['font.size'] = 12.0
mpl.rcParams['xtick.labelsize'] = 12.0
mpl.rcParams['ytick.labelsize'] = 12.0
mpl.rcParams['xtick.direction'] = 'inout'
mpl.rcParams['ytick.direction'] = 'inout'
mpl.rcParams['xtick.top'] = 'False'
mpl.rcParams['ytick.right'] = 'False'

################# Module to calculate a constant Ye slice of the EOS ##############

data = h5.File("comp_BLh_180_0.35.hdf5")
comp = data["composition"]

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

Yq = 1. - comp[3] ## Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))

#####################################################################################

################ Module to integrate Yq over all values of Ye in [0, 0.15]. Just applying limits to this integral  ############
### Weighted mean : <Yq> (rho, T) =   \int Yq(rho, T, Ye) dYe
#################################    -------------------------
#################################     \int dYe
mask = Ye_new<0.16 # mask of Ye_new array containing values less than 0.16 o.e. from 0.01 to 0.15
Ye_sub = Ye_new[mask] # A sub array of Ye_new values containing values from 0.01 to 0.15

Yq_avg = np.zeros((T_new.size * log_rho_new.size))
Yq_avg = Yq_avg.reshape((T_new.size, log_rho_new.size)) ####Because <Yq> will be free of Ye degrees of freedom

# Defining the Ye integrated Yq ####
for i in range(0, T_new.size):
    for j in range(0, log_rho_new.size):
        Yq_avg[i, j] = 0.
        for k in range(0, Ye_sub.size):
            Yq_avg[i, j] += Yq[i, k, j] * Ye_sub[k] # Summing over all values of Ye_sub
        Yq_avg[i, j] /= Ye_sub.sum()

Yq_avg_interp = RegularGridInterpolator((T_new, log_rho_new), Yq_avg, method = 'linear', bounds_error=False, fill_value=None)
def Yq_avg_calc(x, y):
    return(np.maximum(0.0, Yq_avg_interp([y, x])[0]))


################################ Plotting Ye weighted BLQ #########################################
N_res = 1000
log_rho_refined = np.linspace(12., log_rho_new.max(), N_res)
T_refined = np.linspace(T_new.min(), T_new.max(), N_res)
Yq_avg_refined = np.zeros((N_res * N_res))
Yq_avg_refined = Yq_avg_refined.reshape((N_res, N_res))

for i in tqdm.tqdm(range(0, log_rho_refined.size)):  # rds
    for j in range(0, T_refined.size):
        Yq_avg_refined[j, i] = Yq_avg_calc(log_rho_refined[i], T_refined[j])

print("\n Plotting the BLQ EOS averaged over Ye")
plt.pcolormesh((10**log_rho_refined/rho_nuc), T_refined, Yq_avg_refined[:,:], cmap = 'gist_stern')
plt.colorbar(label=r'$\frac{\int Y_q(\rho,\;T,\;Y_e)\;dY_e}{\int dY_e}$')
plt.xlabel(r'$\rho / \rho_{nuc}$')
plt.ylabel(r'$T \;(MeV)$')
plt.title(r'$\rm{BLQ\;EOS\;averaged\; over\; Y_e\;\epsilon[0,0.15]}$')
plt.savefig('avg_Yq_Ye_0.15.png', dpi = 1200)
plt.close()

