#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, FixedLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
import sys

#TOV_M = np.array([1.3, 1.4])
TOV_M = np.array([1.02, 1.259, 1.30, 1.3325, 1.365, 1.40, 1.50, 1.60, 1.856])

fig = plt.figure(figsize=(2.5, 3.0))
ax = fig.add_axes([0.2, 0.15, 0.95-0.17, 0.97-0.15])

blue = plotsettings.color_list[1]
red = plotsettings.color_list[3]

uamu_mev = 931.494061
mev_to_g = 1e6*1.782662e-33
cm_to_fm = 1e-13
Msun = ut.metric.solar_mass

M_BLh, R_BLh = np.loadtxt("../../../data/TOVs/TOVs/BL/BLh.txt",
        usecols=(4,2), unpack=True)
M_BLQ, R_BLQ = np.loadtxt("../../../data/TOVs/TOVs/BLQ/BLQ.txt",
        usecols=(4,2), unpack=True)
#n_DD2 = rho_DD2/(uamu_mev*mev_to_g)*cm_to_fm**3
#n_BHB = rho_BHB/(uamu_mev*mev_to_g)*cm_to_fm**3

TOV_R_BLh = np.interp(TOV_M, M_BLh, R_BLh)
TOV_R_BLQ = np.interp(TOV_M, M_BLQ, R_BLQ)

ax.plot(R_BLh, M_BLh, color=blue, label=r"$\mathrm{BLh}$")
ax.scatter(TOV_R_BLh, TOV_M, color=(0,0,0,0), edgecolors=blue, marker="o")
ax.plot(R_BLQ, M_BLQ, "-", linewidth=0.5, color=red, label="$\mathrm{BLQ}$")
ax.scatter(TOV_R_BLQ, TOV_M, color=(0,0,0,0), edgecolors=red, marker="s",
        linewidth=0.5)
#ax.plot([0.5, 6], [1.6, 1.6], color='black', lw=0.5)

#ax.annotate(r"$M = 1.6\ M_\odot$", xy=(3.15, 1.65))

ax.set_xlabel(r"$R\;[\rm{km}]$")
ax.set_xlim(10.5, 14.2)
ax.xaxis.set_major_locator(MultipleLocator(1))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))

ax.set_ylabel(r"$M\ [M_\odot]$")
ax.set_ylim(1.0, 2.2)
ax.yaxis.set_major_locator(MultipleLocator(0.5))
#ax.yaxis.set_minor_locator(MultipleLocator(0.1))

ax.legend(loc="upper right")

#axins = fig.add_axes([0.58, 0.32, 0.35, 0.3])
#axins.plot(R_BLh, M_BLh, color=blue, label=r"$BLh$")
#axins.scatter(TOV_R_BLh, TOV_M, color=(0,0,0,0), edgecolors=blue,
#        marker="o")
#axins.plot(R_BLQ, M_BLQ, "-", linewidth=0.5, color=red, label="BLQ")
#axins.scatter(TOV_R_BLQ, TOV_M, color=(0,0,0,0), edgecolors=red,
#        marker="s", linewidth=0.5)
#axins.set_xlim(10, 14)
#axins.set_ylim(1.0, 2.1)
#axins.xaxis.set_major_locator(MultipleLocator(5))
#axins.xaxis.set_minor_locator(MultipleLocator(1))
#axins.yaxis.set_major_locator(MultipleLocator(0.5))
#axins.yaxis.set_minor_locator(MultipleLocator(1))
plt.savefig(sys.argv[0].replace(".py", "") + ".pdf")
