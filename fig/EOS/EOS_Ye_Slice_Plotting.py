############### Computes a constant Ye slice of the Qark Equation of State ########################

import numpy as np
import h5py as h5
from scipy.interpolate import RegularGridInterpolator

import os
import sys
sys.path.append(os.curdir)

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
                           'figure.figsize'            : [5., 4.0],
                           'image.cmap'                : 'gist_stern',
                           'image.interpolation'       : 'none',
                           })


import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pylab import *
import tqdm

rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc


########################################################################################
data = h5.File("comp_BLh_180_0.35.hdf5")
comp = data["composition"]

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

Yq = 1. - comp[3] ## Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))

#####################################################################################
print("\n Calculating a constant Ye slice of EOS:")

N_res = 1000
log_rho_refined = np.linspace(12., log_rho_new.max(), N_res)
T_refined = np.linspace(T_new.min(), T_new.max(), N_res)
Ye0 = 0.01 # The constant Ye value for slice

Yq_refined = np.zeros((N_res * N_res))
Yq_refined = Yq_refined.reshape((N_res, N_res))

for i in tqdm.tqdm(range(0, log_rho_refined.size)):  # rds
    for j in range(0, T_refined.size):
        Yq_refined[j, i] = Yq_calc(log_rho_refined[i], T_refined[j], Ye0)


################################ Plotting the slice #########################################

print("\n Plotting the constant Ye = "+str(Ye0)+" slice")

plt.pcolormesh((10**log_rho_refined/rho_nuc), T_refined, Yq_refined[:,:], cmap = 'gist_stern')
plt.colorbar(label=r'$\rm{Y_q}$')
plt.title(r"$\rm{Ye = 0.01}$")
plt.xlabel(r'$\rho / \rho_{\rm{nuc}}$')
plt.ylabel(r'$T \;[\rm{MeV}]$')
plt.savefig('Ye='+str(Ye0)+'.png', dpi = 1200)
plt.close()

