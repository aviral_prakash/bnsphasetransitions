#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, FixedLocator, NullFormatter, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut
from utils import get_retarded_time
import sys

#mev_to_g = 1e6*1.782662e-33
#cm_to_fm = 1e-13
rho_nuc = 2.70 * 1.e14

color_cycle = itertools.cycle([plotsettings.color_list[0],
    plotsettings.color_list[0], plotsettings.color_list[1], plotsettings.color_list[1], plotsettings.color_list[2], plotsettings.color_list[2], plotsettings.color_list[3], plotsettings.color_list[3], plotsettings.color_list[4], plotsettings.color_list[4], plotsettings.color_list[5], plotsettings.color_list[5], plotsettings.color_list[6], plotsettings.color_list[6] ])
#dashes_cycle = itertools.cycle([{}, {"dashes": (4,2)}, {"dashes": (1,1)}])

fig, ax = plt.subplots()
#fig.subplots_adjust(left=0.1, bottom=0.15, top=0.97, right=0.98, hspace=0)

han, lab = [], []
#for m1m2 in ["135135", "140140", "150150"]:
for m1m2 in models.masses:
    #dashes = next(dashes_cycle)
    for idx, eos in enumerate(models.EOSs):
        lstyle = next(plotsettings.style_cycle)
        color = next(color_cycle)
        #lstyle.update(dashes)

        mod = models.get_model(eos,m1m2)
        M1 = float(mod["M1"])
        M2 = float(mod["M2"])
        M = M1 + M2
        MADM = float(mod["MADM"])
        Mb = float(mod["Mb"])
        nu = (M1*M2)/(M**2) # Symmetric mass ratio

        dpath = "../../../data/LR/" + models.get_name(eos,m1m2)
        t, Disk_Mass = np.loadtxt(dpath + "/Disk/disk_mass_r3.asc", usecols=(1,2),
                unpack=True)
        tmerger = float(open(dpath + "/waveforms/tmerger.dat", "r").readline())
        tmerger = get_retarded_time(tmerger, float(mod["M1"])+float(mod["M2"]))
        tt = 1e3*ut.conv_time(ut.cactus, ut.cgs, t - tmerger) # time in ms
        
        ax.plot(tt, Disk_Mass, color=color, **lstyle)
        if idx == 0:
            han.append(plt.Line2D([], [], color="k", **lstyle))
            lab.append(r"${}\, M_\odot + {}\, M_\odot$".format(
                mod["M1"], mod["M2"]))


ax.set_xlim(xmin=-5, xmax=30)
ax.xaxis.set_major_locator(MultipleLocator(5))
ax.xaxis.set_minor_locator(MultipleLocator(1))
#ax.get_yaxis().set_label_coords(-0.06, 0.5)
#ax.xaxis.set_major_formatter(NullFormatter())
ax.set_xlabel(r"$t - t_{\rm merg}\ [\mathrm{ms}]$")

#ax.set_ylim(ymin=1.5, ymax=7.0)
#ax[0].set_ylim(ymin=1.5, ymax=6.5)
ax.yaxis.set_major_locator(MultipleLocator(0.05))
#ax.yaxis.set_minor_locator(MultipleLocator(0.2))
ax.set_ylabel(r"$\rm{Disk\;Mass\;[M_{\odot}]}$")

line_c = plt.Line2D([], [], color = plotsettings.color_list[4], ls = '-')
line_d = plt.Line2D([], [], color = plotsettings.color_list[4], ls = '-.')
ax.add_artist(ax.legend([line_c,line_d], [r"$\rm{BLh}$",r"$\rm{BLQ}$"],
    loc="upper left", ncol=1))

line_1 = plt.Line2D([], [], color = plotsettings.color_list[0], ls = '-')
line_2 = plt.Line2D([], [], color = plotsettings.color_list[1], ls = '-')
line_3 = plt.Line2D([], [], color = plotsettings.color_list[2], ls = '-')
line_4 = plt.Line2D([], [], color = plotsettings.color_list[3], ls = '-')
line_5 = plt.Line2D([], [], color = plotsettings.color_list[4], ls = '-')
line_6 = plt.Line2D([], [], color = plotsettings.color_list[5], ls = '-')
line_7 = plt.Line2D([], [], color = plotsettings.color_list[6], ls = '-')
ax.add_artist(ax.legend([line_1,line_2, line_3, line_4, line_5, line_6, line_7], [r"$\rm{1.30 \;M_{\odot}-1.30\;M_{\odot}}$", r"$\rm{1.3325 \;M_{\odot}-1.3325\;M_{\odot}}$", r"$\rm{1.365 \;M_{\odot}-1.365\;M_{\odot}}$", r"$\rm{1.40 \;M_{\odot}-1.40\;M_{\odot}}$", r"$\rm{1.45 \;M_{\odot}-1.45\;M_{\odot}}$" , r"$\rm{1.259 \;M_{\odot}-1.482\;M_{\odot}}$", r"$\rm{1.020 \;M_{\odot}-1.856\;M_{\odot}}$"],
                        loc="lower right", ncol=1))

#han, lab = ax.get_legend_handles_labels()
#ax.add_artist(ax.legend([han], [lab], loc="upper left"))
#ax.add_artist(ax.legend(han, lab, loc="lower right", ncol=2))
#ax.legend(han, lab, loc="lower right", ncol=1)

plt.savefig(sys.argv[0].replace(".py","") + ".pdf")
