import matplotlib
matplotlib.rcParams.update({
    'figure.figsize'            : [4, 3.6]
})

from collections import OrderedDict
import math
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.legend_handler import HandlerBase
from matplotlib.lines import Line2D
from matplotlib.ticker import AutoMinorLocator, NullFormatter, MultipleLocator
import numpy as np
import plotsettings

# Units
utime   = 0.0049264150318814    # ms
ulength = 1.4769020715358734    # km

# Models DB
models = OrderedDict([
    ("RP7.5_LK_H0.15_v2", ('PRGn',   r'$\texttt{LK\_RP7.5}$',   10.0)),
    ("RP10_LK_H0.15_v2",  ('coolwarm', r'$\texttt{LK\_RP10}$',  30.0)),
    ("RP15_LK_H0.15",     ('BrBG',     r'$\texttt{LK\_RP15}$',  5.0)),
])

class MyLineCollectionHandler(HandlerBase):
    def __init__(self, **kw):
        HandlerBase.__init__(self, **kw)
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height

        x = np.linspace(x0, x0 + width)
        t = np.linspace(0, 1)
        y = np.ones_like(x) * (y0 + 0.25*height)
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc1 = LineCollection(segments, cmap=orig_handle.get_cmap(),
                norm=plt.Normalize(-1, 1),
                transform=handlebox.get_transform())
        lc1.set_array(t - 1)
        handlebox.add_artist(lc1)

        y = np.ones_like(x) * (y0 + 0.75*height)
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc2 = LineCollection(segments, cmap=orig_handle.get_cmap(),
                norm=plt.Normalize(-1, 1),
                transform=handlebox.get_transform())
        lc2.set_array(-t + 1)
        handlebox.add_artist(lc2)
        return [lc1, lc2]

handles = {}
labels  = []

ax = plt.axes([0.12, 0.12, 0.98-0.12, 0.98-0.12])
for mod, val in models.iteritems():
    try:
        data = np.loadtxt("../../data/{0}".format(mod) +
                "/collated/bnstrackergen::bns_positions..asc.gz",
                usecols=(8, 12, 13, 20, 21), unpack=True)
    except:
        data = np.loadtxt("../../data/{0}".format(mod) +
                "/collated/bnstracker::bns_positions..asc.gz",
                usecols=(8, 12, 13, 16, 17), unpack=True)
    data[0] *= utime
    for i in range(1, len(data)):
        data[i] *= ulength

    time = data[0].copy()
    time[time >= val[2]] = val[2]
    time /= val[2]

    points = np.array([data[1], data[2]]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = LineCollection(segments, cmap=plt.get_cmap(val[0]),
            norm=plt.Normalize(-1, 1))
    lc.set_array(time - 1.0)
    ax.add_collection(lc)

    points = np.array([data[3], data[4]]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = LineCollection(segments, cmap=plt.get_cmap(val[0]),
            norm=plt.Normalize(-1, 1))
    lc.set_array(- time + 1)
    ax.add_collection(lc)

    handles[lc] = MyLineCollectionHandler()
    labels.append(val[1])

ax.legend(handles.keys(), labels, ncol=1, loc='lower right', handler_map=handles)

ax.set_xlim(xmin=-120, xmax=120)
ax.set_ylim(ymin=-120, ymax=120)
ax.xaxis.set_major_locator(MultipleLocator(50))
ax.xaxis.set_minor_locator(AutoMinorLocator(5))
ax.yaxis.set_major_locator(MultipleLocator(50))
ax.yaxis.set_minor_locator(AutoMinorLocator(5))

ax.set_xlabel(r"$x\ [\mathrm{km}]$")
ax.set_ylabel(r"$y\ [\mathrm{km}]$")

ax.set_aspect('equal', 'box-forced')
#ax.legend(loc='best')

plt.savefig("trajectories.pdf")
