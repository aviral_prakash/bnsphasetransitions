import csv

def get_eos_name(eos): # Returns the LaTeX equivalent of the name of EOS to be inserted in figures
    if eos == "BLh":
        return r"BLh"
    else:
        return r"BLQ"

def get_name(eos, masses): # Returns the name of the simulation with correct nomenclature
        return "{}_M{}_M0_SR".format(eos, masses)


def get_masses(m1m2):
    return{
        "130-130"    : (r"1.30", r"1.30"),
        "13325-13325": (r"1.3325", r"1.3325"),
        "13651365"   : (r"1.365", r"1.365"),
        "140-140"    : (r"1.40", r"1.40"),
        "145-145"    : (r"1.45", r"1.45"),
        "1475-1475"  : (r"1.475", r"1.475"),
        "150-150"    : (r"1.50", r"1.50"),
        "160-160"    : (r"1.60", r"1.60"),
        "12591482"   : (r"1.259", r"1.482"),
        "10201856"   : (r"1.020", r"1.856"),
        "140-120"    : (r"1.40", r"1.20"),
    }[m1m2]

def get_label(eos, m1, m2):
    return r"{} ${} - {}$".format(get_eos_name(eos), m1, m2)

def get_mass_label(m1, m2):
    return r"${} - {}$".format(m1, m2)

def get_mass_label_long(m1, m2):
    return r"${}\, M_\odot - {}\, M_\odot$".format(m1, m2)

EOSs = ["BLh", "BLQ"]
masses = ["130-130", "13325-13325", "13651365", "140-140","145-145", "1475-1475", "12591482", "10201856", "140-120"]
models = {get_name(eos, m): None for eos in EOSs for m in masses} # Until now the object models
# only stores the simulation names which are also the 1st column of the csv file

# To be completed later:

#MTOVs = {"BHBlp": 2.11, "DD2": 2.42}

#mbs = {"BHBlp": 922.293228, "DD2": 922.293228}

fieldnames = {}
with open("SUMMARY.csv","r") as csvfile:
    reader = csv.DictReader(csvfile) # reader is a csv type object
    for row in reader:
        if models.has_key(row["name"]): # Checking if models has an entry that matches with the entris of the name column in the csv file
            models[row["name"]] = row # Appending/ loading the data in all the columns of that particular simulation's row to the model's object
    fieldnames = reader.fieldnames # Loading the name of the fields. Entries of all column's in the 0th row

def get_model(eos, masses):
    return models[get_name(eos, masses)] # Returns all the information about a specific model


#### We need to add more data to the csv file to make it more professional 
