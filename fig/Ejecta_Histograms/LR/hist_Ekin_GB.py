#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import numpy as np
import plotsettings
import units as ut


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)


##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

# Rescaling is necessary because data from the simulations is in geometrical units where G = c = 1 and
# everything is expressed in Solar mass ! time in t/M
MSun = 1.9889 * 10**(30) # 1 solar mass in kg
MTSun = 4.92549 * 10**(-6) # The conversion factor for one solar mass to seconds *)
#M = 2.7 # Total mass of the binary in solar mass units
MLSun = 1.47670 * 10**(3) # The conversion factor for one solar mass to length in m
mpc = 3.24078 * 10**(-17) # One meter to parsec
D = 40 # Distance to the source in Mpc

###############################################################

for m1m2 in models.masses:
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        dpath = "../../../data/LR/" + models.get_name(eos,m1m2)
        GB, KE = np.loadtxt(dpath + "/outflow_0/hist_Ekin_GB.dat", usecols=(0,1),
                unpack=True)
        ax.step(GB, KE/np.sum(KE), where='mid', color=next(color_cycle), label=models.get_eos_name(eos), **lstyle)

    ax.set_xlim(xmin=0, xmax=2.0)
    #ax.xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major => after every 5 units
    #ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$\Gamma\;\beta$")

    #ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2), xy=(0.5, 1.e-2), fontsize="medium")

    ax.set_ylabel(r"$E_{\rm{kin}}/E_{\rm{total}}$")
    ax.set_yscale("log")
    #ax.set_ylim(5e-5)
    #ax.yaxis.set_major_locator(MultipleLocator(4))
    #ax.yaxis.set_minor_locator(MultipleLocator(1))


    plt.legend(loc="upper right")

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
