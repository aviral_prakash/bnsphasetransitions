#!/usr/bin/env python

import itertools
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import models
import h5py
import numpy as np
import plotsettings
import units as ut


# Following are for using the rc parameters from the matplotlibrc stylesheet
import os
import sys
sys.path.append(os.curdir)

##################################Methods for Nucleosynthetic Yields#########################################

Anrm_range = [180, 200] # The range of heavy elements over which we shall be normalizing
# instead of using tabulated_nucsyn.h5. So that we portray relative yields.

## A is mass number = Total number of protons and neutrons
## Y is nucleosynthetic Yield


def get_solar_abundances():
    Asol, Ysol = np.loadtxt("../../../data/solar_r.dat", usecols=(0,1), unpack=True)
    Ared = np.arange(int(Asol.max()) + 1)
    Yred = np.zeros(int(Asol.max()) + 1)
    for i in range(Yred.shape[0]):
        Yred[i] = np.sum(Ysol[Asol == i])
    Yred /= np.sum(Yred[(Ared >= Anrm_range[0]) &
                        (Ared <= Anrm_range[1])])
    return Ared, Yred

def get_yields(eos, m1m2):
    yields_path= "../../../data/SR/" + models.get_name(eos, m1m2)
    dfile = h5py.File(yields_path + "/outflow_0/yields.h5")
    As = np.array(dfile["A"], dtype=np.int)
    Ys = np.array(dfile["Y_final"])
    Aredu = np.arange(As.max() + 1) # A reduced
    Yredu = np.zeros(As.max() + 1) # Y reduced
    for i in xrange(Yredu.shape[0]):
        Yredu[i] = np.sum(Ys[As==i])
    Ynrm = 0
    for i in range(Anrm_range[0], Anrm_range[1]):
        Ynrm += np.sum(Ys[As==i])
    Yredu /= Ynrm
    return Aredu, Yredu

##############################   UNITS   ################################

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in cgs(g/cc)
rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

# Rescaling is necessary because data from the simulations is in geometrical units where G = c = 1 and
# everything is expressed in Solar mass ! time in t/M
MSun = 1.9889 * 10**(30) # 1 solar mass in kg
MTSun = 4.92549 * 10**(-6) # The conversion factor for one solar mass to seconds *)
#M = 2.7 # Total mass of the binary in solar mass units
MLSun = 1.47670 * 10**(3) # The conversion factor for one solar mass to length in m
mpc = 3.24078 * 10**(-17) # One meter to parsec
D = 40 # Distance to the source in Mpc

###############################################################

for m1m2 in models.masses:
    ax = plotsettings.axes()
    color_cycle = itertools.cycle([plotsettings.color_list[1],
        plotsettings.color_list[3]])
        
    Asol, Ysol = get_solar_abundances()
    ax.plot(Asol,Ysol, '.', color = 'green', label ='Solar', zorder = 100)
    for eos in models.EOSs:
        lstyle = next(plotsettings.style_cycle)

        mod = models.get_model(eos,m1m2)

        M1, M2 = mod["M1"], mod["M2"]

        As, Ys = get_yields(eos, m1m2)
        ax.step(As, Ys, where = 'mid', color = next(color_cycle), label=models.get_eos_name(eos), **lstyle)

    ax.set_xlim(xmin=50, xmax=210)
    #ax.xaxis.set_major_locator(MultipleLocator(5)) # Location/frequency of major => after every 5 units
    #ax.xaxis.set_minor_locator(MultipleLocator(1))
    ax.set_xlabel(r"$\rm{A}$")

    ax.annotate(r"${}\, M_\odot - {}\, M_\odot$".format(M1,M2), xy=(120, 1.e-4), fontsize="medium")

    ax.set_ylabel(r"$\rm{Relative\;Abundance\; Y}$")
    ax.set_yscale("log")
    ax.set_ylim(2e-5, 2)
    #ax.yaxis.set_major_locator(MultipleLocator(4))
    #ax.yaxis.set_minor_locator(MultipleLocator(1))


    plt.legend(loc="upper right")

    plt.savefig(sys.argv[0].replace(".py","") + "_m" + m1m2 + ".pdf")
    plt.close()
