#!/usr/bin/env python

import os
import sys
sys.path.append(os.curdir)

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
                           'figure.figsize'            : [4.0, 3.5],
                           'image.cmap'                : 'CMRmap',
                           'image.interpolation'       : 'none',
                           })
from mpl_toolkits.axes_grid1.inset_locator import inset_axes # Beautification


from glob import glob
import h5py as h
from matplotlib.colors import Normalize, LogNorm
from matplotlib.ticker import LogFormatter
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import scidata.carpet.grid as carpet
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator
import units as ut
import utils as util
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from scipy.interpolate import RegularGridInterpolator

#############################    Yq Calculation      ###################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data["composition"]

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

# The above was done by exploring Domenico's Table

Yq = 1. - comp[3] ## Domenico's convention is opposite to this in his table
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):       # This isn't exactly required
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))


#####################################################################################################

# Plotting grid
L = 30 # Half the spatial length scale (xmax, ymax)

################## Units ################
ulength = ut.conv_length(ut.cactus, ut.metric, 1)/1e3 # in km
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)
utime = ut.conv_time(ut.cactus, ut.cgs, 1) * 1.e3 # time in ms
rho_nuc = 2.70 * 1e14 # Nuclear density in g/cc
#########################################


# Read the data

dset_rho = h5.dataset(glob("../../../data/SR/BLQ_M13325-13325_M0_SR/output-000?/data/rho.xy.h5"))
dset_T = h5.dataset(glob("../../../data/SR/BLQ_M13325-13325_M0_SR/output-000?/data/temperature.xy.h5"))
dset_Ye = h5.dataset(glob("../../../data/SR/BLQ_M13325-13325_M0_SR/output-000?/data/Y_e.xy.h5"))

im = []
for it in (dset_rho.iterations):
    xmin, xmax = -L, L
    ymin, ymax = -L, L
    dx = dy = 2.*L/1000.
    
    # Figure setup
    fig, ax = plt.subplots()

    x_plot = np.arange(xmin, xmax + dx, dx)
    y_plot = np.arange(ymin, ymax + dy, dy)
    x_plot, y_plot = np.meshgrid(x_plot, y_plot, indexing='ij')

    xi = np.array((x_plot.flatten(), y_plot.flatten())).T

    grid = dset_rho.get_grid(iteration = it)
    rho = dset_rho.get_grid_data(grid = grid, iteration = it)
    T = dset_T.get_grid_data(grid = grid, iteration = it)
    Ye = dset_Ye.get_grid_data(grid = grid, iteration = it)
    grid.scale(ulength)

    rho_int = Interpolator(grid, rho, interp = 1)
    rho_plot = rho_int(xi).reshape(x_plot.shape)
    rho_plot = rho_plot * udens / rho_nuc
    
    T_int = Interpolator(grid, T, interp = 1)
    T_plot = T_int(xi).reshape(x_plot.shape)
    
    Ye_int = Interpolator(grid, Ye, interp = 1)
    Ye_plot = Ye_int(xi).reshape(x_plot.shape)
    
    vari = np.column_stack([T_plot.flatten(), Ye_plot.flatten(), np.log10(rho_nuc * rho_plot).flatten()])
    Yq_plot = np.maximum(0.0, Yq_interp(vari).reshape(x_plot.shape))
    
    #im.append(ax.imshow(rho_plot.transpose(), extent=[xmin, xmax, ymin, ymax], cmap = 'jet',vmin = 0, vmax =  3))
    #cbar = fig.colorbar(im)
    
    plt.pcolormesh(x_plot, y_plot, rho_plot.transpose(), cmap = 'jet',vmin = 0, vmax =  3)
    clb = plt.colorbar(label=r"$\rho / \rho_{\rm{nuc}}$")
    plt.xlabel(r"$x\;[Km]$")
    plt.ylabel(r"$y\;[Km]$")
    plt.savefig("./rho_movie/"+str(it)+".png", dpi = 500)
    clb.remove()
    plt.close()
    
    plt.pcolormesh(x_plot, y_plot, T_plot.transpose(), cmap = 'hot',vmin = 0, vmax =  50)
    clb = plt.colorbar(label=r"$T\;[\rm{MeV}]$")
    plt.xlabel(r"$x\;[Km]$")
    plt.ylabel(r"$y\;[Km]$")
    plt.savefig("./T_movie/"+str(it)+".png", dpi = 500)
    clb.remove()
    plt.close()

    plt.pcolormesh(x_plot, y_plot, Yq_plot.transpose(), cmap = 'CMRmap',vmin = 0, vmax =  0.15)
    clb = plt.colorbar(label=r"$Y_q$")
    plt.xlabel(r"$x\;[Km]$")
    plt.ylabel(r"$y\;[Km]$")
    plt.savefig("./Yq_movie/"+str(it)+".png", dpi = 500)
    clb.remove()
    plt.close()
    
    #im.append(ax.imshow(T_plot.transpose(), extent=[xmin, xmax, ymin, ymax], cmap = 'hot',vmin = 0, vmax =  50))
    #plt.savefig("./T_movie/"+str(it)+".png")
    
    #im.append(ax.imshow(Yq_plot.transpose(), extent=[xmin, xmax, ymin, ymax], cmap = 'CMRmap',vmin = 0, vmax =  0.15))
    #plt.savefig("./Yq_movie/"+str(it)+".png")
    



