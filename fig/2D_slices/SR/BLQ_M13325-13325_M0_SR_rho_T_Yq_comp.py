#!/usr/bin/env python

import os
import sys
sys.path.append(os.curdir)

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
       'figure.figsize'            : [7.3, 6.0],
       'image.cmap'                : 'CMRmap',
       'image.interpolation'       : 'none',
})
from mpl_toolkits.axes_grid1.inset_locator import inset_axes # Beautification
#### 7.3 " X 6 "

from glob import glob
import h5py as h
from matplotlib.colors import Normalize, LogNorm
from matplotlib.ticker import LogFormatter
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import scidata.carpet.grid as carpet
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator
import units as ut
import utils as util
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from scipy.interpolate import RegularGridInterpolator

#############################    Yq Calculation      ###################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data["composition"]

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

# The above was done by exploring Domenico's Table

Yq = 1. - comp[3] ## Domenico's convention is opposite to this in his table
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):       # This isn't exactly required
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
#####################################################################################################

# Plotting grid
L = [22, 22, 22] # Half the spatial length scale (xmax, ymax) for the 3 simulation snapshots in Km

################## Units ################
ulength = ut.conv_length(ut.cactus, ut.metric, 1)/1e3 # in km
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)
utime = ut.conv_time(ut.cactus, ut.cgs, 1) * 1.e3 # time in ms
rho_nuc = 2.70 * 1e14 # Nuclear density in g/cc
#########################################

# Read the data
dset = [
    h5.dataset(
        glob("../../../data/SR/BLQ_M13325-13325_M0_SR/extract/rho.xy.*.h5")),
        #+ glob("../../../../data/LS220_M140120_M0/extract/dens_unbnd.xy.*.h5")),
    h5.dataset(
        glob("../../../data/SR/BLQ_M13325-13325_M0_SR/extract/T.xy.*.h5")),
        #+glob("../../../../data/LS220_M140120_M0_L50/extract/dens_unbnd.xy.*.h5")),
    h5.dataset(
        glob("../../../data/SR/BLQ_M13325-13325_M0_SR/extract/Ye.xy.*.h5")),
        #+glob("../../../../data/LS220_M140120_M0_L50/extract/dens_unbnd.xy.*.h5")),
    h5.dataset(
        glob("../../../data/SR/BLQ_M13325-13325_M0_SR/extract/s.xy.*.h5")),
        #+glob("../../../../data/LS220_M140120_M0_L50/extract/dens_unbnd.xy.*.h5")),
        
]
               
sel = [0,1,2]
iterations = [dset[0].iterations[isel] for isel in sel]
t_merg = float(open("../../../data/SR/BLQ_M13325-13325_M0_SR/waveforms/tmerger.dat", "r").readline())
t_merg = util.get_retarded_time(t_merg) * utime
               
#norm = LogNorm(vmin=1e7, vmax=1e15)
#labels = [r"$\ell_{\rm mix} = 0$", r"$\ell_{\rm mix} = 50\, {\rm m}$"]
               
# Figure setup
fig, ax = plt.subplots(nrows=3, ncols=3)
fig.subplots_adjust(bottom=0.1, top=0.9, left=0.1, right=0.86, hspace=0.05, wspace=0.3)
               
im = []
for it_idx, it in enumerate(iterations): # it_idx is iteration index:0, 1, 2 or sel (column index). it is the iteration itself
    # Plotting grid
    xmin, xmax = -L[it_idx], L[it_idx]
    ymin, ymax = -L[it_idx], L[it_idx]
    dx = dy = 2.*L[it_idx]/1000.
               
    x_plot = np.arange(xmin, xmax + dx, dx)
    y_plot = np.arange(ymin, ymax + dy, dy)
    x_plot, y_plot = np.meshgrid(x_plot, y_plot, indexing='ij')
               
    xi = np.array((x_plot.flatten(), y_plot.flatten())).T
               # Row index
############################################################
    s_idx = 0
    grid = dset[s_idx].get_grid(iteration = it)
    rho = dset[s_idx].get_grid_data(grid = grid, iteration = it, variable = "HYDROBASE::rho")
        
    grid.scale(ulength)
        
    rho_int = Interpolator(grid, rho, interp = 1)
    rho_plot = rho_int(xi).reshape(x_plot.shape)
    rho_plot = rho_plot * udens / rho_nuc
    
    im.append(ax[s_idx, it_idx].imshow(rho_plot.transpose(), origin="lower", extent=[xmin, xmax, ymin, ymax], cmap = 'jet',vmin = 0, vmax =  3))
    ax[s_idx,it_idx].set_aspect('equal', 'datalim')
##################################################################
############################################################
    s_idx = 1
    grid = dset[s_idx].get_grid(iteration = it)
    T = dset[s_idx].get_grid_data(grid = grid, iteration = it, variable = "HYDROBASE::temperature")
    
    grid.scale(ulength)
    
    T_int = Interpolator(grid, T, interp = 1)
    T_plot = T_int(xi).reshape(x_plot.shape)
    
    im.append(ax[s_idx, it_idx].imshow(T_plot.transpose(), origin="lower", extent=[xmin, xmax, ymin, ymax], cmap = 'hot', vmin = 0, vmax = 50))
    ax[s_idx,it_idx].set_aspect('equal', 'datalim')
##################################################################
############################################################
    s_idx = 2
    grid = dset[s_idx].get_grid(iteration = it)
    Ye = dset[s_idx].get_grid_data(grid = grid, iteration = it, variable = "HYDROBASE::Y_e")
    
    grid.scale(ulength)
    
    Ye_int = Interpolator(grid, Ye, interp = 1)
    Ye_plot = Ye_int(xi).reshape(x_plot.shape)
    vari = np.column_stack([T_plot.flatten(), Ye_plot.flatten(), np.log10(rho_plot * rho_nuc).flatten()])
    Yq_plot = np.maximum(0.0, Yq_interp(vari)).reshape(x_plot.shape) # This is vectorization 

    im.append(ax[s_idx, it_idx].imshow(Yq_plot.transpose(), origin="lower", extent=[xmin, xmax, ymin, ymax], cmap = 'CMRmap', vmin = 0, vmax = 0.15))
    ax[s_idx,it_idx].set_aspect('equal', 'datalim')
##################################################################
    t = dset[0].get_time(iteration=it) * utime
    t = t - t_merg
    ax[0,it_idx].set_title(r"$t - %s = %.2f\ {{\rm ms}}$"%(r"t_{\rm{merg}}",t))
    ax[0,it_idx].set_xticklabels([])
    ax[1,it_idx].set_xticklabels([])
    ax[2,it_idx].set_xlabel(r"$x\ [{\rm km}]$")


ax[0,0].set_ylabel(r"$y\ [{\rm km}]$")
ax[1,0].set_ylabel(r"$y\ [{\rm km}]$")
ax[2,0].set_ylabel(r"$y\ [{\rm km}]$")

axins0 = inset_axes(ax[0, 2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="96%",  # height : 100%
                    loc = 'lower left',
                    bbox_to_anchor=(1.1, 0., 1, 1),
                    bbox_transform=ax[0, 2].transAxes,
                    borderpad=0)

axins1 = inset_axes(ax[1, 2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="96%",  # height : 100%
                    loc = 'lower left',
                    bbox_to_anchor=(1.1, 0., 1, 1),
                    bbox_transform=ax[1, 2].transAxes,
                    borderpad=0)

axins2 = inset_axes(ax[2, 2],
                    width="5%",  # width = 5% of parent_bbox width
                    height="96%",  # height : 100%
                    loc = 'lower left',
                    bbox_to_anchor=(1.1, 0., 1, 1),
                    bbox_transform=ax[2, 2].transAxes,
                    borderpad=0)

cbar0 = fig.colorbar(im[0], cax = axins0, orientation='vertical', label=r"$\rho / \rho_{\rm{nuc}}$")
cbar1 = fig.colorbar(im[1], cax = axins1, orientation='vertical', label=r"$T\;[\rm{MeV}]$")
cbar2 = fig.colorbar(im[2], cax = axins2, orientation='vertical', label=r"$Y_q$")

plt.savefig(sys.argv[0].replace(".py", "") + ".pdf")
    
               
