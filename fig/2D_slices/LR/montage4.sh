#!/usr/bin/env bash
​
for fname in ./rho_movie/rho_iter_*.png; do
bname=$(basename $fname)
bash /Users/aviralprakash/Desktop/scivis/bin/scivis-montage \
./rho_movie/${bname} \
-geometry 640x480-15+0 \
${bname}
done
