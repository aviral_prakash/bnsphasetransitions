#!/usr/bin/env python

import os
import sys
sys.path.append(os.curdir)

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
                           'figure.figsize'            : [3.0, 3.0],
                           'image.cmap'                : 'CMRmap',
                           'image.interpolation'       : 'none',
                           })
from mpl_toolkits.axes_grid1.inset_locator import inset_axes # Beautification


from glob import glob
import h5py as h
from matplotlib.colors import Normalize, LogNorm
from matplotlib.ticker import LogFormatter
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import scidata.carpet.grid as carpet
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator
import units as ut
import utils as util
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from scipy.interpolate import RegularGridInterpolator

#############################    Yq Calculation      ###################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data["composition"]

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

# The above was done by exploring Domenico's Table

Yq = 1. - comp[3] ## Domenico's convention is opposite to this in his table
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):       # This isn't exactly required
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))


#####################################################################################################

# Plotting grid
L = 30 # Half the spatial length scale (xmax, ymax)

################## Units ################
ulength = ut.conv_length(ut.cactus, ut.metric, 1)/1e3 # in km
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)
utime = ut.conv_time(ut.cactus, ut.cgs, 1) * 1.e3 # time in ms
rho_nuc = 2.70 * 1e14 # Nuclear density in g/cc
#########################################


# Read the data

dset_rho = h5.dataset(glob("../../../data/LR/BLQ_M13325-13325_M0_LR/output-000?/data/rho.xy.h5"))

im = []
for it in (dset_rho.iterations):
    # Figure setup
    fig, ax = plt.subplots()
    #fig.subplots_adjust(bottom=0.1, top=0.9, left=0.1, right=0.86, hspace=0.05, wspace=0.3)

    xmin, xmax = -L, L
    ymin, ymax = -L, L
    dx = dy = 2.*L/1000.

    x_plot = np.arange(xmin, xmax + dx, dx)
    y_plot = np.arange(ymin, ymax + dy, dy)
    x_plot, y_plot = np.meshgrid(x_plot, y_plot, indexing='ij')

    xi = np.array((x_plot.flatten(), y_plot.flatten())).T

    grid = dset_rho.get_grid(iteration = it)
    rho = dset_rho.get_grid_data(grid = grid, iteration = it)
    
    grid.scale(ulength)

    rho_int = Interpolator(grid, rho, interp = 1)
    rho_plot = rho_int(xi).reshape(x_plot.shape)
    rho_plot = rho_plot * udens / rho_nuc

    im.append(ax.imshow(rho_plot.transpose(), extent=[xmin, xmax, ymin, ymax], cmap = 'jet',vmin = 0, vmax =  3))
    plt.savefig("./rho_movie/rho_iter_"+str(it)+".png")
    plt.close()


