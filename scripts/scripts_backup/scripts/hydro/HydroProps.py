#!/usr/bin/env python

import numpy as np
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator
from scidata.utils import locate
import scivis.eostable as eos
import os
import sys
import time

if len(sys.argv) < 2 or not os.path.isdir(sys.argv[1]):
    print("Usage: {} /path/to/sim".format(sys.argv[0]))
    exit(0)
target = sys.argv[1]

dset = h5.dataset(locate("rho.xy.h5", root=target, followlinks=True) +
        locate("Y_e.xy.h5", root=target, followlinks=True) +
        locate("temperature.xy.h5", root=target, followlinks=True))

eos.init(os.environ['HOME'] + "/Data/EOS/BHB/BHB_lp_comp_14-Jul-2016.h5")

ofile = open("{}/collated/hydroprops.dat".format(target),"w")
ofile.write("# 1:time 2:rho_c 3:temp_c 4:Ye_c 5:XL_c 6:x_m 7:y_m 8:rho_m "
        "9:temp_m 10:Ye_m 11:XL_m\n")

for idx, it in enumerate(dset.iterations):
    print("Processing frame {}/{}...".format(idx, len(dset.iterations)-1)),
    start_t = time.time()
    grid = dset.get_grid(iteration=it)

    rho = dset.get_grid_data(grid, iteration=it, variable="HYDROBASE::rho")
    Ye = dset.get_grid_data(grid, iteration=it, variable="HYDROBASE::Y_e")
    temp = dset.get_grid_data(grid, iteration=it,
            variable="HYDROBASE::temperature")

    irho = Interpolator(grid, rho, interp=0)
    itemp = Interpolator(grid, temp, interp=0)
    iYe = Interpolator(grid, Ye, interp=0)
    xc = np.zeros((1,2))
    rho_c = irho(xc)[0]
    temp_c = itemp(xc)[0]
    Ye_c = iYe(xc)[0]
    XL_c = eos.evaluate("XL", rho_c, temp_c, Ye_c)

    x,y = grid[-1].mesh()
    im = np.argmax(rho[-1])
    x_m = x.flatten()[im]
    y_m = y.flatten()[im]
    rho_m = rho[-1].flatten()[im]
    temp_m = temp[-1].flatten()[im]
    Ye_m = Ye[-1].flatten()[im]
    XL_m = eos.evaluate("XL", rho_m, temp_m, Ye_m)

    ofile.write("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}\n".format(
        dset.get_time(it), rho_c, temp_c, Ye_c, XL_c, x_m, y_m, rho_m, temp_m,
        Ye_m, XL_m))

    print("done! (%.2f sec)" % (time.time() - start_t))
ofile.close()
