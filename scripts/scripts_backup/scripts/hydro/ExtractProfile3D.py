#!/usr/bin/env python
# Extracts the 3D hydro profiles

import argparse
from glob import glob
import h5py
from math import pi, sqrt
import numpy as np
import scidata.carpet.hdf5 as h5
import time

# Parse the CLI
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--description", dest="description",
        help="Brief string describing the data")
parser.add_argument("-i", "--iteration", dest="iteration",
        type=int, help="Number of the iteration to angle average")
parser.add_argument("-o", "--output", dest="output", required=True,
        help="Output filename (will be overwritten!)")
parser.add_argument("-p", "--path", dest="path", default='.',
        help="Path of the checkpoint files to use as input")
args = parser.parse_args()

# Locate files
print("Locating input files..."),
start_t = time.time()
if args.iteration is not None:
    flist = glob("%s/checkpoint*%d*.h5" % (args.path, args.iteration))
else:
    flist = glob("%s/checkpoint*.h5" % args.path)
assert(len(flist) > 0)
print("done! (%.2f sec)" % (time.time() - start_t))

# Read files
print("Parsing the metadata..."),
start_t = time.time()
dset = h5.dataset(flist)
print("done! (%.2f sec)" % (time.time() - start_t))

# Get the grid
print("Reading the grid..."),
start_t = time.time()
if args.iteration is None:
    args.iteration = dset.select_iterations().next()
grid = dset.get_grid(iteration=args.iteration)
print("done! (%.2f sec)" % (time.time() - start_t))

# Extract the 3D data
print("Extracting the 3D data..."),
start_t = time.time()

full_name = {
    'lapse'     : 'ADMBASE::alp',
    'betax'     : 'ADMBASE::betax',
    'betay'     : 'ADMBASE::betay',
    'betaz'     : 'ADMBASE::betaz',
    'gxx'       : 'ADMBASE::gxx',
    'gxy'       : 'ADMBASE::gxy',
    'gxz'       : 'ADMBASE::gxz',
    'gyy'       : 'ADMBASE::gyy',
    'gyz'       : 'ADMBASE::gyz',
    'gzz'       : 'ADMBASE::gzz',
    'rho'       : 'HYDROBASE::rho',
    'velx'      : 'HYDROBASE::vel[0]',
    'vely'      : 'HYDROBASE::vel[1]',
    'velz'      : 'HYDROBASE::vel[2]',
    'eps'       : 'HYDROBASE::eps',
    'Ye'        : 'HYDROBASE::Y_e',
    'press'     : 'HYDROBASE::press',
    'entr'      : 'HYDROBASE::entropy',
    'temp'      : 'HYDROBASE::temperature',
    'w_lorentz' : 'HYDROBASE::w_lorentz',
    'vol'       : 'THC_CORE::volform',
}

dfile = h5py.File(args.output.replace("@ITER@", str(args.iteration)), "w")

if args.description is not None:
    dfile.create_dataset("description", data=np.string_(args.description))

for rl in range(len(grid)):
    gname = "reflevel=%d" % rl
    dfile.create_group(gname)
    dfile[gname].attrs.create("delta", grid[rl].delta)
    dfile[gname].attrs.create("extent", grid[rl].extent())
    dfile[gname].attrs.create("iteration", args.iteration)
    dfile[gname].attrs.create("reflevel", rl)
    dfile[gname].attrs.create("time", grid[rl].time)
    for key, val in full_name.iteritems():
        data = dset.get_reflevel_data(grid[rl], iteration=args.iteration,
                variable=val, timelevel=0, dtype=np.float32)
        dfile[gname].create_dataset(key, data=data)
dfile.close()
print("done! (%.2f sec)" % (time.time() - start_t))
