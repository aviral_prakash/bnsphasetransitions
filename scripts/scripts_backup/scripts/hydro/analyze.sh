#!/usr/bin/env bash

function call {
    echo $*
    eval $*
}

ANALYSIS_HOME=$(cd $(dirname $0); pwd)
export PYTHONPATH=${ANALYSIS_HOME}/../modules:${PYTHONPATH}

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/simulation"
    exit 1
fi
target=$1

call cd ${target}

call python ${ANALYSIS_HOME}/HydroProps.py . || exit 1

call touch hydro.done
