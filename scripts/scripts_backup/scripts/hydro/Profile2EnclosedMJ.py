#!/usr/bin/env python

import gc
from glob import glob
import h5py
from math import pi, log10
import numpy as np
import scidata.carpet.grid as grid
from scidata.carpet.interp import Interpolator
import time

NLEVELS = 7

def read_carpet_grid(dfile):
  L = []
  for il in range(NLEVELS):
    gname = "reflevel={}".format(il)
    group = dfile[gname]
    level = grid.basegrid()
    level.delta = np.array(group.attrs["delta"])
    level.dim = 3
    level.directions = range(3)
    level.iorigin = np.array([0,0,0], dtype=np.int32)
    level.origin = np.array(group.attrs["extent"][0::2])
    level.n = np.array(group["rho"].shape, dtype=np.int32)
    level.rlevel = il
    L.append(level)
  return grid.grid(sorted(L, key=lambda x: x.rlevel))

def make_stretched_grid(x0, x1, x2, nlin, nlog):
  assert x1 > 0
  assert x2 > 0
  x_lin_f = np.linspace(x0, x1, nlin)
  x_log_f = 10.0**np.linspace(log10(x1), log10(x2), nlog)
  return np.concatenate((x_lin_f, x_log_f))

if __name__ == '__main__':
  fname = sorted(glob("*.h5"), key=lambda x: int(x.replace(".h5", "")))[-1]
  dfile = h5py.File(fname, "r")

  # Read carpet grid
  start_t = time.time()
  print("Reading grid..."),
  grid = read_carpet_grid(dfile)
  print("done! ({:.2f} sec)".format(time.time() - start_t))

  # Rest mass and angular momentum densities
  D, J = [], []
  for idx, rlevel in enumerate(grid):
    start_t = time.time()
    print("Processing reflevel {}...".format(idx)),
    group = dfile["reflevel={}".format(rlevel.rlevel)]

    x, y, z = rlevel.mesh()

    rho   = np.array(group["rho"])
    eps   = np.array(group["eps"])
    press = np.array(group["press"])
    velx  = np.array(group["velx"])
    vely  = np.array(group["vely"])
    velz  = np.array(group["velz"])
    W     = np.array(group["w_lorentz"])
    gxx   = np.array(group["gxx"])
    gxy   = np.array(group["gxy"])
    gxz   = np.array(group["gxz"])
    gyy   = np.array(group["gyy"])
    gyz   = np.array(group["gyz"])
    gzz   = np.array(group["gzz"])
    vol   = np.array(group["vol"])

    vup = [velx, vely, velz]
    vlow = [np.zeros_like(vv) for vv in [velx, vely]]
    metric = [[gxx, gxy, gxz], [gxy, gyy, gyz], [gxz, gyz, gzz]]
    for i in range(2):
      for j in range(3):
        vlow[i][:] += metric[i][j][:]*vup[j][:]
    vphi = -y*vlow[0] + x*vlow[1]

    D.append(rho*W*vol)
    J.append((rho*(1 + eps) + press)*W*W*vol*vphi)

    del vup
    del vlow
    del metric
    del vphi
    del rho
    del eps
    del press
    del velx
    del vely
    del velz
    del W
    del gxx
    del gxy
    del gxz
    del gyy
    del gyz
    del gzz
    print("done! ({:.2f} sec)".format(time.time() - start_t))
  gc.collect()

  # Make cylindrical grid
  r_cyl_f   = make_stretched_grid(0., 15., 512., 75, 64)
  z_cyl_f   = make_stretched_grid(0., 15., 512., 75, 64)
  phi_cyl_f = np.linspace(0, 2*pi, 64)

  r_cyl   = 0.5*(r_cyl_f[1:] + r_cyl_f[:-1])
  z_cyl   = 0.5*(z_cyl_f[1:] + z_cyl_f[:-1])
  phi_cyl = 0.5*(phi_cyl_f[1:] + phi_cyl_f[:-1])

  r_cyl_3d, phi_cyl_3d, z_cyl_3d = np.meshgrid(r_cyl, phi_cyl, z_cyl, indexing='ij')
  x_cyl_3d = r_cyl_3d*np.cos(phi_cyl_3d)
  y_cyl_3d = r_cyl_3d*np.sin(phi_cyl_3d)

  # Interpolate D and J to the cylindrical grid
  start_t = time.time()
  print("Interpolating to cylindrical grid..."),

  iD = Interpolator(grid, D, interp=1)
  iJ = Interpolator(grid, J, interp=1)

  xi = np.column_stack([x_cyl_3d.flatten(), y_cyl_3d.flatten(), z_cyl_3d.flatten()])
  D_cyl = iD(xi).reshape(r_cyl_3d.shape)
  J_cyl = iJ(xi).reshape(r_cyl_3d.shape)

  print("done! ({:.2f} sec)".format(time.time() - start_t))

  # Reduce data (assume symmetry across xy-plane)
  start_t = time.time()
  print("Reduce data..."),
  dphi_cyl = np.diff(phi_cyl_f)[np.newaxis,:,np.newaxis]
  dz_cyl = np.diff(z_cyl_f)[np.newaxis,np.newaxis,:]
  D_rc = 2*np.sum(D_cyl*dz_cyl*dphi_cyl, axis=(1,2))
  J_rc = 2*np.sum(J_cyl*dz_cyl*dphi_cyl, axis=(1,2))
  print("done! ({:.2f} sec)".format(time.time() - start_t))

  # Write to disk
  start_t = time.time()
  print("Output data..."),

  dr_cyl = np.diff(r_cyl_f)
  ofile = open("MJ_encl_" + fname.replace(".h5", ".txt"), "w")
  ofile.write("# 1:rcyl 2:drcyl 3:M 4:J\n")
  for i in range(r_cyl.shape[0]):
    ofile.write("{} {} {} {}\n".format(r_cyl[i], dr_cyl[i], D_rc[i], J_rc[i]))
  ofile.close()

  print("done! ({:.2f} sec)".format(time.time() - start_t))
