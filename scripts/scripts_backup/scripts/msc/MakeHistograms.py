#!/usr/bin/env python

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import argparse
from glob import glob
import numpy as np
import os
import scidata.carpet.hdf5 as h5
import time

# Hardcoded options: histograms ranges
rho_edges = 10.0**np.linspace(4.0, 16.0, 120)
temp_edges = 10.0**np.linspace(-2, 2, 50)
Ye_edges = np.linspace(0, 0.5, 50)

# Parse CLI
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", metavar="/path/to/data", required=True,
        help="Path to the folder containing the 3D CarpetIOHDF5 data")
parser.add_argument("-o", "--outdir", default=".",
        help="Path to the output data folder")
parser.add_argument("-r", "--reflevel", type=int, default=0,
        help="Refinement level to analyze")
parser.add_argument("-w", "--weight", type=float, default=1.0,
        help="Multiply mass by this factor (to account for symmetries)")
args = parser.parse_args()

# Prepare output dir
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)

# Locate files
print("Locating input files..."),
start_t = time.time()
flist  = glob(args.input + "/rho.file*.h5")
flist += glob(args.input + "/temperature.file*.h5")
flist += glob(args.input + "/Y_e.file*.h5")
flist += glob(args.input + "/volform.file*.h5")
flist += glob(args.input + "/w_lorentz.file*.h5")
print("done! (%.2f sec)" % (time.time() - start_t))
assert(len(flist) > 0)

# Read the metadata
print("Parsing the metadata..."),
start_t = time.time()
dset = h5.dataset(flist)
print("done! (%.2f sec)" % (time.time() - start_t))

# Output histogram edges
ofname = args.outdir + "/edges.npz"
if not os.path.isfile(ofname):
    np.savez(ofname, rho=rho_edges, temp=temp_edges, Ye=Ye_edges)

# Analyze the data
for it in dset.iterations:
    ofname = args.outdir + "/histogram.r{}.i{:06d}.npz".format(args.reflevel, it)
    if os.path.isfile(ofname):
        continue

    print("Processing iteration %d..." % it),
    rlevel = dset.get_reflevel(iteration=it, reflevel=args.reflevel)
    rho = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::rho")
    temp = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::temperature")
    Ye = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::Y_e")
    vol = dset.get_reflevel_data(rlevel, iteration=it,
            variable="THC_CORE::volform")
    W = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::w_lorentz")
    dens = rho*W*vol
    del W
    del vol

    # get rid of ghost data
    rho = rho[3:-3,3:-3,3:-3]
    temp = temp[3:-3,3:-3,3:-3]
    Ye = Ye[3:-3,3:-3,3:-3]
    dens = dens[3:-3,3:-3,3:-3]

    # convert rho to g/cm^3
    rho *= 6.176269145886162e+17

    # compute histograms
    data = np.column_stack((rho.flatten(), temp.flatten(), Ye.flatten()))
    H, _ = np.histogramdd(data,
            bins=(rho_edges, temp_edges, Ye_edges),
            weights=dens.flatten()*np.prod(rlevel.delta)*args.weight)

    # output data
    np.savez(ofname, histogram=H, time=dset.get_time(it))

    print("done! (%.2f sec)" % (time.time() - start_t))
