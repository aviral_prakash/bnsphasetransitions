#!/usr/bin/env bash
  
if [ -z "$1" ]; then
        echo "Usage: $0 simulation"
        exit 1
fi
target=$1
cd $target

for f in output-????; do
        if [ ! -f ${f}.tar.done ]; then
                bash /global/homes/a/aup1075/scripts/runs_thc_aa_scripts/MakeTarM0.sh ${f}
                wait
                touch ${f}.tar.done
        fi
done
