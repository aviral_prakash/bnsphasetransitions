#!/usr/bin/env python
# Convert 2D output on the xy plane to 1D output along the x direction

import scidata.carpet.hdf5 as h5
import scidata.monodataset as mdd
import re
import sys

try:
    ifname = sys.argv[1]
except IndexError:
    print("Usage: {} file1.xz.h5".format(sys.argv[0]))
    exit(1)

match = re.match("(.+)\.xz\.h5", ifname)
if match is None:
    print("Invalid input file: {}".format(ifname))
    exit(1)
else:
    ofnamex = match.group(1) + ".x.xg"
    ofnamez = match.group(1) + ".z.xg"

dset2d = h5.dataset(ifname)
dsetx  = mdd.dataset()
dsetz  = mdd.dataset()

framesx, framesy = [], []
for it in dset2d.iterations:
    grid = dset2d.get_grid(iteration=it)
    data = dset2d.get_grid_data(grid, iteration=it)
    Lx, Ly = [], []
    for i in range(len(grid)):
        rlevel = grid.levels[i]
        x, z = rlevel.mesh()
        ix0 = x.shape[0]/2
        iz0 = 3

        frm = mdd.frame()
        frm.index = rlevel.timestep
        frm.time = rlevel.time
        frm.data_x = x[:, iz0]
        frm.data_y = data[i][:, iz0]
        Lx.append(frm)

        frm = mdd.frame()
        frm.index = rlevel.timestep
        frm.time = rlevel.time
        frm.data_x = z[ix0, :]
        frm.data_y = data[i][ix0, :]
        Lz.append(frm)
    frmx = Lx[0]
    for f in Lx[1:]:
        frmx.merge(f)
    framesx.append(frmx)

    frmz = Lz[0]
    for f in Lz[1:]:
        frmz.merge(f)
    framesz.append(frmz)
dsetx.import_framelist(framesx)
dsetx.write_xg(ofnamex)

dsetz.import_framelist(framesz)
dsetz.write_xg(ofnamez)

