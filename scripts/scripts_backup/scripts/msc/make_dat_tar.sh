#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Usage: $0 simulation"
	exit 1
fi
target=$1
cd $target

for f in output-????; do 
	if [ ! -f ${f}.dat.tar.done ]; then 
		tar --exclude=${f}/checkpoint -cf ${f}.dat.tar ${f}
		wait 
		touch ${f}.dat.tar.done
	fi
done
