#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 [segment]"
    exit 1
fi

if [ -f $1/PREVIOUS ]; then
    PREVIOUS=$1/PREVIOUS
else
    PREVIOUS=""
fi

tar cvf $1.tar \
    $1/data/BH_diagnostics.ah1.gp\
    $1/data/bnstracker* \
    $1/data/mp_Psi4_l?_m?_r*.asc \
    $1/data/rho.maximum.asc \
    $1/data/temperature.maximum.asc \
    $1/data/H.norm2.asc \
    $1/data/alp.minimum.asc \
    $1/data/dens*.norm1.asc \
    $1/data/luminosity*.norm1.asc \
    $1/data/dens_unbnd.x?.h5 \
    $1/data/s_phi.x?.h5 \
    $1/data/alp.x?.h5 \
    $1/data/rho.x?.h5 \
    $1/data/entropy.x?.h5 \
    $1/data/temperature.x?.h5 \
    $1/data/Y_e.x?.h5 \
    $1/data/outflow_det_?.asc \
    $1/data/outflow_surface_det_0*.asc \
    $1/data/outflow_surface_det_1*.asc \
    $1/data/thc_leakagem0-thc_leakage_m0_is_on..asc \
    $1/data/thc_leakagem0-thc_leakage_m0_flux..asc \
    $1/data/carpet-timing..asc \
    $1/batch.sub \
    $1/parfile.par \
    ${PREVIOUS} \
    $1/*.out \
    $1/*.err \
        || exit 1
