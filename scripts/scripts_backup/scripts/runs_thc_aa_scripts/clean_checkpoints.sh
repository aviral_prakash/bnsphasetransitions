#!/bin/bash

function get_it_num() {
    local p=${1/*it_}
    local q=${p%.file*}
    echo $q
}

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/checkpoint/folder"
    exit 1
fi
cd $1 || exit 1

rm -f checkpoint*.tmp.*

maxn=0
for f in checkpoint*; do
    n=$(get_it_num $f)
    if [ $n -gt $maxn ]; then
        maxn=$n
    fi
done

for f in checkpoint*; do
    n=$(get_it_num $f)
    if [ $n -lt $maxn ]; then
        rm $f
    fi
done
