#!/usr/bin/python

import sys
import os
from os import path
import multiprocessing as mp
from argparse import ArgumentParser
from functools import partial
import json
import numpy as np
import h5py
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
import time
import datetime

import units # units.py

def task_test(filename):
    """ Dummy task """
    print mp.current_process(), filename
    time.sleep(1)
    return filename

def default_setup():
    """ Default setup/parameters """
    return ({"nref":7,           # number of refinement levels
             "corr":True,        # apply the correction to the Cactus grid
             "lapse_lim":0.15,   # limiting lapse to determine BH formation
             "ref_level_plot":6, # 
             "rho_atm_cgs":1e2,  # atmosphere density in gcs units
             # size of the histogram arrays
             "n_dens":120,       # density
             "n_temp":50,        # temperature
             "n_ye":64,          # Ye
             # extrema histogram arrays
             "dens_min":1e4,     # min density [g/cm^3]
             "dens_max":1e16,    # max density [g/cm^3]
             "temp_min":1e-2,    # min temperature [MeV]
             "temp_max":1e2,     # max temperature [MeV]
             "ye_min":1e-2,      # min ye [-]
             "ye_max":6.4e-1,    # max ye [-]
             # buffer zones negative dir
             "nbf_coa":[3,3,3],  # x-y-z coarse grid 
             "nbf_ref":[9,9,3],  # x-y-z refinements
             # buffer zones positive dir
             "pbf_coa":[3,3,3],  # x-y-z coarse grid 
             "pbf_ref":[9,9,9]   # x-y-z refinements
         })

def dump_dict_to_json(d, filename):
    """ Dump a dictionary to JSON file """
    j = json.dumps(d)
    f = open(filename,"w")
    f.write(j)
    f.close()
    return

def compute_hist(filename, s, outdir):
    """ Main task """
    process = mp.current_process()
    print("%s: %s" % (process.name, filename))
    based, filen = os.path.split(filename)
    name, ext = filen.split(".") # note: name = iteration
    mylogf = open(outdir + "/" + name + "_" + process.name + ".log","w")
    # Define the histogram arrays
    ldens_array = np.linspace(np.log10(s["dens_min"]),np.log10(s["dens_max"]),s["n_dens"])
    ltemp_array = np.linspace(np.log10(s["temp_min"]),np.log10(s["temp_max"]),s["n_temp"])
    ye_array    = np.linspace(s["ye_min"],s["ye_max"],s["n_ye"])
    # Convert the density array in cactus units
    ldens_array = np.log10(units.conv_dens(units.cgs,units.cactus,10.**ldens_array))
    # Convert the atmosphere limit to Cactus units
    rho_atm_cactus = units.conv_dens(units.cgs,units.cactus,s["rho_atm_cgs"])
    # Define the number of buffer zones in the negative direction
    nbuff_neg = []
    nbuff_neg.append(s["nbf_coa"])
    for i in range(1,s["nref"]):
        nbuff_neg.append(s["nbf_ref"]) 
    # Define the number of buffer zones in the positive direction
    nbuff_pos = []
    nbuff_pos.append(s["pbf_coa"])
    for i in range(1,s["nref"]):
        nbuff_pos.append(s["pbf_ref"])
    # Open the h5 file
    try:
        f = h5py.File(filename,"r")
    except IOError:
        msg = "%s: Error opening %s" % (process.name, filename)
        print(msg)
        mylogf.write(msg)
        mylogf.close()
        return(msg)
    # Load the grid resolution for the different refinement levels
    dx = []
    for i in range(s["nref"]):
        key = "reflevel="+str(i)
        dx.append(list(f[key].attrs.values())[0][:] )
        time_cactus = list(f[key].attrs.values())[4]
        time_cgs = units.conv_time(units.cactus,units.cgs,time_cactus)
        mylogf.write('dx of level' + str(i) + ': ' + str(dx[i])+'\n')
    mylogf.write('time=' + str(time_cgs)+'\n')               
    # Load the grid extension for the different refinement levels
    xmin = []
    xmax = []
    for i in range(s["nref"]):
        tmp = np.zeros(3)
        key = "reflevel="+str(i)
        try:
            ndummy = len(list(f[key].attrs.values())[1])
            num_read = 1 
        except TypeError:
            ndummy = len(list(f[key].attrs.values())[2])
            num_read = 2 
        tmp[0] = list(f[key].attrs.values())[num_read][0]-dx[i][0]/2.#+nbuff_neg[i][0]*dx[i][0]
        tmp[1] = list(f[key].attrs.values())[num_read][2]-dx[i][1]/2.#+nbuff_neg[i][1]*dx[i][1]
        tmp[2] = list(f[key].attrs.values())[num_read][4]-dx[i][2]/2.#+nbuff_neg[i][2]*dx[i][2]
        xmin.append(tmp)
        tmp=np.zeros(3)
        tmp[0] = list(f[key].attrs.values())[num_read][1]+dx[i][0]/2.#-nbuff_pos[i][0]*dx[i][0]
        tmp[1] = list(f[key].attrs.values())[num_read][3]+dx[i][1]/2.#-nbuff_pos[i][1]*dx[i][1]
        tmp[2] = list(f[key].attrs.values())[num_read][5]+dx[i][2]/2.#-nbuff_pos[i][2]*dx[i][2]
        xmax.append(tmp)
        # Correct along all directions
        if (s["corr"]):
            if (i > 0):
                xmin[i] = xmin[i]+dx[i]/2.
                xmax[i] = xmax[i]+dx[i]/2.
        mylogf.write('grid min (incl. buffer zones) of level'+str(i)+': '+str(xmin[i])+'\n')
        mylogf.write('grid max (incl. buffer zones) of level'+str(i)+': '+str(xmax[i])+'\n')
    # Search for the minimum lapse function in the symmetry z plane at the most refined level
    ref_level_plot = s["ref_level_plot"]
    key = "reflevel="+str(ref_level_plot)
    lapse = np.array(f[key+"/lapse"])
    lapse.astype(float)
    lapse = np.ma.masked_invalid(lapse,1.)
    min_lapse = np.amin(lapse)
    loc_min_lapse = np.unravel_index(np.argmin(lapse),lapse.shape)
    mylogf.write('min_lapse='+str(min_lapse)+'\n')
    mylogf.write('loc_min_lapse='+str(loc_min_lapse)+'\n')
    mylogf.write('min_lapse='+str(min_lapse)+'\n')
    mylogf.write('loc_min_lapse='+str(loc_min_lapse)+'\n')
    # Initialize the histogram
    hist_d_t  = np.zeros((s["n_dens"],s["n_temp"])) 
    hist_d_ye = np.zeros((s["n_dens"],s["n_ye"])) 
    hist_t_ye = np.zeros((s["n_temp"],s["n_ye"])) 
    hist_d_t_ye = np.zeros((s["n_dens"],s["n_temp"],s["n_ye"])) 
    for n in range(s["nref"]):
        mylogf.write('Process level '+str(n)+'\n')
        key="reflevel="+str(n)
        dens=np.array(f[key+"/rho"])
        temp=np.array(f[key+"/temp"])
        ye=np.array(f[key+"/Ye"])
        sqrtdetg=np.array(f[key+"/vol"])
        W=np.array(f[key+"/w_lorentz"])
        lapse=np.array(f[key+"/lapse"])
        lapse=np.ma.masked_invalid(lapse,1.)
        for i in range(nbuff_neg[n][0],dens.shape[0]-nbuff_pos[n][0]):
            next_x=0
            pos_x = xmin[n][0] + dx[n][0]/2. + float(i)*dx[n][0]
            if (n < s["nref"]-1):
                if (pos_x > xmin[n+1][0] and pos_x < xmax[n+1][0] ):
                    next_x=1
            for j in range(nbuff_neg[n][1],dens.shape[1]-nbuff_pos[n][1]): 
                next_y=0
                pos_y = xmin[n][1] + dx[n][1]/2 + float(j)*dx[n][1]
                if (n < s["nref"]-1):
                    if (pos_y > xmin[n+1][1] and pos_y < xmax[n+1][1] ):
                        next_y=1
                for k in range(nbuff_neg[n][2],dens.shape[2]-nbuff_pos[n][2]):
                    next_z=0
                    pos_z = xmin[n][2] + dx[n][2]/2 + float(k)*dx[n][2]
                    if (n < s["nref"]-1):
                        if (pos_z > xmin[n+1][2] and pos_z < xmax[n+1][2] ):
                            next_z=1
                    # Skip atmosphere 
                    if (dens[i][j][k] < rho_atm_cactus):
                        continue
                    # Skip BH portion
                    if (lapse[i][j][k] < s["lapse_lim"]):
                        continue
                    # Skip if not at the locally most refined level 
                    if (next_x+next_y+next_z == 3):
                        continue
                    # Skip if outside of the grids
                    #if ( dens[i][j][k] < s["dens_min"] or dens[i][j][k] > s["dens_max"] ):
                    #    continue
                    #if ( temp[i][j][k] < s["temp_min"] or temp[i][j][k] > s["temp_max"] ):
                    #    continue
                    #if ( ye[i][j][k]   < s["ye_min"]   or ye[i][j][k]   > s["ye_max"] ):
                    #    continue
                    # Compute the indexes for the histograms
                    ii = np.argmin(np.abs(ldens_array - np.log10(dens[i][j][k])))
                    jj = np.argmin(np.abs(ltemp_array - np.log10(temp[i][j][k])))
                    kk = np.argmin(np.abs(ye_array - ye[i][j][k]))
                    # Compute the local baryonic mass
                    tmp = W[i][j][k]*dens[i][j][k]*sqrtdetg[i][j][k]*dx[n][0]*dx[n][1]*dx[n][2]
                    # Update the histograms
                    hist_d_t[ii][jj] += tmp
                    hist_d_ye[ii][kk] += tmp
                    hist_t_ye[jj][kk] += tmp
                    hist_d_t_ye[ii][jj][kk] += tmp
    f.close()          
    # Create the output folder/file
    out_file_name = outdir #+ "/" + name + "/"
    if not os.path.exists(out_file_name): 
      os.makedirs(out_file_name)
    # Save the histograms
    #np.save(out_file_name+'/'+ name +'_hist_d_t',hist_d_t)
    #np.save(out_file_name+'/'+ name +'_hist_d_ye',hist_d_ye)
    #np.save(out_file_name+'/'+ name +'_hist_t_ye',hist_t_ye)
    np.savez(out_file_name+'/hist_'+ name, 
             dens_temp_ye=hist_d_t_ye,
             dens_temp=hist_d_t, 
             dens_ye=hist_d_ye, 
             temp_ye=hist_t_ye, 
             time=[int(name),time_cactus,time_cgs],
             setup=s)
    np.savetxt(out_file_name+'/time_'+name+'.txt',[int(name),time_cactus,time_cgs])
    msg = "%s %s => %s" % (process.name,filename,out_file_name)
    mylogf.write(msg)
    mylogf.close()                              
    return msg

if __name__ == '__main__':
    
    # Parse input
    parser = ArgumentParser(description="process h5 file and compute mass-weighted histograms")
    parser.add_argument("-i", dest="filenames", 
                        nargs='+', required=True,
                        help="inputfiles", metavar="FILE")
    parser.add_argument("-p", dest="procs", 
                        nargs='?', default=1,
                        help="processes in the pool")
    parser.add_argument("-o", dest="outputdir", 
                        nargs='?', default=os.getcwd(),
                        help="output directory")    
    #parser.add_argument("-s", dest="setup", type=json.loads,
    #                    nargs='?', default=default_setup(),
    #                    help="JSON file for setup/parameters")
    
    # Info/checks
    args = parser.parse_args()
    files_found = []
    for f in args.filenames:
        if os.path.isfile(f):
            files_found.append(f)
            print(f)
    if not files_found:
        print("No file was found")
        sys.exit()        
    if not os.path.exists(args.outputdir):
        os.mkdir( args.outputdir, 0755 )
    print("Process %d files of %d" % (len(files_found),len(args.filenames)))
    print("pool_procs = %d" % int(args.procs))
    #dump_dict_to_json(default_setup(), "profiles2hist_setup.json") # 

    # Launch tasks
    pool = mp.Pool(processes=int(args.procs))
    #result_list = pool.map(task_test, args.filenames) # test: the pool/map
    task = partial(compute_hist, s=default_setup(), outdir=args.outputdir)
    #task(files_found[0]) # test/use to spot errors in task()
    result_list = pool.map(task, files_found)

    for m in result_list:
        print(m)
    sys.exit()

