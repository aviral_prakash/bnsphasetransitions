#!/usr/bin/env python

""" Plot histograms from numpy files, work in parallel """

from argparse import ArgumentParser
import multiprocessing as mp
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import os
import sys

from histoplot1 import load_and_plot

if __name__ == '__main__':

    parser = ArgumentParser(description="Plot histograms from numpy files, work in parallel")
    parser.add_argument("-i", dest="filenames", 
                        nargs='+', required=True,
                        help="inputfiles")
    parser.add_argument("-o", dest="outputdir", 
                        nargs='?', default=os.getcwd(),
                        help="output directory") 
    parser.add_argument("-p", dest="procs", 
                        nargs='?', default=1,
                        help="processes in the pool")

    # Info/checks
    args = parser.parse_args()
    files_found = []
    for f in args.filenames:
        if os.path.isfile(f):
            files_found.append(f)
            print(f)
    if not files_found:
        print("No file was found")
        sys.exit()
    if not os.path.exists(args.outputdir):
        os.mkdir( args.outputdir, 0755 )
    print("pool_procs = %d" % int(args.procs))
              
    # Launch tasks
    pool = mp.Pool(processes=int(args.procs))
    task = partial(load_and_plot, outputdir=args.outputdir)
    result_list = pool.map(task, files_found)

    for m in result_list:
        print(m)
    print("done")
    sys.exit()
