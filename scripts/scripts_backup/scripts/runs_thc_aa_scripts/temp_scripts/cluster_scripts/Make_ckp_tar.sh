#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 segment"
    exit 1
fi
target=$1

if [ -d ${target}/checkpoint ]; then
    tar cf ${target}.chk.tar ${target}/checkpoint || exit 1
fi
touch ${target}.ckp.tar.done
