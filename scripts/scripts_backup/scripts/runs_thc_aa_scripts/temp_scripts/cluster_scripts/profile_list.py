import os
import optparse as op
import numpy as np
import scidata.multipole as multipole
from scidata.utils import diff, fixed_freq_int_1, fixed_freq_int_2, integrate
from scidata.windows import exponential as window
from scipy.signal import detrend

##NB this script requires a local installation of anaconda and scidata!

#name of output file with merger time
flist = "profile_times.txt"

def collate(outfile, infiles):
  ofile = open(outfile, 'w')

  # Collate data
  c = '#'
  told  = None
  for fname in infiles:
    for dline in open(fname, 'r'):
      tnew = float(dline.split()[0])
      if tnew > np.inf:
        break
      if told is None or tnew > told*(1 + 1e-15):
        ofile.write(dline)
        told = tnew
      #
    #
  #
#

def collate_psi4(n_restarts):
  files   = ['output-0{:03d}/data/mp_Psi4_l2_m2_r600.00.asc'.format(i) for i in range(n_restarts+1)]
  string  = []
  for i in range(n_restarts+1):
    string.append(files[i])
    collate('./collate/mp_Psi4_l2_m2_r600.00.dat', string)
  #
#

def load_psi4(fname):
  L = []
  L += open(fname, 'r').readlines()
  return multipole.parse(L)
#

def get_22_strain(sd_path):
  options = {"cutoff.freq"     : 0.002,
    "detector.radius" : 600.0,
    'window.delta'    : 200.0}
  fname = os.path.join(sd_path,'mp_Psi4_l2_m2_r600.00.dat')
  Psi4 = {}

  t, Psi4 = load_psi4(fname)
  Psi4 *= options['detector.radius']
  #print("%d, %d" % (len(t), len(Psi4)))
  dtime = t[1] - t[0]

  win = window(t/t.max(), delta=options["window.delta"]/t.max())

  psi4 = Psi4 * win

  h_ff = win * fixed_freq_int_2(psi4, 2*options["cutoff.freq"]/2, dt=dtime)

  return t, h_ff, dtime
#
def save_tmerger(sd):
  t, h, dt = get_22_strain(sd)
  tm = t[np.argmin(np.sqrt(h.conjugate()*h).max()-np.sqrt(h.conjugate()*h))]
  ofile = open(flist, 'w')
  times = (np.array([0,2,8,32,128])*85*dt)+tm

  for it in times:
    next_pt = it
    ofile.write(str(next_pt)+'\n')
  #
#

def main(sd_path, skip_subdirs=0):
  """only works if the only directories are restarts, otherwise need to change the
  skip-subdirs parameter"""
  n_restarts = len(os.walk(sd_path).next()[1]) - skip_subdirs
  sdc = os.path.join(sd_path,'collate')
  os.mkdir(sdc)
  collate_psi4(n_restarts)
  save_tmerger(sdc)
#

main('./', 1)
