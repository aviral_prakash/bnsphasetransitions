#!/bin/bash

SIMPATH=""
HERE=pwd
VIZQUANTITIES="rho.maximum.asc"

## Vizualize quantities for sanity checks
python plot_quantities.py $SIMPATH $VIZQUANTITIES

##
# VSEVA'S ANALYSIS OF THE DISK
##
##
# PUT DISK FILES INTO output-????
##

# Create tarballs
for s in $SIMPATH
do	
	cd $SIMPATH/$s
	for o in $(ls output-????) 
	do	
		if ! [ -e $o.tar ]
		then 
			bash $HERE/MakeTarM0.sh $o 
			bash $HERE/Make_dat_tar.sh $o
		fi
	done
done

## Extract profiles
#TODO:	skip LR simulations
#	think about the fact that each profile extraction will take long and for many simulations we could exceed the number of hours in the queue. However, a maximum of 8 jobs can be run simultaneously on LRZ..
for s in $SIMPATH
do	
	$HERE/ExtractProfile3D.sh $SIMPATH/$s 
done

## Find the outputs from which extract the profiles
# TODO:	since Sebastiano wants this step not to be automatic, probably the best thing is to extract ALL the profiles but store only the significant ones. In this case the following procedure can be run on Tullio to rsync AT LEAST the significant ones selected by the algorythm. Then the user has to check if other profiles are relevant and save them by hand.
python $HERE/profile_list.py 

prefix="./"
suffix="/data/mp_Psi4_l2_m2_r600.00.asc"


input="./profile_times.txt"
output="./profile_outputs.txt"

while IFS='' read -r var
do
  echo $var
  out=$(grep -Ril "$var" ./output-0???/data/mp_Psi4_l2_m2_r600.00.asc)
  out=${out#$prefix}
  out=${out%$suffix}
  echo $out >> $output
done < "$input"
