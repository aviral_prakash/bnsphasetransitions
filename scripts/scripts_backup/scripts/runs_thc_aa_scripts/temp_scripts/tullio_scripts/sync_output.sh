#!/bin/bash

## Script to syncronize the relevant data of a simulations and extract the tarballs.

## Path in which the simulations are stored on Tullio e.g. $/path/to/simulations$
local_path=""

## List of simulations to sync, space-separated e.g. "SIM1 SIM2 .."
sim_list=""

## Address and path of simulations in the cluster, e.g. user_id@address_cluster:/path/to/simulations
path_address=""

## NB you can avoid to type the password setting a pub_key for ssh
## NB here it is assumed that the name of the folder on Tullio is the same as the one on the cluster
for s in $sim_list 
do
	cd $local_path/$s
	OUTLIST=$(ls -d output-????)
	aux=''
	for o in $OUTLIST
	do
		aux=$aux' --exclude=*'$o.tar
	done
	rsync -arvP --append $path_address/$s/*.tar --exclude="*.dat.tar"$aux ./
	rsync -arvP --append $path_address/$s/profiles/ ./
	for o in $(ls output-????.tar) 
	do
		tar -xvf $o
		rm -rf $o
		chgrp -R numrel *
		chmod -R g+rx *
		chmod -R g-w */
	done
done
