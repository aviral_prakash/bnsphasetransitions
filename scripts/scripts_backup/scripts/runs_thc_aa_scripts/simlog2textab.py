#!/usr/bin/env python

""" simlog2textab.py
Process simulation.log into a LaTeX table """

import argparse
import os

def fgrep(fname, s):
    """ Dummy file grep, return a list """
    ln = []
    for line in open(fname):
        if s in line:
            #print line
            ln.append(line.strip())
    return ln

def maketab(cols,fname,captxt="",labtxt=""):
    """ Print a LateX table to file 
    Assumes columns are strings """
    nrow = len(cols)
    ncol = len(cols[0])
    print "no_rows = ",nrow
    print "no_cols = ",ncol
    # Define some formats
    colalignm = "c" * ncol
    colformat = "%s&" * ncol
    colformat = colformat[:-1]+"\\\ \n"
    # Write
    f = open(fname, "w")
    line = "\\begin{table}[]\n"
    line += "\centering\n"
    line += "\caption{" + captxt + "}\n"
    line += "\label{" + labtxt + "}\n"
    line += "\\begin{tabular}{" + colalignm + "}\n"
    line += "\hline\hline\n"
    f.write(line)
    for n in range(nrow):
        #print n , tuple(cols[:][n]), colformat
        line = colformat % tuple(cols[:][n])
        #print line
        f.write(line)        
    line = "\hline\hline\n"
    line += "\end{tabular}\n"
    line += "\end{table}\n"
    f.write(line)
    f.close()
    return


if __name__ == "__main__":

    # Parse input file
    parser = argparse.ArgumentParser(description='Process simulation.log into a LaTeX table')
    parser.add_argument('inputfile', help='file simulation.log')
    parser.add_argument('outputfile', help='output tex file')#, default="simtab.tex")
    #parser.add_argument('caption', help='table caption')
    #parser.add_argument('label', help='table label')
    args = parser.parse_args()

    # Get simulation names from parfile names
    ln = fgrep(args.inputfile,"%% Parfile:")
    simname = [i.split(' ')[2] for i in ln] # extract filenames
    simname = [i.split('.')[0] for i in simname] # rm '.par'
    simname = [i.replace('_', '\_') for i in simname]
                
    # Get cluster names
    ln = fgrep(args.inputfile,"%% Run:")
    cluster = [i.split(' ')[2] for i in ln] 
    
    # Get data location --- NOT YET FILLED
    #ln = fgrep(args.inputfile,"%% Data:")
    #data = [i.split(' ')[2] for i in ln] 

    # Get who is running
    ln = fgrep(args.inputfile,"%% Runner:")
    who = [i.split(' ')[2] for i in ln] 

    # Get who is running
    ln = fgrep(args.inputfile,"%% Status:")
    status = [i.split(' ')[2] for i in ln] 

    # Sort
    #TODO
    
    # Now make a table
    #maketab([["a","b","c"],["d","f","g"]],"test.tex",captxt="") # this is a test
    #ln = [simname,cluster,status,who,data]
    ln = [simname,cluster,status,who]
    maketab(map(list, zip(*ln)),args.outputfile)    

    # cat simtab.tex | pandoc -t latex -o simtab.pdf 

