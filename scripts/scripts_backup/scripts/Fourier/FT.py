#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# To be done : Add treatment for reding comments 
"""
Created on Fri Jun  7 12:50:33 2019

@author: aviralprakash
"""

import numpy as np
import scipy
from scipy import signal

import os
import sys
import argparse

# Parse CLI
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default="./",help="Path to waveforms folder containing strain (l,m) in c = G = 1 units")
parser.add_argument("-o", "--outdir", default="Fourier", help="Output Directory")

args = parser.parse_args()

# Methods for constructing the High Pass Butterworth Filter (copied)

def butter_highpass(cutoff,nyq,order = 5):
    normal_cutoff = cutoff/nyq
    b, a = scipy.signal.butter(order,normal_cutoff,btype = 'high',analog = False)
    return b, a


def butter_highpass_filter(data,cutoff,nyq,order = 5):
    b, a = butter_highpass(cutoff,nyq,order = order)
    y = scipy.signal.filtfilt(b, a,data)
    return y


# Files
str1 = open(str(args.input)+"Fourier/h_t_SI.dat","r") # file to be read
str2 = open(str(args.outdir)+"/DFT.dat","w") # file to write onto
str3 = open(str(args.outdir)+"/fit_data.dat","w") # File containing Tukey + Filter Spectrum data to be fitted


# Declarations
time = [] # Time data
h_plus_t = [] # h_+(t)
h_cross_t = [] # h_x(t)
freq = [] # f from 0 to 2.* fN
h_plus_f = [] # h_+(f) (Untukeyed)
h_cross_f = [] # h_x(f) (Untukeyed)
tukey = [] # Tukey Window generation for  h(t)
tukey_h_plus_t = [] # Tukeyed  h_+(t)
tukey_h_cross_t = [] # Tukeyed h_x(t)
h_f = [] # h(f) = Sqrt [((|h_+|^2)+(|h_x|^2))/2]
tukey_h_plus_f = [] # Fourier Transform of Tukeyed h_+(t)
tukey_h_cross_f = [] # Fourier Transform of Tukeyed h_x(t)
tukey_h_f = [] # h(f) for Tukeyed Signals.
filt_h_plus_t = [] # Tukeyed + Butterworth h_+(t)
filt_h_cross_t = [] # Tukeyed + Butterworth h_x(t)
filt_h_plus_f = [] # FT of TUKEYED + Butterworth h_+(t)
filt_h_cross_f = [] # FT of Tukeyed + Butterworth h_x(t)
filt_h_f = [] # h(f) for Tukeyed + Butterworth Signals



# READ time, h_+(t) and h_x(t)
for l in str1.readlines():
    if l.strip():
        v1 = float(l.split()[0])
        v2 = float(l.split()[1])
        v3 = float(l.split()[2])
        time.append(v1)
        h_plus_t.append(v2)
        h_cross_t.append(v3)


# Generating frequency series
T = time[len(time)-1] # The total time duration of the signal
df = 1./T # Step Size in Frequency in Hz
dt = time[1]-time[0] # Step size in time
fN = 1./(2.*dt)  # Nyquist Frequency in Hz
N = 2.*fN/df # This is an integer: the total number of sampling points in frequency
freq.insert(0,0.0)
j = 1
while j<= int(N):
    freq.insert(j,(freq[j-1]+df))
    j = j+1


# Applyig the Tukey Window
tukey = scipy.signal.tukey(len(h_plus_t),0.25,False) # Tukey window generation
tukey_h_plus_t = h_plus_t * tukey # Applying the Tukey Window to the time series waveform h_+
tukey_h_cross_t = h_cross_t * tukey # Applying the Tukey Window to the time series waveform h_x


# Taking Fourier transforms
h_plus_f = np.abs(np.fft.rfft(h_plus_t,int(N)))*dt
h_cross_f = np.abs(np.fft.rfft(h_cross_t,int(N)))*dt
tukey_h_plus_f = np.abs(np.fft.rfft(tukey_h_plus_t,int(N)))*dt
tukey_h_cross_f = np.abs(np.fft.rfft(tukey_h_cross_t,int(N)))*dt


# Applying High Pass Filter and Fourier Transforms
filt_h_plus_t = butter_highpass_filter(tukey_h_plus_t,800,fN,5) # Butterworth Filter on Tukeyed h_+(t) cutoff = 0.80kHz
filt_h_cross_t = butter_highpass_filter(tukey_h_cross_t,800,fN,5) # Butterworth Filter on Tukeyed h_x(t)
filt_h_plus_f = np.abs(np.fft.rfft(filt_h_plus_t,int(N)))*dt
filt_h_cross_f = np.abs(np.fft.rfft(filt_h_cross_t,int(N)))*dt



# Power spectral Density calculations
h_f = (((h_plus_f*h_plus_f) + (h_cross_f*h_cross_f))/2.)**(0.5)
tukey_h_f = (((tukey_h_plus_f*tukey_h_plus_f) + (tukey_h_cross_f*tukey_h_cross_f))/2.)**(0.5)
filt_h_f = (((filt_h_plus_f*filt_h_plus_f) + (filt_h_cross_f*filt_h_cross_f))/2.)**(0.5)


# Writing the spectral data
i = 0
while i<=int(N):
    str2.write(str(freq[i])+"\t\t"+str(2.*h_f[i]*(freq[i]**(0.5)))+"\t\t" + str(2.*tukey_h_f[i]*(freq[i]**(0.5)))+"\t\t"+str(2.*filt_h_f[i]*(freq[i]**(0.5)))+"\n")
    str3.write(str(freq[i]/1000.)+"\t\t"+str(2.*filt_h_f[i]*(freq[i]**(0.5)))+"\n") # Write spectral data in kHz easy fit
    i = i+1


str1.close()
str2.close()
str3.close()


