#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 13:00:35 2019

@author: aviralprakash
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 23:16:32 2019

@author: aviralprakash
"""

import numpy as np
import scipy
from scipy import optimize
import math
import integrate





file1 = open("fit_data.dat","r")
file2 = open("fit_plot.dat","w")
file3 = open("params_data.dat","w")

def S(f,A1,F1,W1,A2a,A2b,F2,W2,s,A2G,F2G,W2G,A0,F0,W0):
    y = ((A1*np.exp(-((f-F1)**2)/(W1**2))) + ((A2G*np.exp(-((f-F2G)**2)/(W2G**2))) +((((A2b-A2a)*(f-F2))+((W2)*(A2b+A2a)))/(2*W2*((1+np.exp(-(f-F2+W2)/s))*(1+np.exp((f-F2-W2)/s)))))) + (A0*np.exp(-((f-F0)**2)/(W0**2)))) 
    return y 

def S2(f,A2a,A2b,F2,W2,s,A2G,F2G,W2G):
    z = ((A2G*np.exp(-((f-F2G)**2)/(W2G**2))) +((((A2b-A2a)*(f-F2))+((W2)*(A2b+A2a)))/(2*W2*((1+np.exp(-(f-F2+W2)/s))*(1+np.exp((f-F2-W2)/s))))))
    return z


freq = []
h_f = []
popt = []
pcov = []
perr = [] # Computes 1 sigma (S.D.) errors on the parameters
p0 = []
p0 = [1,2.38,1,1,1,1,1,1,1,1,1,1,2.05,1]

# READ frequency, h(f)
for l in file1.readlines():
    if l.strip():
        v1 = float(l.split()[0])
        v2 = float(l.split()[1])
        freq.append(v1)
        h_f.append(v2)
        
popt, pcov = scipy.optimize.curve_fit(S,freq,h_f,p0,maxfev = 60000)
print(popt)

perr = np.sqrt(np.diag(pcov))
file3.write("\n" + "The Parameter list is: \n" + str(popt))
file3.write("\n" + "The 1-sigma errors in the parameter are: \n" + str(perr))
file3.write("\n" + "The Covariance matrix is: \n" + str(pcov))


class parameters:
    A1 = popt[0]
    F1 = popt[1]
    W1 = popt[2]
    A2a = popt[3]
    A2b = popt[4]
    F2 = popt[5]
    W2 = popt[6]
    s = popt[7]
    A2G = popt[8]
    F2G = popt[9]
    W2G = popt[10]
    A0 = popt[11]
    F0 = popt[12]
    W0 = popt[13]

p = parameters() #  Constructor Call for parameter type object



i = 0.5
while i<=3.50:
    file2.write(str(i) + "\t\t" + str(S(i,popt[0],popt[1],popt[2],popt[3],popt[4],popt[5],popt[6],popt[7],popt[8],popt[9],popt[10],popt[11],popt[12],popt[13])) + "\n")
    i = i+0.001

Nmax = 10000
# Calculating the peak frequency at HMNS

def Integrand_num(f,p):
    return (f*S2(f,p.A2a,p.A2b,p.F2,p.W2,p.s,p.A2G,p.F2G,p.W2G))

def Integrand_den(f,p):
    return(S2(f,p.A2a,p.A2b,p.F2,p.W2,p.s,p.A2G,p.F2G,p.W2G))

f2=((integrate.integral(Nmax,0.,20.,Integrand_num,p))/(integrate.integral(Nmax,0.,20.,Integrand_den,p)))

file3.write("\n\n\n" + "f2 = " + str(f2))

file1.close()
file2.close()
file3.close()
    
