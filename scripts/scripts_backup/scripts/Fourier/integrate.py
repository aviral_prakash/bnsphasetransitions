""" File : integrate.py 

Programmer : Aviral Prakash ph16c009@smail.iitm.ac.in

Version : Original 

Commments : 28 September 2017 :Integrator
								"""
import math

import numpy as np

def integral(npts,a,b,my_func,params):

	sums = 0.0 #The net result of the numerical integral 

	df = ((b-a)/(npts)) # Step size

	i = a

	while (i<=b):

		sums += my_func(i,params) * df

		i += df
 
	return sums


	
