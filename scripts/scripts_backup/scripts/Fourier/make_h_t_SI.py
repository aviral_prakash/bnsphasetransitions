"""
    NAME: Aviral Prakash aup1075@psu.edu
    
    DATE: 05/30/2019
    
    COMMENTS: The code generates a time-series signal in file h_t.txt from data.h5 BNS simulations from
    http://www.computational-relativity.org/gwdb/
    
    
    REVISION HISTORY: 07/09/2019 Implemented subsequntly for McLachlan simulations by Takami and Rezzolla.
    
    """

import numpy as np
import scipy
import csv

import os
import sys
import argparse

# Parse CLI

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default="./",help="Path to waveforms folder containing strain (l,m) in c = G = 1 units")
parser.add_argument("-o", "--outdir", default="Fourier", help="Output Directory")

args = parser.parse_args()

# Making output Folder

if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)

str1 = open(str(args.input)+"waveforms/strain_l2_m2.dat","r") # Source file for simulation in geometric units
#i.e. c = G = 1
str2 = open(str(args.outdir)+"/h_t_SI.dat","w") # h_t data in SI units

t, h_p, h_c = [], [], [] # time, h_+ and h_x data to be rescaled from Geometrical to SI

# Rescaling is necessary because data from the simulations is in geometrical units where G = c = 1 and
# everything is expressed in Solar mass ! time in t/M
MSun = 1.9889 * 10**(30) # 1 solar mass in kg
MTSun = 4.92549 * 10**(-6) # The conversion factor for one solar mass to seconds *)
#M = 2.7 # Total mass of the binary in solar mass units
MLSun = 1.47670 * 10**(3) # The conversion factor for one solar mass to length in m
mpc = 3.24078 * 10**(-17) # One meter to parsec

# Reading the source simulation file
for line in str1:
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t.append(values[0])
    h_p.append(values[1])
    h_c.append(values[2])

# The following prints the actual data in SI i.e. strain in 10^(-21) and time in s
i = 0
while i<len(t):
    str2.write(str(t[i] * MTSun)+"\t"+str(h_p[i] * ((MLSun) / (50.*(10.**6.)/mpc)))+"\t"+str(h_c[i] * ((MLSun) / (50.*(10.**6.)/mpc)))+ "\n" )
    # The writing scheme is t   h_+     h_x
    i = i+1
str1.close()
str2.close()

