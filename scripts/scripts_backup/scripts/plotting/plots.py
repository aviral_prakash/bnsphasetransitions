# The script makes diagnostic 1-D plots 

import numpy as np

import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt 

import scidata.units as ut

import os 
import sys 
import argparse

import utils as util # To calculate the tmerg (retarded time) for rho,lapse, T everything except strain as 
# gwS are delayed while travelling to the detector

utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms 
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)/1e15 # density in cgs X 1e15

# Parsing the CLI

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default="./", help="Path to Simulation data archived in cfs")
parser.add_argument("-o", "--outdir", default="Plots", help="Output Directory")

args = parser.parse_args()

# Making Output Folder 

if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)


#######################   1.259 1.482 M0 LR ##################

########################## Strain (2,2) #######################
####### Hadron part #######
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M12591482_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M12591482_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M12591482_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M12591482_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-4.0, 10.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.259 Mo and 1.482 Mo")
plt.savefig(str(args.outdir)+'/strain_1259_1482_M0_LR.png', dpi=1200)
plt.close()



############### 1.30 1.30 ##############

####### Hadron part #######
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M130-130_M0_LR/complete/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M130-130_M0_LR/complete/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M130-130_M0_LR/complete/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M130-130_M0_LR/complete/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-3.0, 17.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.30 Mo and 1.30 Mo")
plt.savefig(str(args.outdir)+'/strain_130_130_M0_LR.png', dpi=1200)
plt.close()


################ 1.3325 and 1.3325 Mo ############
####### Hadron part #######
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M13325-13325_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M13325-13325_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M13325-13325_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M13325-13325_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-4.0, 15.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.3325 Mo and 1.3325 Mo")
plt.savefig(str(args.outdir)+'/strain_13325_13325_M0_LR.png', dpi=1200)
plt.close()

############# 1.40 1.40 Mo #########
#### Hadron Part #########
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M140-140_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M140-140_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M140-140_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M140-140_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-4.0, 7.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.40 Mo and 1.40 Mo")
plt.savefig(str(args.outdir)+'/strain_140_140_M0_LR.png', dpi=1200)
plt.close()


######### 1.365 1.365 Mo #######
####### Hadron Part ######
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M13651365_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M13651365_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M13651365_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M13651365_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-4.0, 7.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.365 Mo and 1.365 Mo")
plt.savefig(str(args.outdir)+'/strain_1365_1365_M0_LR.png', dpi=1200)
plt.close()


#################### 1.020 1.856 Mo ########################### 
##### Strain 2,2
##### Hadronic part 
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M10201856_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())*utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M10201856_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[0]*utime - tmerg_h)
    hp_h.append(values[1])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M10201856_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())*utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M10201856_M0_LR/waveforms/strain_l2_m2.dat','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[0]*utime - tmerg_q)
    hp_q.append(values[1])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-15.0, 10.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("strain (2,2) (c = G = 1)")
plt.title("1.020 Mo and 1.856 Mo")
plt.savefig(str(args.outdir)+'/strain_1020_1856_M0_LR.png', dpi=1200)
plt.close()

############## 1.3325 1.3325  ##########
#####rho 
#### Hadron part 
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M13325-13325_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime 
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M13325-13325_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2]*udens)
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M13325-13325_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime
t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M13325-13325_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2]*udens)

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-5.0, 10.1)
plt.ylim(0.5, 2.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.3325 Mo and 1.3325 Mo")
plt.savefig(str(args.outdir)+'/rho_13325_13325_M0_LR.png', dpi=1200)
plt.close()
############################## 1.30 1.30 ################################################
#####rho 
#### Hadron part 
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M130-130_M0_LR/complete/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M130-130_M0_LR/complete/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2]*udens)
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M130-130_M0_LR/complete/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M130-130_M0_LR/complete/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2]*udens)

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-2.0, 10.0)
plt.ylim(0.5, 2.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.30 Mo and 1.30 Mo")
plt.savefig(str(args.outdir)+'/rho_130_130_M0_LR.png', dpi=1200)
plt.close()


###################### 1.365 1.365 ###########################
#### rho
##### Hadronic Part 
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M13651365_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M13651365_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2]*udens)
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M13651365_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime
t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M13651365_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2]*udens)

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-5.0, 4.9)
plt.ylim(0.0, 2.2)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.365 Mo and 1.365 Mo")
plt.savefig(str(args.outdir)+'/rho_1365_1365_M0_LR.png', dpi=1200)
plt.close()



################################## 1.40 1.40 Mo ############################################

#### rho
##### Hadronic Part
##### Calculating tmerg

for line in open(str(args.input)+'BLh_M140-140_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M140-140_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2]*udens)
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M140-140_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M140-140_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2]*udens)

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-2.5,2.0)
plt.ylim(0.0, 2.0)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.40 Mo and 1.40 Mo")
plt.savefig(str(args.outdir)+'/rho_140_140_M0_LR.png', dpi=1200)
plt.close()


################### 1.259 1.482 ##############
#### rho
###### Hadronic Part 
#### Calc tmerg
for line in open(str(args.input)+'BLh_M12591482_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M12591482_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2]*udens)
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M12591482_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime

t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M12591482_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2]*udens)

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-2.5, 3.0)
plt.ylim(0.0, 2.5)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.259 Mo and 1.482 Mo")
plt.savefig(str(args.outdir)+'/rho_1259_1482_M0_LR.png', dpi=1200)
plt.close()

############################ 1.020 1.856 ######################

#### rho
##### Hadronic Part 
##### Calculating tmerg
for line in open(str(args.input)+'BLh_M10201856_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_h=float(line.strip())

tmerg_h = util.get_retarded_time(tmerg_h) * utime
##### Diagnostic plot #######
t_h, hp_h = [], []
for line in open(str(args.input)+'BLh_M10201856_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_h.append(values[1]*utime - tmerg_h)
    hp_h.append(values[2])
##########Quark Part ##############
for line in open(str(args.input)+'BLQ_M10201856_M0_LR/waveforms/tmerger.dat','r'):
    tmerg_q=float(line.strip())

tmerg_q = util.get_retarded_time(tmerg_q) * utime
t_q, hp_q = [], []
for line in open(str(args.input)+'BLQ_M10201856_M0_LR/collated/rho.maximum.asc','r'):
    li = line.strip()
    if li.startswith("#"):
        continue
    values = [float(s) for s in line.split()]
    t_q.append(values[1]*utime - tmerg_q)
    hp_q.append(values[2])

plt.plot(t_h, hp_h,'r', t_q, hp_q, 'b')
plt.xlim(-5.0, 1.0)
plt.ylim(0.0,0.005)
plt.legend(["BLh", "BLQ"], loc="upper right")
plt.xlabel("t - t_merg (ms)")
plt.ylabel("rho X 1e15")
plt.title("1.020 Mo and 1.856 Mo")
plt.savefig(str(args.outdir)+'/rho_1020_1856_M0_LR.png', dpi=1200)
plt.close()


