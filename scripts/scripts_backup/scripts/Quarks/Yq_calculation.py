#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 18:37:25 2020

@author: aviralprakash
"""
# Import all necessary modules 
import numpy as np
from scipy.interpolate import RegularGridInterpolator

import matplotlib.pyplot as plt 

import scidata.carpet.hdf5 as h5
import scidata.units as ut
import h5py as h

import os
import sys 
import argparse


# Parse CLI ---Command Line Interface
# Arguments preceeded by - are CLI options for the bash script analyse.sh
# Arguments preceeded by -- are CLI options for the command line when you call this script from CL inside output-????/data with a copy of the quark data 
# Argument names following dest= are for use within this script as args.<dest_name>

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", default=".", help="Path to rho, T, Ye h5 data")
parser.add_argument("-o", "--outdir", default = "Quarks", help = "Output folder")

parser.add_argument("-log_rho-min", "--log_rho-min", dest="log_rho_min", type=float, default=11.5, help="Minimum log_rho with rho in g/cc (default = 11.5)")
parser.add_argument("-log_rho-max", "--log_rho-max", dest="log_rho_max", type=float, default=15.4, help="Maximum log_rho with rho in g/cc (default = 15.4)")
parser.add_argument("-log_rho-cmap","--log_rho-cmap", dest="log_rho_cmap", default="jet", help="log_rho color map")

parser.add_argument("-Temp-min", "--T-min", dest="T_min", type=float, default=0.0, help="Minimum T in MeV (default = 0.0)")
parser.add_argument("-Temp-max", "--T-max", dest="T_max", type=float, default=60.0, help="Maximum T in MeV (default = 60.0)")
parser.add_argument("-Temp-cmap", "--T-cmap", dest="T_cmap",  default="hot", help="T color map")

parser.add_argument("-Ye-min", "--Ye-min", dest="Ye_min", type=float, default=0.07, help="Minimum Ye (default = 0.07)")
parser.add_argument("-Ye-max", "--Ye-max", dest="Ye_max", type=float, default=0.15, help="Maximum Ye (default = 0.15)")
parser.add_argument("-Ye-cmap", "--Ye-cmap", dest="Ye_cmap", default="gnuplot", help="Ye color map")

parser.add_argument("-Yq-min", "--Yq-min", dest="Yq_min", type=float, default=0.0, help="Minimum Yq (default = 0.0)")
parser.add_argument("-Yq-max", "--Yq-max", dest="Yq_max", type=float, default=0.15, help="Maximum Yq (default = 0.15)")
parser.add_argument("-Yq-cmap", "--Yq-cmap", dest="Yq_cmap", default="CMRmap", help="Yq color map")

args = parser.parse_args()


# Creating output Folders

if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)
for subdir in ["rho", "T", "Ye", "Yq"]:
    dpath = os.path.join(args.outdir, subdir)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)

# Conversion to cgs units 
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)    #g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1) * 1e3  # time in ms
    
#############################    Yq Calculation      ###################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

# The above was done by exploring Domenico's Table 

Yq = 1. - comp[3] ## Domenico's convention is opposite to this in his table 
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
###########################################################################################

# Reading 2D slices for rho, T and Ye from simulation output
dset_rho = h5.dataset(str(args.input)+"/rho.xy.h5")
dset_T = h5.dataset(str(args.input)+"/temperature.xy.h5")
dset_Ye = h5.dataset(str(args.input)+"/Y_e.xy.h5")

######## pLotting rho for all iterations ##############
print("\n" + "Processing 2D slices for rho ... ")
k = 1
for i in (dset_rho.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset_rho.iterations))+" ...")
    grid = dset_rho.get_grid(iteration=i)
    rho = dset_rho.get_grid_data(grid, iteration = i)
    T = dset_T.get_grid_data(grid, iteration = i)
    Ye = dset_Ye.get_grid_data(grid, iteration = i)
    x, y = grid[-1].mesh()

    plt.pcolormesh(x, y, np.log10(rho[-1] * udens), cmap = args.log_rho_cmap)
    plt.clim(args.log_rho_min, args.log_rho_max)
    clb = plt.colorbar(label = 'log rho')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title("t = " + str(dset_rho.get_time(iteration = i) * utime) + " ms")
    plt.savefig(os.path.join(args.outdir,"rho",str(i)+".png"))
    clb.remove()
    plt.close()
    k = k + 1
    print("done")

######## pLotting T for all iterations ##############
print("\n" + "Processing 2D slices for T ... ")
k = 1
for i in (dset_rho.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset_rho.iterations))+" ...")
    grid = dset_rho.get_grid(iteration=i)
    rho = dset_rho.get_grid_data(grid, iteration = i)
    T = dset_T.get_grid_data(grid, iteration = i)
    Ye = dset_Ye.get_grid_data(grid, iteration = i)
    x, y = grid[-1].mesh()
    
    plt.pcolormesh(x, y, T[-1], cmap = args.T_cmap)
    plt.clim(args.T_min, args.T_max)
    clb = plt.colorbar(label = 'T (MeV)')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title("t = " + str(dset_rho.get_time(iteration = i) * utime) + " ms")
    plt.savefig(os.path.join(args.outdir,"T",str(i)+".png"))
    clb.remove()
    plt.close()
    k = k + 1
    print("done")


######## pLotting Ye for all iterations ##############
print("\n" + "Processing 2D slices for Ye ... ")
k = 1
for i in (dset_rho.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset_rho.iterations))+" ...")
    grid = dset_rho.get_grid(iteration=i)
    rho = dset_rho.get_grid_data(grid, iteration = i)
    T = dset_T.get_grid_data(grid, iteration = i)
    Ye = dset_Ye.get_grid_data(grid, iteration = i)
    x, y = grid[-1].mesh()
    
    plt.pcolormesh(x, y, Ye[-1], cmap = args.Ye_cmap)
    plt.clim(args.Ye_min, args.Ye_max)
    clb = plt.colorbar(label = 'Ye')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title("t = " + str(dset_rho.get_time(iteration = i) * utime) + " ms")
    plt.savefig(os.path.join(args.outdir,"Ye",str(i)+".png"))
    clb.remove()
    plt.close()
    k = k + 1
    print("done")

##### Plotting Yq for all iterations ##########
print("\n" + "Processing 2D slices for Yq ... ")
k = 1
for i in (dset_rho.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset_rho.iterations))+" ...")
    grid = dset_rho.get_grid(iteration=i)
    rho = dset_rho.get_grid_data(grid, iteration = i)
    T = dset_T.get_grid_data(grid, iteration = i)
    Ye = dset_Ye.get_grid_data(grid, iteration = i)
    x, y = grid[-1].mesh()


    x_new = x[:,0]
    y_new = y[0,:]
    X, Y = np.meshgrid(x_new, y_new, sparse = True)
    Yq_calc_array = np.zeros((len(x_new) * len(y_new)))
    Yq_calc_array = Yq_calc_array.reshape((len(y_new), len(x_new)))

    for ii in range(0, len(x_new)):
        for j in range(0, len(y_new)):
            Yq_calc_array[j, ii] = Yq_calc(np.log10(rho[-1][ii, j] * udens),T[-1][ii, j],Ye[-1][ii, j])
    
    #cnt = plt.contour(x, y,rho[-1], colors = 'green', linewidths=1.5)
    cnt1 = plt.contour(x, y, T[-1], colors = 'yellow', linewidths=1.5)
    #plt.clabel(cnt, inline=1, fontsize = 6.0)
    plt.clabel(cnt1, inline=1, fontsize = 6.0)
    plt.pcolormesh(X, Y, Yq_calc_array[:,:], cmap = args.Yq_cmap)
    plt.clim(args.Yq_min, args.Yq_max)
    clb = plt.colorbar(label = 'Yq')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title("t = " + str(dset_rho.get_time(iteration = i) * utime) + " ms")
    plt.savefig(os.path.join(args.outdir,"Yq",str(i)+".png"), dpi = 1200)
    clb.remove()
    plt.close()
    k = k + 1
    print("done")

