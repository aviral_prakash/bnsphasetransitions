#!/usr/bin/env bash

if [ -z "$1" ]; then 
    echo "Usage: $0 /path/to/segments"
    exit 1 

fi 
target=$1
cd $target || exit 1

# Copying the Quark Fraction table to the calling simulation folder. On a different machine please modify this address  

echo "Copying Quark table to local repo"
cp -v ~/scripts/Quarks/comp_BLh_180_0.35.hdf5 $target

# Calling the Quark Fraction Calculation for all segments 
for f in output-????/data; do 
	if [ ! -f ${f}/quarks.done ]; then
		echo "For segment ${f}:" 
		python -u ~/scripts/Quarks/Yq_calculation.py -i ${f} \
			-log_rho-min 11.5 -log_rho-max 14.5 -log_rho-cmap "jet" \
			-Temp-min 0.0 -Temp-max 60.0 -Temp-cmap "hot" \
			-Ye-min 0.0 -Ye-max 0.20 -Ye-cmap "gnuplot" \
			-Yq-min 0.0 -Ye-max 0.15 -Yq-cmap "CMRmap"  || continue 1
		python -u ~/scripts/Quarks/Yq_max_calculation.py -i ${f} || continue 1  
		touch ${f}/quarks.done
	fi 
done
touch quarks.done

# Removing the Quark table as it is no longer required
echo "Deleting Quark Table" 
rm -rf $target/comp_BLh_180_0.35.hdf5
