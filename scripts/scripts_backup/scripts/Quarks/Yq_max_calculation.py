#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 18:37:25 2020

@author: aviralprakash
"""
# Import all necessary modules 
import numpy as np
from scipy.interpolate import RegularGridInterpolator

import os  
import sys 
import argparse

import matplotlib.pyplot as plt 

import scidata.carpet.hdf5 as h5
import scidata.units as ut
import h5py as h

# Parse CLI -- Command Line Interface 
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", default=".", help="Path to rho, T, Ye h5 data")
args = parser.parse_args()

# Variables in CGS units 
udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # density in g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3 # time in ms 

print("\n Calculating Yq_maximum ...")
################################################### Yq Calculation ##################################################

data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01


Yq = 1. - comp[3] ## Oppisite to Domenico's convention 
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))

######################################################################################################################

# Reading 2d slices for rho, T and Ye 
dset_rho = h5.dataset(str(args.input)+"/rho.xy.h5")
dset_T = h5.dataset(str(args.input)+"/temperature.xy.h5")
dset_Ye = h5.dataset(str(args.input)+"/Y_e.xy.h5")

str1 = open(str(args.input)+"/Yq.maximum.asc","w")

for i in (dset_rho.iterations):
    grid = dset_rho.get_grid(iteration=i)
    rho = dset_rho.get_grid_data(grid, iteration = i)
    T = dset_T.get_grid_data(grid, iteration = i)
    Ye = dset_Ye.get_grid_data(grid, iteration = i)
    x, y = grid[-1].mesh()


    x_new = x[:,0]
    y_new = y[0,:]
    X, Y = np.meshgrid(x_new, y_new, sparse = True)
    Yq_calc_array = np.zeros((len(x_new) * len(y_new)))
    Yq_calc_array = Yq_calc_array.reshape((len(y_new), len(x_new)))

    for ii in range(0, len(x_new)):
        for j in range(0, len(y_new)):
            Yq_calc_array[j, ii] = Yq_calc(np.log10(rho[-1][ii, j]*udens),T[-1][ii, j],Ye[-1][ii, j])
        
    
    str1.write(str(dset_rho.get_time(iteration = i)*utime) + "\t\t" + str(Yq_calc_array.max()) + "\n")
str1.close()

