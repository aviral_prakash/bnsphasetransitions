#!/usr/bin/env python
# Computes the PSD of the real part of the strain

import numpy as np
from scidata.utils import ilocate
from scipy.fftpack import fft
from scipy.fftpack.helper import fftfreq
import sys
import units as ut
import os

# Parse CLI
if len(sys.argv) != 2 or not os.path.isdir(sys.argv[1]):
    print("Usage: {0} /path/to/strain.dat".format(sys.argv[0]))
    exit(1)
target = sys.argv[1]

# Read strain
for ifname in ilocate("strain_l?_m?.dat", root=target):
    print("Processing {}...".format(os.path.basename(ifname))),

    rawdata = np.loadtxt(ifname, unpack=True)
    t = ut.conv_time(ut.cactus, ut.cgs, rawdata[0])
    h = rawdata[1] + 1j*rawdata[2]
    A = np.abs(h)
    tmrgr = t[np.argmax(A)]
    del A

    dt = t[1]-t[0]
    f = fftfreq(h.shape[0], dt)
    hhat = dt*(np.abs(fft(np.real(h))) + 1j*np.abs(fft(np.imag(h))))

    ofname = os.path.dirname(ifname) + "/psd_" + \
             os.path.basename(ifname.replace("strain_", ""))
    ofile = open(ofname, "w")
    ofile.write("# 1:freq 2:Re 3:Im\n")
    for i in xrange(f.shape[0]):
        if f[i] > 0:
            ofile.write("{} {} {}\n".format(f[i], hhat[i].real, hhat[i].imag))
    ofile.close()

    print("done!")
