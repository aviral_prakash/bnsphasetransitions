#!/usr/bin/env bash

function call {
    echo $*
    eval $*
}

ANALYSIS_HOME=$(cd $(dirname $0); pwd)
export PYTHONPATH=${ANALYSIS_HOME}/../modules:${PYTHONPATH}

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/simulation"
    exit 1
fi
target=$1

cd $target

call mkdir -p waveforms || exit 1
call cd waveforms
call python ${ANALYSIS_HOME}/strain.py ../collated || exit 1
call python ${ANALYSIS_HOME}/postmerger.py || exit 1
call python ${ANALYSIS_HOME}/convert.py || exit 1
call python ${ANALYSIS_HOME}/spectra.py . || exit 1
call python ${ANALYSIS_HOME}/fpeak.py . || exit 1
call cd ..

call touch gw.done
