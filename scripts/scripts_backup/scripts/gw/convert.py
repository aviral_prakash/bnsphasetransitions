#!/usr/bin/env python

import numpy as np
import sys
import units as ut

Msun_cm = ut.conv_length(ut.cactus, ut.cgs, 1)

t, hp, hx = np.loadtxt("strain_l2_m2.dat", usecols=(0,1,2), unpack=True)
t = ut.conv_time(ut.cactus, ut.cgs, t)
hx *= (-1.0)

dtnew = 1.0/16384.0
tnew = np.arange(t.min(), t.max() + dtnew, dtnew)

hp = np.interp(tnew, t, hp, right=0.0)*Msun_cm
hx = np.interp(tnew, t, hx, right=0.0)*Msun_cm

ofile = open("waveform_l2_m2.dat", "w")
ofile.write("# 1:time 2:h+ 3:hx\n")
for i in range(tnew.shape[0]):
    ofile.write("{} {} {}\n".format(tnew[i], hp[i], hx[i]))
del ofile
