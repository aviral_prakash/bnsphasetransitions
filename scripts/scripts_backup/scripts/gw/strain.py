#!/usr/bin/env python

# Import data analysis tools
import math
from math import pi
import numpy as np
import os
from pprint import pprint
import re
import scidata.multipole as multipole
from scidata.utils import diff, fixed_freq_int_1, fixed_freq_int_2, integrate
from scidata.windows import exponential as window
from scipy.signal import detrend
import sys

# Matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# Options
options = {
    "cutoff.freq"     : 0.002,
    "detector.radius" : 400.0,
    'window.delta'    : 200.0,
}


# Factor to account for negative m modes
def factor(m):
    return 1 if m == 0 else 2


# Parse CLI
if len(sys.argv) != 2 or not os.path.isdir(sys.argv[1]):
    print("Usage: %s /path/to/multipole/data" % sys.argv[0])
    exit(1)
data_path = sys.argv[1]
output_path = "."

# Read Psi4
dset = multipole.dataset(basedir=data_path, ignore_negative_m=True)
Psi4 = {}
for l, m in dset.modes:
    print("Reading (%d,%d)..." % (l,m)),
    t, Psi4[(l,m)] = dset.get(var="Psi4", l=l, m=m, r=options["detector.radius"])
    Psi4[l,m] *= options['detector.radius']
    print("done!")
dtime = t[1] - t[0]


# -------------------------------------------------------
# Compute the strain
# -------------------------------------------------------
# Window function
win = window(t/t.max(), delta=options["window.delta"]/t.max())

# Strain from direct integration
h_2i = {}

# Strain from fixed-frequency integration
h_ff = {}

A_ff = {}
phi_ff = {}
omega_ff = {}

h_dot_ff = {}
E_GW_dot_ff = {}
E_GW_ff = {}
J_GW_dot_ff = {}
J_GW_ff = {}

# Compute strain, energy and angular momentum of each mode
for l, m in dset.modes:
    psi4_lm = Psi4[l,m] * win
    h_2i[(l,m)] = detrend(integrate(integrate(psi4_lm)))*(dtime)**2

    # Note: we also window the integrated waveform to suppress noise
    # at early times (useful to make hybrid waveforms)
    h_ff[(l,m)] = win * fixed_freq_int_2(psi4_lm,
            2*options["cutoff.freq"]/max(1,abs(m)), dt=dtime)

    A_ff[(l,m)] = np.abs(h_ff[l,m])
    phi_ff[(l,m)] = -np.unwrap(np.angle(h_ff[l,m]))
    omega_ff[(l,m)] = diff(phi_ff[l,m])/diff(t)

    # Note: we also window the integrated waveform to suppress noise
    # at early times (useful to make hybrid waveforms)
    h_dot_ff[(l,m)] = win * fixed_freq_int_1(psi4_lm,
            2*options["cutoff.freq"]/max(1,abs(m)), dt=dtime)
    E_GW_dot_ff[(l,m)] = 1.0/(16.*pi) * np.abs(h_dot_ff[l,m])**2
    E_GW_ff[(l,m)] = integrate(E_GW_dot_ff[l,m]) * dtime
    J_GW_dot_ff[(l,m)] = 1.0/(16.*pi) * m * np.imag(h_ff[l,m] * np.conj(h_dot_ff[l,m]))
    J_GW_ff[(l,m)] = integrate(J_GW_dot_ff[l,m]) * dtime


# -------------------------------------------------------
# Compute the GW energy and angular momentum
# -------------------------------------------------------
E_GW_dot = {}
E_GW = {}
J_GW_dot = {}
J_GW = {}
for m in dset.mmodes:
    E_GW_dot[m] = np.zeros_like(t)
    E_GW[m] = np.zeros_like(t)
    J_GW_dot[m] = np.zeros_like(t)
    J_GW[m] = np.zeros_like(t)
for l, m in dset.modes:
    E_GW_dot[m] += factor(m) * E_GW_dot_ff[l,m]
    E_GW[m] += factor(m) * E_GW_ff[l,m]
    J_GW_dot[m] += factor(m) * J_GW_dot_ff[l,m]
    J_GW[m] += factor(m) * J_GW_ff[l,m]

E_GW_dot_all = np.zeros_like(t)
E_GW_all = np.zeros_like(t)
J_GW_dot_all = np.zeros_like(t)
J_GW_all = np.zeros_like(t)
for m in dset.mmodes:
    E_GW_dot_all += E_GW_dot[m]
    E_GW_all += E_GW[m]
    J_GW_dot_all += J_GW_dot[m]
    J_GW_all += J_GW[m]


# -------------------------------------------------------
# Make diagnostic plots
# -------------------------------------------------------
l, m = (2, 1)
plt.figure()
plt.title("Re h(l=%d,m=%d)" % (l,m))
plt.plot(t - dtime, np.real(h_2i[l,m]), label="Direct integration")
plt.plot(t, np.real(h_ff[l,m]), label="Fixed frequency integration")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.legend(loc='best')
plt.savefig(output_path + "/Re_l%d_m%d.png" % (l,m))

plt.figure()
plt.title("Im h(l=%d,m=%d)" % (l,m))
plt.plot(t - dtime, np.real(h_2i[l,m]), label="Direct integration")
plt.plot(t, np.real(h_ff[l,m]), label="Fixed frequency integration")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.legend(loc='best')
plt.savefig(output_path + "/Im_l%d_m%d.png" % (l,m))


l, m = (2, 2)
plt.figure()
plt.title("Re h(l=%d,m=%d)" % (l,m))
plt.plot(t - dtime, np.real(h_2i[l,m]), label="Direct integration")
plt.plot(t, np.real(h_ff[l,m]), label="Fixed frequency integration")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.legend(loc='best')
plt.savefig(output_path + "/Re_l%d_m%d.png" % (l,m))

plt.figure()
plt.title("Im h(l=%d,m=%d)" % (l,m))
plt.plot(t - dtime, np.real(h_2i[l,m]), label="Direct integration")
plt.plot(t, np.real(h_ff[l,m]), label="Fixed frequency integration")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.legend(loc='best')
plt.savefig(output_path + "/Im_l%d_m%d.png" % (l,m))


plt.figure()
plt.title("E_GW_dot_l2")
plt.plot(t, E_GW_dot_all, label='E_GW_dot')
plt.plot(t, 2*E_GW_dot_ff[2,1], label='2 x E_GW_dot(l=2,m=1)')
plt.plot(t, 2*E_GW_dot_ff[2,2], label='2 x E_GW_dot(l=2,m=2)')
plt.gca().set_yscale("log")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.legend(loc='best')
plt.savefig(output_path + "/E_GW_dot_l2.png")

plt.figure()
plt.title("E_GW_l2")
plt.plot(t, E_GW_all, label='E_GW')
plt.plot(t, 2*E_GW_ff[2,1], label='2 x E_GW(l=2,m=1)')
plt.plot(t, 2*E_GW_ff[2,2], label='2 x E_GW(l=2,m=2)')
plt.gca().set_yscale("log")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.legend(loc='best')
plt.savefig(output_path + "/E_GW_l2.png")


plt.figure()
plt.title("E_GW_dot")
plt.plot(t, E_GW_dot_all, label='E_GW_dot')
for m in dset.mmodes:
    plt.plot(t, 2*E_GW_dot[m], label='2 x E_GW_dot(m=%d)' % m)
plt.gca().set_yscale("log")
plt.legend(loc='best')
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.savefig(output_path + "/E_GW_dot.png")
plt.figure()

plt.title("E_GW")
plt.plot(t, E_GW_all, label='E_GW')
for m in dset.mmodes:
    plt.plot(t, 2*E_GW[m], label='2 x E_GW(m=%d)' % m)
plt.gca().set_yscale("log")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.legend(loc='best')
plt.savefig(output_path + "/E_GW.png")

plt.figure()
plt.title("J_GW_dot")
plt.plot(t, J_GW_dot_all, label='J_GW_dot')
for m in dset.mmodes:
    plt.plot(t, 2*J_GW_dot[m], label='2 x J_GW_dot(m=%d)' % m)
plt.gca().set_yscale("log")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.legend(loc='best')
plt.savefig(output_path + "/J_GW_dot.png")

plt.figure()
plt.title("J_GW")
plt.plot(t, J_GW_all, label='J_GW')
for m in dset.mmodes:
    plt.plot(t, 2*J_GW[m], label='2 x J_GW(m=%d)' % m)
plt.gca().set_yscale("log")
plt.xlim(xmin = t.min() + options["window.delta"],
         xmax = t.max() - options["window.delta"])
plt.ylim(ymin=1e-16)
plt.legend(loc='best')
plt.savefig(output_path + "/J_GW.png")

# Show plots
#plt.show()


# -------------------------------------------------------
# Output data
# -------------------------------------------------------
# Find the merger time from the 22 mode
tmerger = t[np.argmax(A_ff[2,2])]
open(output_path + "/tmerger.dat", "w").write("{}\n".format(tmerger))

# Output the strain
for l, m in dset.modes:
    print("Writing (%d,%d)..." % (l,m)),

    ofile = open(output_path + "/psi4_l%d_m%d.dat" % (l,m), "w")
    ofile.write("# 1:time 2:Re 3:Im\n")
    for i in xrange(t.shape[0]):
        ofile.write("{} {} {}\n".format(t[i], Psi4[l,m][i].real,
            Psi4[l,m][i].imag))
    ofile.close()

    ofile = open(output_path + "/strain_l%d_m%d.dat" % (l,m), "w")
    ofile.write("# 1:time 2:Re 3:Im 4:phi 5:omega 6:E_dot_GW 7:E_GW 8:J_dot_GW 9:J_GW\n")
    for i in xrange(t.shape[0]):
        ofile.write("{} {} {} {} {} {} {} {} {}\n".format( t[i],
            h_ff[l,m][i].real, h_ff[l,m][i].imag, phi_ff[l,m][i],
            omega_ff[l,m][i], E_GW_dot_ff[l,m][i], E_GW_ff[l,m][i],
            J_GW_dot_ff[l,m][i], J_GW_ff[l,m][i]))
    ofile.close()

    print("done!")

outfile = open(output_path + "/EJ.dat", "w")
outfile.write("# 1:t 2:E_GW_dot 3:E_GW 4:J_GW_dot 5:J_GW\n")
for i in xrange(t.shape[0]):
    outfile.write("{} {} {} {} {}\n".format(t[i], E_GW_dot_all[i], E_GW_all[i],
        J_GW_dot_all[i], J_GW_all[i]))
outfile.close()
