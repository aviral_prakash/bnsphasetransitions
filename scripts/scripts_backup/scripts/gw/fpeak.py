#!/usr/bin/env python
# Extract the peak frequency of the post-merger spectrum mode by mode

import numpy as np
from scidata.utils import ilocate
import sys
import units as ut
import re
import os

# Parse CLI
if len(sys.argv) != 2 or not os.path.isdir(sys.argv[1]):
    print("Usage: {0} /path/to/strain.dat".format(sys.argv[0]))
    exit(1)
target = sys.argv[1]

# Output file
ofile = open("fpeak.dat", "w")
ofile.write("# 1:l 2:m 3:fpeak\n")

# Read PSDs
for ifname in ilocate("psd_l?_m?.dat", root=target):
    bfname = os.path.basename(ifname)
    print("Processing {}...".format(bfname)),

    f, h = np.loadtxt(ifname, unpack=True, usecols=(0,1))
    idx = f > 1e3
    f = f[idx]
    h = h[idx]

    match = re.match("psd_l(\d)_m(\d).dat", bfname)
    l = int(match.group(1))
    m = int(match.group(2))

    ofile.write("{} {} {}\n".format(l, m, f[np.argmax(h)]))
    print("done!")
ofile.close()
