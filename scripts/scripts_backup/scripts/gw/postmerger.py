#!/usr/bin/env python

from math import pi
import numpy as np
from scidata.windows import exponential as window
from scipy.fftpack import fft
from scipy.fftpack.helper import fftfreq
import sys
import units as ut

tmerger = float(open("tmerger.dat", "r").readline())
t, hp, hx = np.loadtxt("strain_l2_m2.dat", usecols=(0,1,2), unpack=True)
t = ut.conv_time(ut.cactus, ut.cgs, t - tmerger)
hx *= (-1.0)

dtnew = 1.0/16384.0
tnew = np.arange(0, 0.02 + dtnew, dtnew)

hp = np.interp(tnew, t, hp, right=0.0)
hx = np.interp(tnew, t, hx, right=0.0)

ofile = open("postmerger_strain_l2_m2.dat", "w")
ofile.write("# 1:time 2:h+ 3:hx\n")
for i in range(tnew.shape[0]):
    ofile.write("{} {} {}\n".format(tnew[i], hp[i], hx[i]))
del ofile

w = window(tnew/tnew.max(), 0.001/tnew.max())
f = fftfreq(hp.shape[0], dtnew)
hp = dtnew*fft(w*hp)
hx = dtnew*fft(w*hx)
ofile = open("postmerger_psd_l2_m2.dat", "w")
ofile.write("# 1:f 2:hp.real 3:hp.imag 4:hx.real 5:hx.imag\n")
for i in range(f.shape[0]):
    ofile.write("{} {} {} {} {}\n".format(f[i], hp[i].real, hp[i].imag,
        hx[i].real, hx[i].imag))
del ofile
