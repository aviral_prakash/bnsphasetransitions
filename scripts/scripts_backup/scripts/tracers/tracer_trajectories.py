#!/usr/binenv python

import argparse
import os 
import sys 

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import scidata.gizmo as gizmo
from scidata.utils import locate
from decimal import *
import scidata.units as ut
from scipy.interpolate import RegularGridInterpolator

import h5py as h

print("\n Calculating Tracer Trajectories")
##################################### Yq Calculation #######################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01


Yq = 1. - comp[3] ## Opposite to Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
##############################################################################################

udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # in g/cc 
utime  = ut.conv_time(ut.cactus, ut.cgs, 1) * 1e3 # in ms 


# Parse CLI 
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default=".", help="Path to the tracer data")
parser.add_argument("-o", "--outdir", default="tracers/tracer_x_y_trajectories", help="Output Folder")
args = parser.parse_args()
# Locating the tracer data 
dset = gizmo.dataset(locate("tracers.*.hdf5", root=args.input))
if len(dset.iterations) == 0:
    sys.exit("No tracer data found in \"{}\"".format(args.input))

# Create Output Folders 
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)
for subdir in ["rho", "T", "Ye", "Yq"]:
    dpath = os.path.join(args.outdir, subdir)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)

tracer_nrad = 32
tracer_nth  = 16
tracer_nphi = 64

def tracer_idx(ir, ith, iphi):
    return iphi + tracer_nphi*(ith + tracer_nth*ir)

### Plotting rho
print("\n trajectories weighted by rho ....")
k = 1
for i in (dset.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset.iterations))+" ...")
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Temperature
    T = dset.get_field_data(it = i, name = "Temperature")
    # Electron fraction
    Ye = dset.get_field_data(it = i, name="Ye")
    # Entropy
    entr = dset.get_field_data(it = i, name="Entropy")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])


    sel_none = np.zeros_like(mass, dtype=np.bool)

    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=10.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    sel_xy = sel_none.copy()
    sel_xy[L] = True

    fig, ax = plt.subplots()
    ax.set_title("Tracers on the xy-plane")
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")

    mysel = sel_xy
    sc = ax.scatter(x[mysel], y[mysel], c=rho[mysel], cmap = 'jet', vmin = 0.0, vmax = 1.0)
    clb = plt.colorbar(sc, label=r"$rho$")
    plt.title('t = '+str(dset.get_time(i)*utime)+' ms')
    plt.savefig(str(args.outdir)+"/rho/"+str(i)+".png")
    clb.remove()
    plt.close()
    k = k + 1
    print(" done")

#### Plotting T
print("\n Trajectories weighted by T ....")
k = 1
for i in (dset.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset.iterations))+" ...")
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Temperature
    T = dset.get_field_data(it = i, name = "Temperature")
    # Electron fraction
    Ye = dset.get_field_data(it = i, name="Ye")
    # Entropy
    entr = dset.get_field_data(it = i, name="Entropy")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    
    sel_none = np.zeros_like(mass, dtype=np.bool)
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=10.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    sel_xy = sel_none.copy()
    sel_xy[L] = True

    fig, ax = plt.subplots()
    ax.set_title("Tracers on the xy-plane")
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")

    mysel = sel_xy
    sc = ax.scatter(x[mysel], y[mysel], c=T[mysel], cmap = 'hot', vmin = 0.01, vmax = 100.0)
    clb = plt.colorbar(sc, label=r"$T (MeV)$")
    plt.title('t = '+str(dset.get_time(i)*utime)+' ms')
    plt.savefig(str(args.outdir)+"/T/"+str(i)+".png")
    clb.remove()
    plt.close()
    k = k + 1
    print("done")


#### Plotting Ye
print("\n Trajectories weighted by Ye ....")
k = 1
for i in (dset.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset.iterations))+" ...")
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Temperature
    T = dset.get_field_data(it = i, name = "Temperature")
    # Electron fraction
    Ye = dset.get_field_data(it = i, name="Ye")
    # Entropy
    entr = dset.get_field_data(it = i, name="Entropy")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    
    sel_none = np.zeros_like(mass, dtype=np.bool)
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=10.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    sel_xy = sel_none.copy()
    sel_xy[L] = True

    fig, ax = plt.subplots()
    ax.set_title("Tracers on the xy-plane")
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")

    mysel = sel_xy
    sc = ax.scatter(x[mysel], y[mysel], c=Ye[mysel], cmap = 'gnuplot', vmin = 0.0, vmax = 0.5)
    clb = plt.colorbar(sc, label=r"$Ye$")
    plt.title('t = '+str(dset.get_time(i)*utime)+' ms')
    plt.savefig(str(args.outdir)+"/Ye/"+str(i)+".png")
    clb.remove()
    plt.close()
    k = k + 1
    print("done")

#### Plotting Yq
print("\n Trajectories weighted by Yq ....")
k = 1
for i in (dset.iterations):
    print("Processing iteration "+str(k)+"/"+str(len(dset.iterations))+" ...")
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Temperature
    T = dset.get_field_data(it = i, name = "Temperature")
    # Electron fraction
    Ye = dset.get_field_data(it = i, name="Ye")
    # Entropy
    entr = dset.get_field_data(it = i, name="Entropy")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    Yq = np.zeros((65536)) # Total number of tracers = 65536
    #### Yq calculation for all tracer particles
    for j in range(0, 65536):
        Yq[j] = Yq_calc(np.log10(1e15 * rho[j]),T[j],Ye[j])
    
    sel_none = np.zeros_like(mass, dtype=np.bool)
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=10.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    sel_xy = sel_none.copy()
    sel_xy[L] = True

    fig, ax = plt.subplots()
    ax.set_title("Tracers on the xy-plane")
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")

    mysel = sel_xy
    sc = ax.scatter(x[mysel], y[mysel], c=Yq[mysel], cmap = 'CMRmap', vmin = 0.01, vmax = 0.15)
    clb = plt.colorbar(sc, label=r"$Yq$")
    plt.title('t = '+str(dset.get_time(i)*utime)+' ms')
    plt.savefig(str(args.outdir)+"/Yq/"+str(i)+".png")
    clb.remove()
    plt.close()
    k = k + 1
    print("done")
