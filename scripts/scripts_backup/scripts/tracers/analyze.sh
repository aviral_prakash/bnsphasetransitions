#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/segments"
    exit 1

fi
target=$1
cd $target || exit 1

# Copying the Quark Fraction table to the calling simulation folder. On a different machine please modify this address  

echo "Copying Quark table to local repo"
cp -v ~/scripts/Quarks/comp_BLh_180_0.35.hdf5 $target

# Calling the Quark Fraction Calculation for all segments 
for f in output-????/data; do
        if [ ! -f ${f}/tracers.done ]; then
                echo "For segment ${f}:" 
#		python -u ~/scripts/tracers/tracer_evolution.py -i ${f} || continue 1
#               python -u ~/scripts/tracers/plot_hist_from_tracers.py -i ${f} || continue 1
#               python -u ~/scripts/tracers/phase_diagram.py -i ${f} || continue 1
		python -u ~/scripts/tracers/phase_diagram_tracers_on_EOS.py -i ${f} || continue 1
#		python -u ~/scripts/tracers/tracer_trajectories.py -i ${f} || continue 1
                touch ${f}/tracers.done
        fi
done
touch tracers.done

# Removing the Quark table as it is no longer required
echo "Deleting Quark Table" 
rm -rf $target/comp_BLh_180_0.35.hdf5
