
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import scidata.gizmo as gizmo
from scidata.utils import locate
from decimal import *
import scidata.units as ut

from scipy.interpolate import RegularGridInterpolator
import h5py as h

import argparse
import os 
import sys 

print("\n Calculating the time Evolution of tracer's thermodynamic quantities")

# Parse CLI 
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", default=".", help="Path to tracer data")
args = parser.parse_args()

# Files for evolution data of Tracers' thermodynamic quantities 
str_rho = open(str(args.input) + "/tracers_rho.asc","w")
str_T = open(str(args.input) + "/tracers_T.asc","w")
str_Ye = open(str(args.input) + "/tracers_Ye.asc","w")
str_Yq = open(str(args.input) + "/tracers_Yq.asc","w")

##################################### Yq Calculation #######################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01


Yq = 1. - comp[3] ## Oppposite to  Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
##############################################################################################

udens = ut.conv_dens(ut.cactus, ut.cgs, 1) # g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3   # ms


# Locating the data 
dset = gizmo.dataset(locate("tracers.*.hdf5", root=args.input))

tracer_nrad = 32
tracer_nth  = 16
tracer_nphi = 64

def tracer_idx(ir, ith, iphi):
    return iphi + tracer_nphi*(ith + tracer_nth*ir)

### rho
print("\n Processing rho evolution ....")
for i in (dset.iterations):
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=5.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    str_rho.write(str(dset.get_time(i)*utime))
    for j in range(0, len(L)):
        str_rho.write("\t" + str(rho[L[j]]))
    str_rho.write("\n############\n")

print("done")

### T
print("\n Processing T evolution ....")
for i in (dset.iterations):
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Temperature
    T = dset.get_field_data(it = i,name="Temperature")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=5.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    str_T.write(str(dset.get_time(i)*utime))
    for j in range(0, len(L)):
        str_T.write("\t" + str(T[L[j]]))
    str_T.write("\n############\n")

print("done")

### Ye
print("\n Processing Ye evolution ....")
for i in (dset.iterations):
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Ye
    Ye = dset.get_field_data(it = i,name="Ye")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=5.0):            # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
    str_Ye.write(str(dset.get_time(i)*utime))
    for j in range(0, len(L)):
        str_Ye.write("\t" + str(T[L[j]]))
    str_Ye.write("\n############\n")

print("done")
### Yq
print("\n Processing Yq evolution ....")
for i in (dset.iterations):
    x, y, z = dset.get_coordinates(it = i)
    r = np.sqrt(x*x + y*y + z*z)
    # Density
    rho = dset.get_field_data(it = i,name="Density")*udens/1e15
    # Temperature
    T = dset.get_field_data(it = i,name="Temperature")
    # Ye
    Ye = dset.get_field_data(it = i,name="Ye")
    # Mass (each tracer counts twice because of the z --> -z reflection symmetry)
    mass = dset.get_field_data(it = i, name="Masses")*2
    # Tracer ids
    tracer_id = np.arange(mass.shape[0])
    
    # Tracers initially near the xy-plane
    L = []
    for ir in range(0, tracer_nrad):
        for iph in range(0, tracer_nphi, 1):
            if (r[tracer_idx(ir, tracer_nth - 1, iph)]<=5.0):  # Only selecting tracers from the core
                L.append(tracer_idx(ir, tracer_nth-1, iph))
            else:
                continue
            
    str_Yq.write(str(dset.get_time(i)*utime))
    for j in range(0, len(L)):
        str_Yq.write("\t" + str(Yq_calc(np.log10(1e15 * rho[L[j]]),T[L[j]],Ye[L[j]])))
    str_Yq.write("\n############\n")
print("Done")


str_rho.close()
str_T.close()
str_Ye.close()
str_Yq.close()



