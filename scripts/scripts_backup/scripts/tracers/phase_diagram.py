#!/usr/bin/env python
# This script plots tracer trajectory in the phase space over a dynamic 2D histogram of rho, T and Yq
# Which can be considered to be a proxy for the EOS 
from __future__ import division, print_function

import argparse
import matplotlib as mpl
mpl.use('Agg')
from matplotlib.colors import LogNorm, Normalize
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import scidata.gizmo as gizmo
import scidata.units as ut
from scidata.utils import locate
from scipy.interpolate import RegularGridInterpolator
import h5py as h

print("\n Calculating the  Phase Diagram")
##################################### Yq Calculation #######################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01


Yq = 1. - comp[3] ## Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
##############################################################################################



# Conversion units
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)       # g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3   # ms


# Parse CLI
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default=".", help="Path to the tracer data")
parser.add_argument("-o", "--outdir", default="tracers", help="Output folder")
parser.add_argument("--cmap", default="jet", help="Color map")

parser.add_argument("--log-rho", dest="log_rho", action='store_true',
        help="Use log scale for density")
parser.add_argument("--rho-min", dest="rho_min", type=float, default=1e4,
        help="Minimum density (default: 10^4 g/cc)")
parser.add_argument("--rho-max", dest="rho_max", type=float, default=2e15,
        help="Maximum density (default: 10^15 g/cc)")
parser.add_argument("--nrho", type=int, default=100,
        help="Number of bins in density (default: 100)")

parser.add_argument("--log-temp", dest="log_temp", action='store_true',
        help="Use log scale for temperature")
parser.add_argument("--temp-min", dest="temp_min", type=float, default=1e-2,
        help="Minimum temperature (default: 0.01 MeV)")
parser.add_argument("--temp-max", dest="temp_max", type=float, default=1e2,
        help="Maximum temperature (default: 100 MeV)")
parser.add_argument("--ntemp", type=int, default=100,
        help="Number of bins in temperature (default: 100)")

parser.add_argument("--entr-min", dest="entr_min", type=float, default=0.0,
        help="Minimum entropy (default: 0)")
parser.add_argument("--entr-max", dest="entr_max", type=float, default=10.0,
        help="Maximum entropy (default: 10)")
parser.add_argument("--nentr", type=int, default=100,
        help="Number of bins in entropy (default: 100)")

parser.add_argument("--ye-min", dest="ye_min", type=float, default=0.0,
        help="Minimum Ye (default: 0)")
parser.add_argument("--ye-max", dest="ye_max", type=float, default=0.5,
        help="Maximum Ye (default: 0.5)")
parser.add_argument("--nye", type=int, default=100,
        help="Number of bins in Ye (default: 100)")


parser.add_argument("--yq-min", dest="yq_min", type=float, default=0.0, # Specified range of Yq and # of bins for Yq
                    help="Minimum Yq (default: 0)")
parser.add_argument("--yq-max", dest="yq_max", type=float, default=1.0,
                    help="Maximum Yq (default: 1.0)")
parser.add_argument("--nyq", type=int, default=100,
                    help="Number of bins in Yq (default: 100)")


args = parser.parse_args()


# Locate data
dset = gizmo.dataset(locate("tracers.*.hdf5", root=args.input))
if len(dset.iterations) == 0:
    sys.exit("No tracer data found in \"{}\"".format(args.input))


# Create output folders
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)
for subdir in ["Phase_Diag_rho_T_yq"]:     # Created one more folder for quark fraction
    dpath = os.path.join(args.outdir, subdir)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)


# Prepare histogram bins
if args.log_rho:
    rhof = np.linspace(np.log10(args.rho_min), np.log10(args.rho_max), args.nrho+1)
else:
    rhof = np.linspace(args.rho_min, args.rho_max, args.nrho+1)/1e15
if args.log_temp:
    tempf = np.linspace(np.log10(args.temp_min), np.log10(args.temp_max), args.ntemp+1)
else:
    tempf = np.linspace(args.temp_min, args.temp_max, args.ntemp+1)
entrf = np.linspace(args.entr_min, args.entr_max, args.nentr+1)
yef = np.linspace(args.ye_min, args.ye_max, args.nye+1)
yqf = np.linspace(args.yq_min, args.yq_max, args.nyq + 1)


# Loop over iterations and plot histograms 
itfill = len(str(dset.iterations[-1])) ### I believe -1 is for the refinement level --- I may be wrong
for idx, it in enumerate(dset.iterations):
    print("Processing iteration {}/{}...".format(idx + 1, len(dset.iterations)), end='')
    time = dset.get_time(it)*utime
    norm = LogNorm(0.00000001, 1.0)
    
    rho = dset.get_field_data(it, name="Density")*udens # This is rho in cgs
    if args.log_rho:
        rho = np.log10(rho)         # This is log_rho where rho is in cgs
    else:
        rho /= 1e15
    temp = dset.get_field_data(it, name="Temperature")
    if args.log_temp:
        temp = np.log10(temp)
    entr = dset.get_field_data(it, name="Entropy")
    ye = dset.get_field_data(it, name="Ye")
    yq = np.zeros((65536)) # Total number of tracers = 65536
    #### Yq calculation for all tracer particles
    for i in range(0, 65536):
        yq[i] = Yq_calc(np.log10(1e15 * rho[i]),temp[i],ye[i])
    
    fname = str(it).zfill(itfill) + ".png"

    ax = plt.axes()
    ax.set_title(r"$t = {:.3f}\ [{{\rm ms}}]$".format(time))
    _, _, _, im = ax.hist2d(rho, temp, bins=(rhof, tempf),
                        weights=yq, norm=norm, cmap=args.cmap)
    plt.plot(rho[10], temp[10], 'go') # Plotting rho, T for individual tracers
    plt.plot(rho[100], temp[1000], 'bo')
    plt.plot(rho[55536], temp[55536], 'co')
    plt.plot(rho[65530], temp[65530], 'mo')
    cbar = plt.colorbar(im)
    ax.set_xlabel("rho / 1e15")
    ax.set_ylabel("T MeV")
    cbar.set_label("Yq")
    plt.savefig(os.path.join(args.outdir, "Phase_Diag_rho_T_yq", fname))
    plt.close()

    print("done!")
