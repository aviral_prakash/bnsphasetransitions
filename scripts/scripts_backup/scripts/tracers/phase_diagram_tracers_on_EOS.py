#!/usr/bin/env python
# This script plots tracer trajectory over the Ye integrated phase space of the BLQ EOS 
from __future__ import division, print_function

import argparse
import matplotlib as mpl
mpl.use('Agg')
from matplotlib.colors import LogNorm, Normalize
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import scidata.gizmo as gizmo
import scidata.units as ut
from scidata.utils import locate
from scipy.interpolate import RegularGridInterpolator
import h5py as h
#import tqdm # Managing / Monitoring time

rho_nuc = 2.70 * 1e14 # Nuclear Saturation Density in g/cc

print("\n Calculating the  Phase Diagram")
##################################### Yq Calculation #######################################
data = h.File("comp_BLh_180_0.35.hdf5")
comp = data['composition']

log_rho_new =  np.array(comp[0][0:28])
T_new = np.array(comp[1][0::1680])
Ye_new = np.zeros((60))
for i in range(0, 60):
    Ye_new[i] = i/100. + 0.01

Yq = 1. - comp[3] ## Domenico's convention
Yq = Yq.reshape((len(T_new), len(Ye_new), len(log_rho_new)))
Yq_interp = RegularGridInterpolator((T_new, Ye_new, log_rho_new), Yq, method = "linear", bounds_error=False, fill_value=None)

def Yq_calc(x, y, z):
    return(np.maximum(0.0, Yq_interp([y,z,x])[0]))
##############################################################################################

#####################################################################################

################ Module to integrate Yq over all values of Ye ############
### Weighted mean : <Yq> (rho, T) =   \int Yq(rho, T, Ye) dYe
#################################    -------------------------
#################################     \int dYe
print("\n Integrating the EOS ...")
Yq_avg = np.zeros((T_new.size * log_rho_new.size))
Yq_avg = Yq_avg.reshape((T_new.size, log_rho_new.size)) ####Because <Yq> will be free of Ye degrees of freedom

# Defining the Ye integrated Yq ####
for i in range(0, T_new.size):
    for j in range(0, log_rho_new.size):
        Yq_avg[i, j] = (Ye_new.size/Ye_new.sum()) * np.average(Yq[i,:,j] * Ye_new[:])

#### Explanation: Yq_avg[i, j] =  np.average(Yq[i,:,j]) will only return (1/Ye_new.size) (Sum of Quark Fractions)
#### ----Just the average and not the weighted average. We want a weughted average like expectation values in QM

Yq_avg_interp = RegularGridInterpolator((T_new, log_rho_new), Yq_avg, method = 'linear', bounds_error=False, fill_value=None)
def Yq_avg_calc(x, y):
    return(np.maximum(0.0, Yq_avg_interp([y, x])[0]))

################################ Calculating Ye weighted BLQ for a refined grid #########################################

log_rho_refined = np.linspace(log_rho_new.min(), log_rho_new.max(), 1000)
T_refined = np.linspace(T_new.min(), T_new.max(), 1000)
Yq_avg_refined = np.zeros((1000 * 1000))
Yq_avg_refined = Yq_avg_refined.reshape((1000, 1000))

for i in range(0, log_rho_refined.size):  # rds
    for j in range(0, T_refined.size):
        Yq_avg_refined[j, i] = Yq_avg_calc(log_rho_refined[i], T_refined[j])

print(" Done")

# Conversion units
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)       # g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3   # ms


# Parse CLI
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default=".", help="Path to the tracer data")
parser.add_argument("-o", "--outdir", default="tracers", help="Output folder")
parser.add_argument("--cmap", default="gist_stern", help="Color map")

args = parser.parse_args()


# Locate data
dset = gizmo.dataset(locate("tracers.*.hdf5", root=args.input))
if len(dset.iterations) == 0:
    sys.exit("No tracer data found in \"{}\"".format(args.input))


# Create output folders
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)
for subdir in ["Phase_Diag_rho_T_yq"]:     # Created one more folder for quark fraction
    dpath = os.path.join(args.outdir, subdir)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)

str1 = open("t_rho_T_R_tracer.asc", "a+") # stores evolution time, rho, T and radius of a tracer 
# R ~ distance from the merger remnant'c core 

# Loop over iterations and plot tracer trajectories on EOS 
itfill = len(str(dset.iterations[-1])) ### I believe -1 is for the refinement level --- I may be wrong
for idx, it in enumerate(dset.iterations):
    print("Processing iteration {}/{}...".format(idx + 1, len(dset.iterations)), end='')
    time = dset.get_time(it)*utime
    x, y, z = dset.get_coordinates(it)
    r = np.sqrt(x*x + y*y + z*z)

    rho = dset.get_field_data(it, name="Density")*udens # This is rho in cgs
    temp = dset.get_field_data(it, name="Temperature")
    #ye = dset.get_field_data(it, name="Ye")

    #yq = np.zeros((65536)) # Total number of tracers = 65536
    #### Yq calculation for all tracer particles
    #for i in range(0, 65536):
    #    yq[i] = Yq_calc(np.log10( rho[i]),temp[i],ye[i])
    
    fname = str(it).zfill(itfill) + ".png"
    plt.pcolormesh((10**log_rho_refined/rho_nuc), T_refined, Yq_avg_refined[:,:], cmap = 'gist_stern')
    plt.colorbar(label=r'$\mathit{\frac{\int Y_q(\rho,\;T,\;Y_e)\;dY_e}{\int dY_e}}$')
    plt.xlabel(r'$\mathit{\rho / \rho_{nuc}}$')
    plt.ylabel(r'$\mathit{T \;(MeV)}$')
    #plt.ylim(0, 60)
    #plt.xlim(0, 8)
    #plt.clim(0.0, 0.025)
     # Plotting rho, T for individual tracers
    #plt.plot(rho[100]/rho_nuc, temp[1000], 'bo')
    plt.plot(rho[65530]/rho_nuc, temp[65530], 'mo')
    str1.write(str(time) + "\t\t" + str(rho[65530]/rho_nuc) + "\t\t" + str(temp[65530]) +"\t\t"+ str(r[65530]) + "\n")
    plt.savefig(os.path.join(args.outdir, "Phase_Diag_rho_T_yq", fname))
    plt.close()

    print("done!")

str1.close()
