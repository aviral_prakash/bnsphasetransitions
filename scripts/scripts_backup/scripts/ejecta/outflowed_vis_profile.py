#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({
    'figure.figsize': [10, 4],
    'image.cmap'    : 'CMRmap'
})

import argparse
from matplotlib.colors import Normalize, LogNorm
import matplotlib.pyplot as plt
import scidata.xgraph as xg
import scivis.units as ut

# Parse the CLI
parser = argparse.ArgumentParser()
parser.add_argument("ifile", metavar="profile_VAR.xg", nargs=1,
        help="Profile to plot")
parser.add_argument("-o", "--output", dest="output",
        help="Output file (defaults to profile_VAR.png)")
parser.add_argument("-r", "--rescale", dest="rescale", type=float,
        help="Normalize the data by multiplying for a given value")
parser.add_argument("-c", "--cmap", dest="cmap", help="Colormap to use")
parser.add_argument("-l", "--log", dest="log", action="store_true",
        default=False, help="Use logarithmic scale")
parser.add_argument("--min", dest="min", type=float,
        help="Minimum value to use for the color map")
parser.add_argument("--max", dest="max", type=float,
        help="Maximum value to use for the color map")
parser.add_argument("--begin", dest="begin", type=float,
        help="Ignore data before this time (in Msol)")
parser.add_argument("--end", dest="end", type=float,
        help="Ignore data after this time (in Msol)")
args = parser.parse_args()

if args.cmap is not None:
    matplotlib.rcParams.update({'image.cmap': args.cmap})

# Read the datafile
mdataset = xg.parsefile(args.ifile[0])

# Re-arrange the data
tfirst  = min(mdataset.time)
tlast   = max(mdataset.time)
theta   = mdataset.frame(0).data_x
rawdata = mdataset.data_y.reshape((mdataset.nframes, theta.shape[0]))
if args.rescale is not None:
    rawdata[:] *= args.rescale

# Generate colorscale
if args.min is None:
    dmin = rawdata.min()
else:
    dmin = args.min
if args.max is None:
    dmax = rawdata.max()
else:
    dmax = args.max
if args.log:
    norm = LogNorm(vmin=dmin, vmax=dmax)
else:
    norm = Normalize(vmin=dmin, vmax=dmax)

# Plot data
ax = plt.axes([0.05, 0.1, 0.85, 0.85])
im = ax.imshow(rawdata.transpose(), origin='lower', norm=norm, extent=[tfirst,
    tlast, theta[0], theta[-1]], aspect="auto", interpolation='none')
if args.begin is not None:
    ax.set_xlim(xmin=args.begin)
if args.end is not None:
    ax.set_xlim(xmax=args.end)
ax = plt.axes([0.92, 0.1, 0.025, 0.85])
plt.colorbar(im, cax=ax)

# Save plot
if args.output is None:
    ofname = args.ifile[0].replace(".xg", "") + ".png"
else:
    ofname = args.output
plt.savefig(ofname)
