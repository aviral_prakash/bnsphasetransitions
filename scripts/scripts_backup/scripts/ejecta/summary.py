#!/usr/bin/env python

import fnmatch
import numpy as np
import sys
import os

def dlocate(pattern, root=os.curdir, followlinks=False):
    for path, dirs, files in os.walk(os.path.abspath(root),
            followlinks=followlinks):
        for dir_ in fnmatch.filter(dirs, pattern):
            yield os.path.join(path, dir_)

# Time units
utime = 0.004925794970773136 # ms
# Energy units
uenergy = 1787.5521500932314  # 10^51 erg

# Radio remnant: radio spectrum from Ehud Nakar and Tsvi Piran 1102.1020v1
# (NATURE  vol 478 2011) Eq.2
# Input:
#   e_k0     : kinetic energy [erg]
#   beta0    : velocity of the ejecta [c]
#   n_ism0   : density of ism [cm^{-3}]
#   dist     : distance [cm]
#   freq_obs : observation frequency [GHz]
# Output:
#   fl_obs   : fluency [mJy]
def flux_peak_obs (e_k0, beta0, n_ism0=0.1, p=2.5, eps_B=0.1, eps_e=0.1,
        dist=3.086e26, freq_obs=1.4):
    e_k0_49 = e_k0 / 1.e49
    eps_B_m1 = eps_B * 10.
    eps_e_m1 = eps_e * 10.
    d_27     = dist / 1.e27
    A = (p + 1.)/4.

    B = p - 1.
    C = (5.*p - 7) / 2.
    D = - ((p-1.)/2.)
    fl_obs = 0.3 * e_k0_49 * pow(n_ism0,A) * pow(eps_B_m1,A) * \
            pow(eps_e_m1,p-1.) * pow(beta0,C) * pow(d_27,-2.) * \
            pow(freq_obs/1.4,D)
    return fl_obs
# Radio remnant decoupling time as in Eq.1 (see above)
# See Ehud Nakar and Tsvi Piran 1102.1020v1 (2011)
# Input:
#   e_k0     : kinetic energy [ergs]
#   beta0    : velocity of the ejecta [c]
#   n_ism0   : density of ism [cm^{-3}]
# Output:
#   t        : decoupling time [days]
#   radius   : decoupling radius [cm]
def decoupling(e_k0, beta0, n_ism0=0.1):
    radius =  1.e17 * pow(e_k0/1.e49, 1./3.) * pow(n_ism0,-1./3.) * \
              pow(beta0,-2./3.)
    t = 30. * pow(e_k0/1.e49, 1./3.) * pow(n_ism0,-1./3.) * pow(beta0,-5./3.)

    return t, radius

# kilonova
# See Eqs(4) Grossman, Korobkin, Rosswog, Piran MNRAS 439 (2014)
# Input:
#   m_unb : ejecta muss [Msun]
#   beta  : velocity of the ejecta [c]
#   kappa : opacity [cm^2 g^{-1}]
# Output:
#   t     : peak time [days]
#   lum   : peak luminosity [erg/s]
#   temp  : temperature at peak [K]
def nuc_peak_gross(m_unb, beta, kappa=10., alpha=1.3):
    kappa_10 = kappa / 10.
    m_unb_m2 = m_unb / 0.01
    v_m1     = beta/0.1

    t    = 4.9e0 * pow((kappa_10 * m_unb_m2) / v_m1, 0.5)
    lum  = 2.5e40 * pow(m_unb_m2,(1.-alpha/2.)) * pow(v_m1/kappa_10,alpha/2.)
    temp = 2200 * pow(kappa_10, -(alpha+2)/8.) * pow(v_m1, (alpha-2.)/8.) * \
           pow(m_unb_m2, -alpha/8.)
    return t, lum, temp

try:
    target = sys.argv[1]
    if not os.path.isdir(target):
        print("{} is not a directory!".format(target))
        exit(1)
except IndexError:
    print("Usage: {} target".format(sys.argv[0]))
    exit(1)

# Compute disk mass at the end of the simulation
t, Mtot = np.loadtxt(target + "/collated/dens.norm1.asc",
        usecols=[1,2], unpack=True)
Mtot *= 2048**3

# BHBlp_M160160_LK did not have dens_unbnd output
try:
    t, Munb = np.loadtxt(target + "/collated/dens_unbnd.norm1.asc",
            usecols=[1,2], unpack=True)
    Munb *= 2048**3
except IOError:
    t_ej, M_ej = np.loadtxt(target + "/outflow_0/total_flux.dat",
            usecols=(0,2), unpack=True)
    Munb = np.interp(t, t_ej, M_ej)

if Mtot[-1] > 1.0:
    Mdisk = np.nan
    tcoll = np.inf
else:
    i_BH = np.argmin(Mtot > 1.0)
    tcoll = t[i_BH]*utime
    i_disk = i_BH + int(1.0/(t[1]*utime))
    Mdisk = Mtot[i_disk] - Munb[i_disk]

for outdir in dlocate("outflow_?", root=target):
    # Compute total ejected mass
    data = np.loadtxt(outdir + "/total_flux.dat")
    Mej  = data[-1,2]

    # Compute average Ye
    Ye, M = np.loadtxt(outdir + "/hist_ye.dat", usecols=(0,1),
            unpack=True)
    Ye_avg = np.sum(Ye * M)/Mej

    # Compute average entropy
    s, M = np.loadtxt(outdir + "/hist_entropy.dat", usecols=(0,1),
            unpack=True)
    s_avg = np.sum(s * M)/Mej

    # Compute the average v_infty
    vel_inf, M = np.loadtxt(outdir + "/hist_vel_inf.dat",
            usecols=(0,1), unpack=True)
    vel_inf_avg = np.sum(vel_inf * M)/Mej

    # Compute the total kinetic energy of the ejecta
    Ekin = np.sum(0.5 * vel_inf**2 * M)

    # Kilonova
    t_p, lum_p, temp_p = nuc_peak_gross(Mej, vel_inf_avg)

    # Radio renmant
    t_dec, r_dec  = decoupling(Ekin * uenergy * 1e51, vel_inf_avg)
    fl_obs = flux_peak_obs(Ekin * uenergy * 1e51, vel_inf_avg)

    # Output results
    ofile = open(outdir + "/summary.txt", "w")
    ofile.write("Mdisk      [1e^{-2} Msol]".ljust(30) + str(Mdisk * 1e2) + "\n")
    ofile.write("tcoll      [ms]".ljust(30) + str(tcoll) + "\n")
    ofile.write("Mej        [1e^{-2} Msol]".ljust(30) + str(Mej * 1e2) + "\n")
    ofile.write("Ye".ljust(30) + str(Ye_avg) + "\n")
    ofile.write("s          [kb/baryon] ".ljust(30) + str(s_avg) + "\n")
    ofile.write("vel_inf    [10^{-1} c] ".ljust(30) + str(vel_inf_avg * 10) + "\n")
    ofile.write("E_kin      [10^51 erg]".ljust(30) + str(Ekin*uenergy) + "\n")
    ofile.write("kilonova t [days]".ljust(30) + str(t_p) + "\n")
    ofile.write("kilonova L [10^41 erg]".ljust(30) + str(lum_p/1e41) + "\n")
    ofile.write("kilonova T [K]".ljust(30) + str(temp_p) + "\n")
    ofile.write("radio    t [years]".ljust(30) + str(t_dec/365.25) + "\n")
    ofile.write("radio    F [mJy]".ljust(30) + str(fl_obs) + "\n")
    ofile.close()
