#!/usr/bin/env python
# Visualizes the fast component of the outflow in 2D

import argparse
from mpl_toolkits.basemap import Basemap
from math import pi, sqrt
import matplotlib as mpl
mpl.rc("image", cmap="RdBu_r")
from matplotlib.colors import LogNorm, Normalize
import matplotlib.pyplot as plt
from matplotlib.pyplot import ScalarFormatter
from matplotlib.ticker import LogFormatterMathtext
import numpy as np
import os
import re
from scidata.utils import locate
import scivis.units as ut
import sys

# SphericalSurface grid
# The data of the Outflow thorn is written out as
#   var[i,j],   i = 0, ... ntheta; j = 0, ..., nphi+1
# Where ntheta and nphi are the number of points on the sphere as given in the
# file header.
#   var[ 0,:]    data at dtheta/2
#   var[-1,:]    data at pi - dtheta/2
#   var[:, 0]    data at phi = 0
#   var[:,-1]    data at phi = 2 pi
class SphericalSurface:
    def __init__(self, ntheta, nphi, radius=1.0):
        self.ntheta = ntheta
        self.nphi   = nphi
        self.radius = radius
        self.dtheta = pi/self.ntheta
        self.dphi   = 2*pi/self.nphi
    def mesh(self):
        theta = (np.arange(self.ntheta) + 0.5)*self.dtheta
        phi   = np.linspace(0, 2*pi, self.nphi+1)
        return np.meshgrid(theta, phi, indexing="ij")
    def area(self):
        theta, phi = self.mesh()
        dA = self.radius**2 * np.sin(theta) * self.dtheta * self.dphi
        dA[:,-1] = 0.0  # avoid double counting data at phi = 2 pi
        return dA
    # This mesh is used for the visualization
    def mesh_vis(self):
        dtheta = 180./(self.ntheta)
        dphi   = 360./(self.nphi + 1)
        theta  = (np.arange(self.ntheta) + 0.5)*dtheta - 90.0
        phi    = (np.arange(self.nphi + 1) + 0.5)*dphi
        return np.meshgrid(theta, phi, indexing='ij')
    def reshape(self, vector):
        return vector.reshape((self.ntheta, self.nphi + 1))
    def size(self):
        return (self.nphi + 1)*self.ntheta

# Variable indices
varmap = {
    'fluxdens'      : 5,
    'w_lorentz'     : 6,
    'eninf'         : 7,
    'rho'           : 9,
    'Y_e'           : 10,
    'entropy'       : 11,
    'temperature'   : 12
}

# Check CLI
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--detector-id", dest="det_id", default=1, type=int,
        help="Detector ID [default: 1]")
parser.add_argument("-d", "--data-path", dest="data_path", required=True,
        help="Path to the simulation data")
parser.add_argument("--pdf", dest="pdf", action="store_true",
        help="Use pdf instead of png for the plots")
parser.add_argument("-f", "--frames", nargs="*",
        help="Plot only the given frames")
parser.add_argument("-t", "--t-mrg", dest="t_mrg", help="Merger time in ms")
parser.add_argument("--t-ret", dest="t_ret", help="Retarded time")
parser.add_argument("--total-only", dest="total_only", action="store_true",
        help="Only output the time integrated total outflow")
parser.add_argument("-M", "--norm-max", dest="norm_max",
        help="Maximum of the colormap used for the plots")
parser.add_argument("-v", "--vel-inf", dest="min_vel_inf",
        help="Minimum asymptotic velocity to consider")
args = parser.parse_args()

# Locate datafiles
flist = sorted(locate("outflow_surface_det_%d_fluxdens.asc" % args.det_id,
        root=args.data_path, followlinks=False))
if len(flist) == 0:
    print("Unkown detector id: %d" % args.det_id)
    exit(1)

if not (args.min_vel_inf is None):
    print("Minimum eninf = {}".format(0.5*float(args.min_vel_inf)**2))

# Read data
# NOTE: currently this reads the entire data into memory (not very
# efficient...)
data = {
    'iter'    : [],
    'time'    : [],
    'fluxdens': [],
}
for f in flist:
    print("Reading %s..." % f),
    sys.stdout.flush()
    fdata = np.loadtxt(f,
        usecols=(0, 1, varmap["fluxdens"], varmap['eninf']),
        dtype=np.float64,
        unpack=True)
    data["iter"].append(np.array(fdata[0], dtype=np.int32))
    data["time"].append(fdata[1])
    if args.min_vel_inf is not None:
        mask = fdata[3] < 0.5*float(args.min_vel_inf)**2
    else:
        mask = np.isnan(fdata[2])
    data["fluxdens"].append(fdata[2])
    data["fluxdens"][-1][mask] = 0.0
    print("done!")
    sys.stdout.flush()
for key in data.keys():
    data[key] = np.concatenate(data[key])
itlist = sorted(list(set(data["iter"])))

# Get the spherical mesh
print("Spherical grid information")
dfile = open(flist[0], "r")
dfile.readline()
match = re.match('# detector no.=(\d+) ntheta=(\d+) nphi=(\d+)$',
        dfile.readline())
assert(int(match.group(1)) == args.det_id)
ntheta = int(match.group(2))
nphi = int(match.group(3))
dfile.readline()
dfile.readline()
L = dfile.readline().split()
radius = round(sqrt(float(L[2])**2 + float(L[3])**2 + float(L[4])**2))
print("     radius = {}".format(radius))
print("     ntheta = {}".format(ntheta))
print("     nphi   = {}".format(nphi))
grid = SphericalSurface(ntheta, nphi, radius)
del dfile
th, ph = grid.mesh()
theta, phi = grid.mesh_vis()
dA = grid.area()
r2 = grid.radius**2

# Computes the total flux
f = open("flux.dat","w")
t_old = data["time"][0]
total_flux = np.zeros((grid.ntheta, grid.nphi+1))
int_flux = 0.0
for it in itlist:
    slice_idx  = data["iter"] == it
    t_new = data["time"][slice_idx][0]
    dt = t_new - t_old
    t_old = t_new
    # This takes care of repeated data
    frame_data = data["fluxdens"][slice_idx][:grid.size()]
    frame_data = grid.reshape(frame_data)

    flux = np.sum(frame_data*dA)
    int_flux += flux*dt
    f.write("{} {} {}\n".format(t_old, flux, int_flux))

    total_flux[:] += frame_data*dt
f.close()
print("Total ejecta mass = {} Msun".format(np.sum(total_flux*dA)))

# Plots the total flux
smap = Basemap(projection='hammer', lon_0=180.0)
vmax = total_flux.max()*r2
vmin = 1e-3*vmax
total_flux[r2*total_flux < vmin] = vmin/r2
smap.pcolormesh(phi.T, theta.T, r2*total_flux.T, latlon=True,
        norm=LogNorm(vmin, vmax), edgecolors="face")
cbar = smap.colorbar(format=LogFormatterMathtext())
if args.min_vel_inf is None or float(args.min_vel_inf) == 0.0:
    cbar.set_label(r"$M_{\rm ej}\ [M_\odot\ {\rm sr}^{-1}]$")
else:
    cbar.set_label(r"$M_{\rm ej}(v > %.1f\, c)\ [M_\odot\ {\rm sr}^{-1}]$" %
            float(args.min_vel_inf))
cbar.ax.tick_params(axis='y', direction='in')
smap.drawparallels(np.arange(-90.,120.,30.))
smap.drawmeridians(np.arange(0.,420.,60.))
plt.savefig("total_flux.pdf", bbox_inches='tight')
plt.close()
if args.total_only:
    sys.exit(0)

# Find the maximum flux
if args.norm_max is not None:
    flux_max = float(args.norm_max)
else:
    flux_max = 0.0
    for it in itlist:
        slice_idx = data["iter"] == it
        # This takes care of repeated data
        frame_data = data["fluxdens"][slice_idx][:grid.size()]
        frame_data = grid.reshape(frame_data)
        #frame_data = frame_data*dA
        frame_data = r2*ut.conv_frequency(ut.cactus, ut.cgs, frame_data) # Msun per sec
        flux_max = max(flux_max, frame_data.max())
vmax = flux_max
vmin = 1e-3*vmax
norm = LogNorm(vmin, vmax)

# Do the actual plotting
if args.frames is None:
    frames = range(len(itlist))
else:
    frames = [int(f) for f in args.frames]
for frame_idx in frames:
    it = itlist[frame_idx]
    print("Plotting: {0}/{1}...".format(frame_idx, len(itlist)-1))
    sys.stdout.flush()

    # Skip existing files
    if args.pdf:
        ofname = "{:05d}.pdf".format(frame_idx)
    else:
        ofname = "{:05d}.png".format(frame_idx)
    if os.path.isfile(ofname):
        continue

    slice_idx = data["iter"] == it
    time = ut.conv_time(ut.cactus, ut.cgs, data["time"][slice_idx][0])*1000 # in ms
    if args.t_mrg is not None:
        time -= float(args.t_mrg)
    if args.t_ret is not None:
        time -= float(args.t_ret)

    # This takes care of repeated data
    frame_data = data["fluxdens"][slice_idx][:grid.size()]
    frame_data = grid.reshape(frame_data)
    #frame_data = frame_data*dA
    frame_data = r2*ut.conv_frequency(ut.cactus, ut.cgs, frame_data) # Msun per sec
    frame_data[frame_data < vmin] = vmin

    smap = Basemap(projection='hammer', lon_0=180.0)
    smap.pcolormesh(phi.T, theta.T, frame_data.T, latlon=True, norm=norm,
            edgecolors="face")
    cbar = smap.colorbar(format=LogFormatterMathtext())
    if args.min_vel_inf is None or float(args.min_vel_inf) == 0.0:
        cbar.set_label(r"$\dot{M}_{\rm ej}\ [M_\odot\ {\rm s}^{-1}\ {\rm sr}^{-1}]$")
    else:
        cbar.set_label(r"$\dot{M}_{\rm ej}(v > %.1f\, c)\ [M_\odot\ {\rm s}^{-1}\ {\rm sr}^{-1}]$" % float(args.min_vel_inf))
    cbar.ax.tick_params(axis='y', direction='in')
    smap.drawparallels(np.arange(-90.,120.,30.))
    smap.drawmeridians(np.arange(0.,420.,60.))
    if args.t_mrg is None:
        plt.title(r'$t = %7.3f\ [\mathrm{ms}]$' % time)
    else:
        plt.title(r'$t - t_{\rm mrg} = %7.3f\ [\mathrm{ms}]$' % time)

    plt.savefig(ofname, bbox_inches='tight', dpi=128)
    plt.close()
