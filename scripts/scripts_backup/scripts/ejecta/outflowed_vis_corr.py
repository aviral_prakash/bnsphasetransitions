#!/usr/bin/env python

import matplotlib
matplotlib.use("Agg")
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt

import numpy as np
import h5py
import sys
import re

norm = LogNorm(vmin=1e-6, vmax=2e-1)

try:
    fname = sys.argv[1]
    match = re.match(r"corr_(.+)_(.+).h5", fname)
    var1_name = match.group(1)
    var2_name = match.group(2)
except:
    print("Usage: {} corr_var1_var2.h5".format(sys.argv[0]))
    exit(1)

dfile = h5py.File(fname, "r")
var1 = np.array(dfile[var1_name])
var2 = np.array(dfile[var2_name])
mass = np.array(dfile["mass"])
mass /= mass.sum()

plt.pcolormesh(var1, var2, mass.T, norm=norm, edgecolors='None')
plt.colorbar()
plt.xlabel(var1_name)
plt.ylabel(var2_name)

plt.savefig(fname.replace(".h5","") + ".png")
