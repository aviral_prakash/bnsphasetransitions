#!/usr/bin/env python

from glob import glob
import numpy as np
import re

flist = glob("outflow_det_*.asc")
for fname in flist:
    det_idx = re.match(r"outflow_det_(\d+).asc", fname).group(1)
    ofname = "ejected_det_{}.dat".format(det_idx)
    ofile = open(ofname, "w")
    ofile.write("# 1:time 2:mass 3:einf>=0\n")
    time, flux1, flux2 = np.loadtxt(fname, usecols=(1, 2, 5), unpack=True)
    mass1 = mass2 = 0.0
    for i in range(1, time.shape[0]):
        mass1 += flux1[i-1] * (time[i] - time[i-1])
        mass2 += flux2[i-1] * (time[i] - time[i-1])
        ofile.write("{:.8} {:.8} {:.8}\n".format(time[i], mass1, mass2))
