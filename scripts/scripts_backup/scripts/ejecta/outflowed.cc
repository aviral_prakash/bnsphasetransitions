///////////////////////////////////////////////////////////////////////////////
// outflowed: analysis of the ejecta in a THC run from the 2D Outflow output
// Copyright (C) 2015, David Radice <dradice@caltech.edu>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
///////////////////////////////////////////////////////////////////////////////
// This package contains utilities for
// . Parsing 2D ASCII output of the Outflow thorn
// . Produce sky-maps with the mass, composition, entropy and other properties
//   of the ejecta as a function of the ejection angle
// . Produce composition, entropy and other histograms
//
// Compiling
// . $EDITOR outflowed.cfg
// . h5c++ outflowed.cc -o outflowed
//
// Usage
// . outflowed /path/to/skynet/densmap.h5 /path/to/skynet/grid.h5
//             /path/to/data/1 [/path/to/data/2 [...]]
// All of the analysis output will be done in the current directory
//
// Assumptions
// . The Outflow output fields are hardcoded
// . The Outflow grid is assumed to be constant
// . The Outflow output time frequency is assumed to be constant
///////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <list>
#include <stdexcept>
#include <string>
#include <vector>

#include <hdf5.h>
#include <hdf5_hl.h>

#define length(X) (sizeof(X)/sizeof(*X))
#define SQ(X) ((X)*(X))

#define MYH5CHECK(ierr) \
  if(ierr < 0) { \
    throw runtime_error("Error in the HDF5 library"); \
  }

// Converts dens to cgs
#define DENS_TO_CGS 6.173937319029555e+17
// Converts the time to ms
#define TIME_TO_MS 0.004926415031881399

using namespace std;

struct HistogramBinsSpec {
  double min;
  double max;
  double nbins;
};

///////////////////////////////////////////////////////////////////////////////
struct {
///////////////////////////////////////////////////////////////////////////////
  struct {
    HistogramBinsSpec entropy;
    HistogramBinsSpec log_rho;
    HistogramBinsSpec temperature;
    HistogramBinsSpec theta;
    HistogramBinsSpec vel;
    HistogramBinsSpec vel_inf;
    HistogramBinsSpec ye;
  } histogram;
  double min_eninf;
} const config =
#include "outflowed.cfg"
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class num {
///////////////////////////////////////////////////////////////////////////////
  public:
    // Number of columns in the datafile
    enum {cols = variable::nvars + 2};
    // Number of variables to read from the datafile
    enum {vars = variable::nvars};
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class OutflowGrid {
///////////////////////////////////////////////////////////////////////////////
  public:
    OutflowGrid(int detector_id, int ntheta, int nphi, double radius,
        double delta_time):
      m_detector_id(detector_id),
      m_ntheta(ntheta),
      // The outflow output also contains the point at phi = 2 pi
      m_nphi(nphi+1),
      m_radius(radius),
      m_delta_theta(M_PI/(m_ntheta)),
      m_delta_phi(2*M_PI/(m_nphi-1)),
      m_delta_time(delta_time) { }

    // Get the number of points of the grid
    int get_detector_id() const { return m_detector_id; }
    int get_ntheta() const { return m_ntheta; }
    int get_nphi() const { return m_nphi; }
    int get_ndof() const { return m_ntheta * m_nphi; }

    // Index map used to access points on the sphere
    int sph_index_map(int itheta, int iphi) const {
      return iphi + itheta * m_nphi;
    }
    // Inverse of sph_index_map
    void inv_sph_index_map(int iray, int * itheta, int * iphi) const {
      *itheta = iray / m_nphi;
      *iphi   = iray % m_nphi;
    }
    // Index map used to access the raw data
    int data_index_map(int itheta, int iphi, int ivar) const {
      return ivar + num::vars * sph_index_map(itheta, iphi);
    }
    // Inverse of data_index_map
    void inv_data_index_map(int idx, int * itheta,
        int * iphi, int * ivar) const {
      *ivar = idx % num::vars;
      inv_sph_index_map(idx/num::vars, itheta, iphi);
    }

    // Get spacing
    double get_delta_theta() const { return m_delta_theta; }
    double get_delta_phi() const { return m_delta_phi; }
    double get_delta_time() const { return m_delta_time; }

    // Get the radius of the grid
    double get_radius() const {
      return m_radius;
    }
    // Get the theta coordinate at the given location
    double get_theta(int itheta) const {
      return (0.5 + itheta) * m_delta_theta;
    }
    // Get the phi coordinate at the given location
    double get_phi(int iphi) const {
      return iphi * m_delta_phi;
    }
    // Get the coordinates at the given location
    void get_coordinates(int itheta, int iphi,
        double * theta, double * phi) const {
      *theta = get_theta(itheta);
      *phi = get_phi(iphi);
    }
    // Get the coordinates at the given location
    void get_coordinates(int iray, double * theta, double * phi) const {
      int itheta, iphi;
      inv_sph_index_map(iray, &itheta, &iphi);
      get_coordinates(itheta, iphi, theta, phi);
    }
    // Get the coordinates at the given location
    void get_coordinates(int itheta, int iphi,
        double * x, double * y, double * z) const {
      double theta, phi;
      get_coordinates(itheta, iphi, &theta, &phi);
      *x = m_radius * cos(phi) * sin(theta);
      *y = m_radius * sin(phi) * sin(theta);
      *z = m_radius * cos(theta);
    }
    // Get the coordinates at the given location
    void get_coordinates(int iray, double * x, double * y, double * z) const {
      int itheta, iphi;
      inv_sph_index_map(iray, &itheta, &iphi);
      get_coordinates(itheta, iphi, x, y, z);
    }

    double get_surface_element(double theta, double) const {
      return SQ(m_radius) * sin(theta) * m_delta_phi * m_delta_theta;
    }
  private:
    int m_detector_id;
    int m_ntheta;
    int m_nphi;
    double m_radius;
    double m_delta_theta;
    double m_delta_phi;
    double m_delta_time;
};

ostream & operator<<(ostream & os, OutflowGrid const & grid) {
  os << "# 2d Outflow" << endl;
  os << "# detector no.=" << grid.get_detector_id();
  os << " ntheta=" << grid.get_ntheta();
  os << " nphi=" << grid.get_nphi() - 1 << endl;
  os << "# gnuplot column index:" << endl;
  // These are currently hardcoded, this might change in the future
  os << "# 1:it 2:t 3:x 4:y 5:z 6:fluxdens 7:w_lorentz 8:eninf "
     << "9:surface_element 10:alp 11:rho 12:vel[0] 13:vel[1] 14:vel[2] "
     << "15:Y_e 16:entropy 17:temperature" << endl;
  return os;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class OutflowData {
///////////////////////////////////////////////////////////////////////////////
  public:
    OutflowData(OutflowGrid const & grid):
      grid(grid),
      iter(-1),
      time(-1) {
      m_raw_data.resize(num::vars * grid.get_ndof());
      for(int iphi = 0; iphi < grid.get_nphi(); ++iphi)
      for(int itheta = 0; itheta < grid.get_ntheta(); ++itheta) {
        double * vals = &m_raw_data[grid.sph_index_map(itheta, iphi)*num::vars];
        fill(&vals[0], &vals[variable::nvars], 0.0);
        grid.get_coordinates(itheta, iphi, &vals[variable::x],
            &vals[variable::y], &vals[variable::z]);
        double theta, phi;
        grid.get_coordinates(itheta, iphi, &theta, &phi);
        vals[variable::surface_element] = grid.get_surface_element(theta, phi);
      }
    }

    // Get all of the data
    vector<double> const & get_raw_data() const { return m_raw_data; }
    vector<double> & get_raw_data() { return m_raw_data; }

    // Get the data at the current point on the sphere
    double const * get_data_at_point(int idx) const {
      return &m_raw_data[idx * num::vars];
    }
    double * get_data_at_point(int idx) {
      return &m_raw_data[idx * num::vars];
    }
  public:
    // Grid supporting the data
    OutflowGrid const grid;
    // Current iteration
    int iter;
    // Current time
    double time;
  private:
    vector<double> m_raw_data;
};

ostream & operator<<(ostream & os, OutflowData const & data) {
  for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta) {
    for(int iphi = 0; iphi < data.grid.get_nphi(); ++iphi) {
      os << data.iter << " " << data.time;
      double const * vals = data.get_data_at_point(
          data.grid.sph_index_map(itheta, iphi));
      for(int v = 0; v < num::vars; ++v) {
        os << " " << vals[v];
      }
      os << endl;
    }
    os << endl;
  }
  return os;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class OutflowReader {
///////////////////////////////////////////////////////////////////////////////
  public:
    // Initialize the OutflowReader driver and reads the metadata
    // NOTE: we assume the metadata to be constant
    OutflowReader(list<string> file_names):
      m_prev_iter(-1),
      m_file_names(file_names),
      mp_grid(NULL) {
      reset();

      // Read the grid size
      string dline;
      getline(m_cur_file, dline);
      int detector_id, ntheta, nphi;
      int n = sscanf(dline.c_str(), "# detector no.=%d ntheta=%d nphi=%d",
        &detector_id, &ntheta, &nphi);
      if(3 != n) {
        throw runtime_error("Failed reading the metadata");
      }

      // Skip the rest of the header
      m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
      m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
      streampos start = m_cur_file.tellg();

      // Read the first dataline to get the detector radius
      int it;
      double t, x, y, z;
      m_cur_file >> it >> t >> x >> y >> z;
      if(m_cur_file.fail()) {
        throw runtime_error("Failed reading the first dataline");
      }
      double radius = sqrt(x*x + y*y + z*z);
      m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');

      // Skip to the second data block to read off the timestep
      double t_new;
      for(int l = 1; l < (nphi + 2)*ntheta; ++l) {
        m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
      }
      m_cur_file >> it >> t_new;
      if(m_cur_file.fail()) {
        throw runtime_error("Failed reading the second data block");
      }
      double delta_time = t_new - t;
      // Rewind stream
      m_cur_file.seekg(start);

      mp_grid = new OutflowGrid(detector_id, ntheta, nphi, radius, delta_time);
    }
    ~OutflowReader() {
      delete mp_grid;
    }

    // Rewind the OutflowReader to handle the first file
    void reset() {
      mp_cur_file_name = m_file_names.begin();
      m_cur_file.open(mp_cur_file_name->c_str(), ifstream::in);
      m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    int get_prev_iter() const { return m_prev_iter; }
    OutflowGrid const & get_grid() const { return *mp_grid; }

    // Error codes used by parse_block
    struct error {
      static int const ok       = 0; // Everything is ok
      static int const finished = 1; // There is no more data to read
      static int const old      = 2; // Currently reading duplicated data
      static int const fail     = 3; // Internal error
    };
    // Reads the next block of data
    int parse_block(OutflowData * data) {
      int const ndof = mp_grid->get_ntheta() * mp_grid->get_nphi();
      assert(data->grid.get_ndof() == ndof);

      // There is no more data to read
      if(!m_cur_file.is_open()) {
        return error::fail;
      }

      // Check if we need to switch to a new file
      if(m_cur_file.eof()) {
        m_cur_file.close();
        ++mp_cur_file_name;
        if(mp_cur_file_name != m_file_names.end()) {
          m_cur_file.open(mp_cur_file_name->c_str(), ifstream::in);
          // Skip header
          m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
          m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
          m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
          m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        else {
          // We are done: there is no more data to read
          return error::finished;
        }
      }

      int iter, iter_p(-1);
      double time;
      double vals[num::vars];
      int iray(0);
      do {
        m_cur_file >> iter >> time;

        // The data is currupted if a file ends in the middle of a block
        if(m_cur_file.eof()) {
          if(0 == iray) {
            return parse_block(data);
          }
          else {
            return error::fail;
          }
        }

        // If iteration number and time cannot be parsed then skip line
        if(m_cur_file.fail()) {
          m_cur_file.clear();
          m_cur_file.ignore(numeric_limits<streamsize>::max(), '\n');
          continue;
        }

        if(-1 == iter_p) {
          iter_p = iter;
        }
        else if(iter_p != iter) {
          return error::fail;
        }

        for(int v = 0; v < num::vars; ++v) {
          m_cur_file >> vals[v];
        }
        if(m_cur_file.fail()) {
          return error::fail;
        }
        memcpy(data->get_data_at_point(iray++), vals,
            num::vars*sizeof(double));
      } while(iray < ndof);

      data->iter  = iter;
      data->time  = time;
      if(iter > m_prev_iter) {
        m_prev_iter = iter;
      }
      else {
        return error::old;
      }
      return error::ok;
    }

  private:
    int m_prev_iter;

    list<string> m_file_names;
    list<string>::iterator mp_cur_file_name;
    ifstream m_cur_file;

    OutflowGrid * mp_grid;
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class Histogram {
///////////////////////////////////////////////////////////////////////////////
  public:
    Histogram() {}
    Histogram(vector<double> const & bins):
      m_bins(bins),
      m_vals(m_bins.size()-1, 0.0) { }
    // Copy constructor (so that we can make a list of Histograms)
    Histogram(Histogram const & other):
      m_bins(other.m_bins),
      m_vals(other.m_vals) {}
    // Assignment operator (so that we can make a list of Histograms)
    Histogram & operator=(Histogram const & other) {
      m_bins = other.m_bins;
      m_vals = other.m_vals;
      return *this;
    }

    // Add another histogram
    Histogram & operator+=(Histogram const & other) {
      assert(other.m_vals.size() == m_vals.size());
      for(unsigned i = 0; i < other.m_vals.size(); ++i) {
        m_vals[i] += other.m_vals[i];
      }
      return *this;
    }

    static vector<double> make_bins(HistogramBinsSpec spec) {
      vector<double> out(spec.nbins + 3);
      double width = (spec.max - spec.min)/static_cast<double>(spec.nbins);
      for(unsigned i = 0; i < out.size(); ++i) {
        out[i] = spec.min + width * (static_cast<double>(i) - 1);
      }
      return out;
    }

    void reset(vector<double> const & bins) {
      m_bins = bins;
      m_vals.clear();
      m_vals.resize(bins.size()-1, 0.0);
    }
    void reset() { fill(m_vals.begin(), m_vals.end(), 0.0); }

    vector<double> const & get_bins() const { return m_bins; }
    vector<double> const & get_vals() const { return m_vals; }

    // Adds a point to the histogram with given weight
    void update(double const val, double const weight) {
      // Ignore infinite values
      if(!isfinite(val)) {
        return;
      }
      if(val <= m_bins.front()) {
        *m_vals.begin() += weight;
        return;
      }
      if(val >= m_bins.back()) {
        *m_vals.rbegin() += weight;
        return;
      }
      vector<double>::const_iterator it = lower_bound(m_bins.begin(),
          m_bins.end(), val);
      m_vals[it - m_bins.begin() - 1] += weight;
    }

    // Normalize the histogram to sum to one
    void normalize() {
      double norm = 0;
      vector<double>::iterator it;
      for(it = m_vals.begin(); it != m_vals.end(); ++it) {
        norm += *it;
      }
      for(it = m_vals.begin(); it != m_vals.end(); ++it) {
        *it /= norm;
      }
    }
  private:
    vector<double> m_bins;
    vector<double> m_vals;
};

ostream & operator<<(ostream & os, Histogram const & hist) {
  vector<double> const & bins = hist.get_bins();
  vector<double> const & vals = hist.get_vals();
  for(unsigned i = 0; i < vals.size(); ++i) {
    os << 0.5*(bins[i] + bins[i+1]) << " " << vals[i] << endl;
  }
  return os;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
template<int rank>
class MultiHistogram {
///////////////////////////////////////////////////////////////////////////////
  public:
    MultiHistogram() {}
    MultiHistogram(vector<double> const * bins[rank]) {
      size_t ndof = 1;
      for(int i = 0; i < rank; ++i) {
        m_bins[i] = *bins[i];
        m_dim[i] = m_bins[i].size() - 1;
        ndof *= m_dim[i];
      }
      m_vals.assign(ndof, 0.0);
    }
    // Copy constructor (so that we can make a list of Histograms)
    MultiHistogram(MultiHistogram<rank> const & other):
      m_vals(other.m_vals) {
      for(int i = 0; i < rank; ++i) {
        m_bins[i] = other.m_bins[i];
        m_dim[i] = other.m_dim[i];
      }
    }
    // Assignment operator (so that we can make a list of Histograms)
    MultiHistogram<rank> & operator=(MultiHistogram<rank> const & other) {
      for(int i = 0; i < rank; ++i) {
        m_bins[i] = other.m_bins[i];
        m_dim[i] = other.m_dim[i];
      }
      m_vals = other.m_vals;
      return *this;
    }

    void reset(vector<double> const * bins[rank]) {
      size_t ndof = 1;
      for(int i = 0; i < rank; ++i) {
        m_bins[i] = *bins[i];
        m_dim[i] = m_bins[i].size() - 1;
        ndof *= m_dim[i];
      }
      m_vals.clear();
      m_vals.assign(ndof, 0.0);
    }
    void reset() {
      fill(m_vals.begin(), m_vals.end(), 0.0);
    }

    vector<double> const & get_bins(int const i) const { return m_bins[i]; }
    size_t get_dim(int const i) const { return m_dim[i]; }
    vector<double> const & get_vals() const { return m_vals; }

    // Adds a point to the histogram with given weight
    void update(double const val[rank], double const weight) {
      // Ignore infinite values
      for(int i = 0; i < rank; ++i) {
        if(!isfinite(val[i])) {
          return;
        }
      }

      int idx = 0;
      int stride = 1;
      for(int i = rank-1; i >= 0; --i) {
        if(val[i] <= m_bins[i].front()) {
          idx += 0*stride;
        }
        else if(val[i] >= m_bins[i].back()) {
          idx += (m_dim[i] - 1)*stride;
        }
        else {
          vector<double>::const_iterator it = lower_bound(m_bins[i].begin(),
              m_bins[i].end(), val[i]);
          idx += (it - m_bins[i].begin() - 1)*stride;
        }
        stride *= m_dim[i];
      }
      m_vals[idx] += weight;
    }

    // Normalize the histogram to sum to one
    void normalize() {
      double norm = 0;
      vector<double>::iterator it;
      for(it = m_vals.begin(); it != m_vals.end(); ++it) {
        norm += *it;
      }
      for(it = m_vals.begin(); it != m_vals.end(); ++it) {
        *it /= norm;
      }
    }
  private:
    vector<double> m_bins[rank];
    size_t m_dim[rank];
    vector<double> m_vals;
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class Linear2DInterpolator {
///////////////////////////////////////////////////////////////////////////////
  public:
    Linear2DInterpolator() {}
    Linear2DInterpolator(
        vector<double> const & x,
        vector<double> const & y,
        vector<double> const & f): m_x(x), m_y(y), m_f(f) {
      assert(m_f.size() == m_x.size()*m_y.size());
    }

    void reset(
        vector<double> const & x,
        vector<double> const & y,
        vector<double> const & f) {
      m_x = x;
      m_y = y;
      m_f = f;
      assert(m_f.size() == m_x.size()*m_y.size());
    }

    inline int index(int i, int j) const {
      return i*m_y.size() + j;
    }
    void find_interp_index_weight(vector<double> const & xg,
        double const xp, size_t * idx, double * w1, double * w2) {
      if(xp <= xg.front()) {
        *idx = 0;
        *w1 = 1.0;
        *w2 = 0.0;
      }
      else if(xp >= xg.back()) {
        *idx = xg.size() - 2;
        *w1 = 0.0;
        *w2 = 1.0;
      }
      else {
        vector<double>::const_iterator it =
          lower_bound(xg.begin(), xg.end(), xp);
        *idx = it - xg.begin() - 1;
        double const xg_idx_p = *it;
        double const xg_idx   = *(--it);
        *w2 = (xp - xg_idx)/(xg_idx_p - xg_idx);
        *w1 = 1. - *w2;
      }
      assert(*w1 >= 0 && *w1 <= 1);
      assert(*w2 >= 0 && *w2 <= 1);
    }
    double operator()(double const x, double const y) {
      size_t ix, iy;
      double wx[2], wy[2];
      find_interp_index_weight(m_x, x, &ix, &wx[0], &wx[1]);
      find_interp_index_weight(m_y, y, &iy, &wy[0], &wy[1]);

      double sum = 0;
      sum += m_f[index(ix+0,iy+0)]*wx[0]*wy[0];
      sum += m_f[index(ix+0,iy+1)]*wx[0]*wy[1];
      sum += m_f[index(ix+1,iy+0)]*wx[1]*wy[0];
      sum += m_f[index(ix+1,iy+1)]*wx[1]*wy[1];

      return sum;
    }
  private:
    // 1D arrays with the interpolation points
    vector<double> m_x, m_y;
    // 2D array with the function values at the interpolation points
    vector<double> m_f;
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class SkyNetTable {
///////////////////////////////////////////////////////////////////////////////
  public:
    SkyNetTable(string const & dens_map_file_name,
        string const & grid_file_name) {
      herr_t ierr;
      hid_t file_id;
      hsize_t dims[2];

      /// read dnsmap_file ////////////////////////////////////////////////////
      file_id = H5Fopen(dens_map_file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        MYH5CHECK(file_id);

      ierr = H5LTget_dataset_info(file_id, "Ye", &dims[0], NULL, NULL);
        MYH5CHECK(ierr);
      vector<double> dmap_ye(dims[0]);
      ierr = H5LTread_dataset_double(file_id, "Ye", &dmap_ye[0]);
        MYH5CHECK(ierr);

      ierr = H5LTget_dataset_info(file_id, "entropy", &dims[1], NULL, NULL);
        MYH5CHECK(ierr);
      vector<double> dmap_entr(dims[1]);
      ierr = H5LTread_dataset_double(file_id, "entropy", &dmap_entr[0]);
        MYH5CHECK(ierr);

      vector<double> dmap_rho(dims[0]*dims[1]);
      ierr = H5LTread_dataset_double(file_id, "density", &dmap_rho[0]);
        MYH5CHECK(ierr);
      for(vector<double>::iterator it = dmap_rho.begin();
          it != dmap_rho.end(); ++it) {
        *it = log10(*it);
      }

      m_densmap.reset(dmap_ye, dmap_entr, dmap_rho);

      H5Fclose(file_id);
      /////////////////////////////////////////////////////////////////////////

      /// read grid_file //////////////////////////////////////////////////////
      file_id = H5Fopen(grid_file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        MYH5CHECK(file_id);

      ierr = H5LTget_dataset_info(file_id, "Ye", &dims[0], NULL, NULL);
        MYH5CHECK(ierr);
      m_grid_ye.resize(dims[0]);
      ierr = H5LTread_dataset_double(file_id, "Ye", &m_grid_ye[0]);
        MYH5CHECK(ierr);

      ierr = H5LTget_dataset_info(file_id, "entropy", &dims[0], NULL, NULL);
        MYH5CHECK(ierr);
      m_grid_entropy.resize(dims[0]);
      ierr = H5LTread_dataset_double(file_id, "entropy", &m_grid_entropy[0]);
        MYH5CHECK(ierr);

      ierr = H5LTget_dataset_info(file_id, "tau", &dims[0], NULL, NULL);
        MYH5CHECK(ierr);
      m_grid_tau.resize(dims[0]);
      ierr = H5LTread_dataset_double(file_id, "tau", &m_grid_tau[0]);
        MYH5CHECK(ierr);

      H5Fclose(file_id);
      /////////////////////////////////////////////////////////////////////////
    }

    void make_bins(vector<double> * bins[3]) {
      make_bin_from_centers(m_grid_ye, bins[0]);
      make_bin_from_centers(m_grid_entropy, bins[1]);
      make_bin_from_centers(m_grid_tau, bins[2]);
    }

    inline double tau(double const ye, double const entropy, double const rho,
        double const vel, double const rad) {
      double const rho_b = pow(10.0, m_densmap(ye, entropy));
      double const tau_0 = 0.5 * M_E * (rad/vel) * TIME_TO_MS;
      double const tau_b = tau_0 * pow(rho/rho_b, 1.0/3.0);
      return tau_b;
    }

    vector<double> const & get_grid_ye() const { return m_grid_ye; };
    vector<double> const & get_grid_entropy() const { return m_grid_entropy; };
    vector<double> const & get_grid_tau() const { return m_grid_tau; };
  private:
    void make_bin_from_centers(vector<double> const & c, vector<double> * b) {
      assert(c.size() > 1);
      b->resize(c.size() + 1);
      double delta = c[1] - c[0];
      b->at(0) = c[0] - 0.5*delta;
      for(int i = 1; i < c.size(); ++i) {
        b->at(i) = c[i-1] + 0.5*delta;
        delta = c[i] - c[i-1];
      }
      b->at(c.size()) = c.back() + 0.5*delta;
    }
  private:
    Linear2DInterpolator m_densmap;
    vector<double> m_grid_ye;
    vector<double> m_grid_entropy;
    vector<double> m_grid_tau;
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
namespace Vars {
///////////////////////////////////////////////////////////////////////////////
class Entropy {
  public:
    static string name() { return string("entropy"); }
    static double extract(double const * vals) {
      return vals[variable::entropy];
    }
};

class Flux {
  public:
    static string name() { return string("flux"); }
    static double extract(double const * vals) {
      return vals[variable::flux];
    }
};

class LogRho {
  public:
    static string name() { return string("log_rho"); }
    static double extract(double const * vals) {
      return log10(vals[variable::rho]);
    }
};

class Rho {
  public:
    static string name() { return string("rho"); }
    static double extract(double const * vals) {
      return vals[variable::rho];
    }
};

class Temperature {
  public:
    static string name() { return string("temperature"); }
    static double extract(double const * vals) {
      return vals[variable::temperature];
    }
};

class Theta {
  public:
    static string name() { return string("theta"); }
    static double extract(double const * vals) {
      double const rad = sqrt(SQ(vals[variable::x]) + SQ(vals[variable::y]) +
        SQ(vals[variable::z]));
      return acos(vals[variable::z]/rad);
    }
};

class Velocity {
  public:
    static string name() { return string("vel"); }
    static double extract(double const * vals) {
      return sqrt(1 - 1/SQ(vals[variable::w_lorentz]));
    }
};

class VelocityInf {
  public:
    static string name() { return string("vel_inf"); }
    static double extract(double const * vals) {
      return sqrt(2*vals[variable::eninf]);
    }
};

class ElectronFraction {
  public:
    static string name() { return string("ye"); }
    static double extract(double const * vals) {
      return vals[variable::ye];
    }
};

} // namespace Vars
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
class OutflowAnalysis {
///////////////////////////////////////////////////////////////////////////////
  public:
    virtual ~OutflowAnalysis() {}
    virtual void update(OutflowData const & data) = 0;
    virtual void write() = 0;
};

// Computes the time history of the histogram of a variable
template<typename Variable>
class HistogramSequence: public OutflowAnalysis {
  public:
    HistogramSequence(vector<double> const & bins): m_bins(bins) {}
    virtual ~HistogramSequence() {}

    void update(OutflowData const & data) {
      m_times.push_back(data.time);
      m_history.push_back(Histogram(m_bins));
      list<Histogram>::reverse_iterator h = m_history.rbegin();

      for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta)
      // Remember that the phi = 2 pi point is duplicated
      for(int iphi = 0; iphi < data.grid.get_nphi() - 1; ++iphi) {
        double const * vals = data.get_data_at_point(
            data.grid.sph_index_map(itheta, iphi));
        if(vals[variable::eninf] >= config.min_eninf) {
          double const myval = Variable::extract(vals);

          double const weight = vals[variable::flux] *
            vals[variable::surface_element] * data.grid.get_delta_time();

          h->update(myval, weight);
        }
      }
    }

    void write() {
      Histogram ave(m_bins);

      string ofname = string("hist_") + Variable::name() + string(".xg");
      ofstream ofile(ofname.c_str());
      list<double>::iterator t;
      list<Histogram>::iterator h;
      for(h = m_history.begin(), t = m_times.begin(); h != m_history.end();
          ++h, ++t) {
        assert(t != m_times.end());
        ave += *h;
        //h->normalize();
        ofile << "# Time = " << *t << endl;
        ofile << *h << endl << endl << endl;
      }
      ofile.close();

      //ave.normalize();
      ofname = string("hist_") + Variable::name() + string(".dat");
      ofile.open(ofname.c_str());
      ofile << ave;
      ofile.close();
    }
  private:
    vector<double> const m_bins;
    list<Histogram> m_history;
    list<double> m_times;
};

// Creates mass weighted and azimuthally averaged profiles
template<typename Variable>
class AngularProfile: public OutflowAnalysis {
  public:
    AngularProfile() {}
    virtual ~AngularProfile() {}

    void update(OutflowData const & data) {
      m_times.push_back(data.time);

      m_data.push_back(vector<double>());
      list<vector<double> >::reverse_iterator v = m_data.rbegin();

      m_grid.push_back(vector<double>());
      list<vector<double> >::reverse_iterator g = m_grid.rbegin();

      m_weight.resize(data.grid.get_ntheta());
      v->resize(data.grid.get_ntheta());
      g->resize(data.grid.get_ntheta());

      for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta) {
        m_weight[itheta] = 0;
        v->at(itheta) = 0;
        g->at(itheta) = data.grid.get_theta(itheta);
        // Remember that the phi = 2 pi point is duplicated
        for(int iphi = 0; iphi < data.grid.get_nphi() - 1; ++iphi) {
          double const * vals = data.get_data_at_point(
              data.grid.sph_index_map(itheta, iphi));
          if(vals[variable::eninf] >= config.min_eninf) {
            m_weight[itheta] += vals[variable::flux];
            v->at(itheta) += Variable::extract(vals) * vals[variable::flux];
          }
        }
        v->at(itheta) /= m_weight[itheta];
      }
    }

    void write() {
      string ofname = string("profile_") + Variable::name() + string(".xg");
      ofstream ofile(ofname.c_str());
      list<double>::iterator t;
      list<vector<double> >::iterator v;
      list<vector<double> >::iterator g;
      for(t = m_times.begin(), v = m_data.begin(), g = m_grid.begin();
          t != m_times.end();
          ++t, ++v, ++g) {
        assert(v != m_data.end());
        assert(g != m_grid.end());
        ofile << "# Time = " << *t << endl;
        for(int i = 0; i < v->size(); ++i) {
          ofile << g->at(i) << " " << v->at(i) << endl;
        }
        ofile << endl << endl;
      }
      ofile.close();
    }
  private:
    vector<double> m_weight;
    list<double> m_times;
    list<vector<double> > m_data;
    list<vector<double> > m_grid;
};

// Computes the correlation between two given properties of the ejecta
template<typename Variable0, typename Variable1>
class Correlation: public OutflowAnalysis {
  public:
    Correlation(
        vector<double> const & bins0,
        vector<double> const & bins1) {
      vector<double> const * bins[2] = {&bins0, &bins1};
      m_data.reset(&bins[0]);
    }
    virtual ~Correlation() {}

    void update(OutflowData const & data) {
      for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta)
      // Remember that the phi = 2 pi point is duplicated
      for(int iphi = 0; iphi < data.grid.get_nphi() - 1; ++iphi) {
        double const * vals = data.get_data_at_point(
            data.grid.sph_index_map(itheta, iphi));
        if(vals[variable::eninf] >= config.min_eninf) {
          double const weight = vals[variable::flux] *
            vals[variable::surface_element] * data.grid.get_delta_time();

          double vars[2];
          vars[0] = Variable0::extract(vals);
          vars[1] = Variable1::extract(vals);

          m_data.update(vars, weight);
        }
      }
    }

    void write() {
      herr_t ierr;
      hid_t file_id;
      hsize_t dims[3];

      string ofname = string("corr_") + Variable0::name() + "_" +
        Variable1::name() + string(".h5");
      file_id = H5Fcreate(ofname.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
          H5P_DEFAULT); MYH5CHECK(file_id);

      vector<double> const & bins0 = m_data.get_bins(0);
      dims[0] = bins0.size();
      ierr = H5LTmake_dataset_double(file_id, Variable0::name().c_str(), 1,
          &dims[0], &bins0[0]);
        MYH5CHECK(ierr);

      vector<double> const & bins1 = m_data.get_bins(1);
      dims[1] = bins1.size();
      ierr = H5LTmake_dataset_double(file_id, Variable1::name().c_str(), 1,
          &dims[1], &bins1[0]);
        MYH5CHECK(ierr);

      vector<double> const & mass = m_data.get_vals();
      dims[2] = mass.size();
      assert(dims[2] == (dims[0]-1)*(dims[1]-1));
      --dims[0];
      --dims[1];
      ierr = H5LTmake_dataset_double(file_id, "mass", 2, &dims[0], &mass[0]);
        MYH5CHECK(ierr);

      H5Fclose(file_id);
    }
  private:
      MultiHistogram<2> m_data;
};

// Creates an histogram to be used to compute nucleosynthetic yields
class EjectaStatistics: public OutflowAnalysis {
  public:
    EjectaStatistics(
        string const & dens_map_file_name,
        string const & grid_file_name):
        m_nusyn_table(dens_map_file_name, grid_file_name) {
        vector<double> bins[3];
        vector<double> * pbins[3] = {&bins[0], &bins[1], &bins[2]};
        m_nusyn_table.make_bins(&pbins[0]);
        m_data.reset(const_cast<vector<double> const **>(&pbins[0]));
      }
    virtual ~EjectaStatistics() {}

    void update(OutflowData const & data) {
      for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta)
      // Remember that the phi = 2 pi point is duplicated
      for(int iphi = 0; iphi < data.grid.get_nphi() - 1; ++iphi) {
        double const * vals = data.get_data_at_point(
            data.grid.sph_index_map(itheta, iphi));
        if(vals[variable::eninf] >= config.min_eninf) {
          double const weight = vals[variable::flux] *
            vals[variable::surface_element] * data.grid.get_delta_time();

          double const ye   = Vars::ElectronFraction::extract(vals);
          double const entr = Vars::Entropy::extract(vals);
          double const rho0 = Vars::Rho::extract(vals) * DENS_TO_CGS;
          double const vel  = Vars::Velocity::extract(vals);
          double const tau  = m_nusyn_table.tau(ye, entr, rho0, vel,
              data.grid.get_radius());
          double const vars[3] = {ye, entr, tau};

          m_data.update(vars, weight);
        }
      }
    }

    void write() {
      herr_t ierr;
      hid_t file_id;
      hsize_t dims[3];
      file_id = H5Fcreate("ejecta.h5", H5F_ACC_TRUNC, H5P_DEFAULT,
          H5P_DEFAULT); MYH5CHECK(file_id);

      vector<double> const & grid_ye = m_nusyn_table.get_grid_ye();
      dims[0] = grid_ye.size();
      ierr = H5LTmake_dataset_double(file_id, "/Ye", 1, &dims[0], &grid_ye[0]);
        MYH5CHECK(ierr);

      vector<double> const & grid_entropy = m_nusyn_table.get_grid_entropy();
      dims[1] = grid_entropy.size();
      ierr = H5LTmake_dataset_double(file_id, "/entropy", 1, &dims[1],
          &grid_entropy[0]); MYH5CHECK(ierr);

      vector<double> const & grid_tau = m_nusyn_table.get_grid_tau();
      dims[2] = grid_tau.size();
      ierr = H5LTmake_dataset_double(file_id, "/tau", 1, &dims[2],
          &grid_tau[0]); MYH5CHECK(ierr);

      vector<double> const & vals = m_data.get_vals();
      assert(vals.size() == dims[0]*dims[1]*dims[2]);
      ierr = H5LTmake_dataset_double(file_id, "/mass", 3, &dims[0], &vals[0]);
        MYH5CHECK(ierr);

      H5Fclose(file_id);
    }
  private:
    MultiHistogram<3> m_data;
    SkyNetTable m_nusyn_table;

};

// Computes mass weighted time averages of 2D data
class MassAverages: public OutflowAnalysis {
  public:
    MassAverages(OutflowGrid const & grid): m_avg(grid) { }

    void update(OutflowData const & data) {
      assert(data.grid.get_ndof() == m_avg.grid.get_ndof());
      double const dt = data.grid.get_delta_time();
      for(int i = 0; i < data.grid.get_ndof(); ++i) {
        double const * vals = data.get_data_at_point(i);
        double * accum = m_avg.get_data_at_point(i);
        if(vals[variable::eninf] >= config.min_eninf) {
          accum[variable::flux] += vals[variable::flux] * dt;
          int const var_idx_map[] = {
            variable::w_lorentz,
            variable::eninf,
            variable::rho,
            variable::ye,
            variable::entropy,
            variable::temperature
          };
          for(unsigned vi = 0; vi < length(var_idx_map); ++vi) {
            accum[var_idx_map[vi]] += vals[var_idx_map[vi]] *
              vals[variable::flux] * dt;
          }
        }
      }
    }

    void write() {
      int const siz = m_avg.grid.get_ndof();
      for(int i = 0; i < siz; ++i) {
        double * vals = m_avg.get_data_at_point(i);
        int const var_idx_map[] = {
          variable::w_lorentz,
          variable::eninf,
          variable::rho,
          variable::ye,
          variable::entropy,
          variable::temperature,
        };
        for(unsigned vi = 0; vi < length(var_idx_map); ++vi) {
          vals[var_idx_map[vi]] /= vals[variable::flux];
        }
      }
      ofstream ofile("mass_averages.dat");
      ofile << m_avg.grid;
      ofile << m_avg;
      ofile.close();
    }
  private:
    OutflowData m_avg;
};

// Computes the time-series of the total ejected flux and mass
class TotalFlux: public OutflowAnalysis {
  public:
    void update(OutflowData const & data) {
      time.push_back(data.time);
      double f = 0;
      for(int itheta = 0; itheta < data.grid.get_ntheta(); ++itheta)
      for(int iphi = 0; iphi < data.grid.get_nphi()-1; ++iphi) {
        double const * vals = data.get_data_at_point(
            data.grid.sph_index_map(itheta, iphi));
        if(vals[variable::eninf] >= config.min_eninf) {
          f += vals[variable::flux] * vals[variable::surface_element];
        }
      }
      flux.push_back(f);
    }
    void write() {
      ofstream ofile("total_flux.dat");
      ofile << "# 1:time 2:flux 3:mass" << endl;

      double dt = time[1] - time[0];
      double mass = 0;
      for(unsigned it = 0; it < time.size(); ++it) {
        mass += flux[it] * dt;
        ofile << time[it] << " " << flux[it] << " " << mass << endl;
      }

      ofile.close();
    }
  private:
    vector<double> time;
    vector<double> flux;
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv) {
///////////////////////////////////////////////////////////////////////////////
  // Parse the CLI
  if(argc < 4) {
    cout << "Usage: " << string(argv[0]) << "/path/to/skynet/tabulated_rho.h5 "
            "/path/to/skynet/grid.h5 /path/to/data1 ..." << endl;
    return EXIT_FAILURE;
  }
  string dens_map_file_name(argv[1]);
  string grid_file_name(argv[2]);
  list<string> input_files;
  for(int fn = 3; fn < argc; ++fn) {
    input_files.push_back(string(argv[fn]));
  }

  // Objects for reading the data
  OutflowReader reader(input_files);
  OutflowData data(reader.get_grid());

  // List of analysis modules
  OutflowAnalysis * modules[] = {
    new HistogramSequence<Vars::Entropy>(
          Histogram::make_bins(config.histogram.entropy)),
    new HistogramSequence<Vars::LogRho>(
          Histogram::make_bins(config.histogram.log_rho)),
    new HistogramSequence<Vars::Temperature>(
          Histogram::make_bins(config.histogram.temperature)),
    new HistogramSequence<Vars::Theta>(
          Histogram::make_bins(config.histogram.theta)),
    new HistogramSequence<Vars::Velocity>(
          Histogram::make_bins(config.histogram.vel)),
    new HistogramSequence<Vars::VelocityInf>(
          Histogram::make_bins(config.histogram.vel_inf)),
    new HistogramSequence<Vars::ElectronFraction>(
          Histogram::make_bins(config.histogram.ye)),
    new AngularProfile<Vars::Entropy>(),
    new AngularProfile<Vars::Flux>(),
    new AngularProfile<Vars::Rho>(),
    new AngularProfile<Vars::Temperature>(),
    new AngularProfile<Vars::Velocity>(),
    new AngularProfile<Vars::VelocityInf>(),
    new AngularProfile<Vars::ElectronFraction>(),
    new Correlation<Vars::ElectronFraction, Vars::Entropy>(
          Histogram::make_bins(config.histogram.ye),
          Histogram::make_bins(config.histogram.entropy)),
    new Correlation<Vars::ElectronFraction, Vars::Theta>(
          Histogram::make_bins(config.histogram.ye),
          Histogram::make_bins(config.histogram.theta)),
    new Correlation<Vars::VelocityInf, Vars::Theta>(
          Histogram::make_bins(config.histogram.vel_inf),
          Histogram::make_bins(config.histogram.theta)),
    new EjectaStatistics(dens_map_file_name, grid_file_name),
    new MassAverages(reader.get_grid()),
    new TotalFlux(),
    NULL
  };

  // Analyze the data
  int ierr;
  do {
    ierr = reader.parse_block(&data);
    if(ierr == OutflowReader::error::ok) {
      cout << "it = " << data.iter << "\t" << "t = " << data.time << endl;
      for(OutflowAnalysis ** a = &modules[0]; *a != NULL; ++a) {
        (*a)->update(data);
      }
    }
    else if(ierr == OutflowReader::error::fail) {
      cerr << "Failed while reading the data. Last read iteration was "
           << reader.get_prev_iter() << "." << endl;
      exit(1);
    }
  } while(ierr != OutflowReader::error::finished);

  // Output the results
  for(OutflowAnalysis ** a = &modules[0]; *a != NULL; ++a) {
    (*a)->write();
  }

  // Cleanup
  for(OutflowAnalysis ** a = &modules[0]; *a != NULL; ++a) {
    delete *a;
  }

  return EXIT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////////

// vim: set sw=2 :
