#!/usr/bin/env bash

function call {
    echo $*
    eval $*
}

ANALYSIS_HOME=$(cd $(dirname $0); pwd)

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/simulation"
    exit 1
fi
target=$1

cd $target

call cd collated
call python $ANALYSIS_HOME/mass_from_outflow.py || exit 1
call cd ..

call mkdir -p outflow
call mv collated/ejected_det_?.dat outflow || exit 1

call mkdir -p outflow_0
call cd outflow_0
call ${ANALYSIS_HOME}/outflowed \
    ${ANALYSIS_HOME}/skynet/densmap.h5 \
    ${ANALYSIS_HOME}/skynet/grid.h5 \
    ../output-????/data/outflow_surface_det_0_fluxdens.asc || exit 1
call ${ANALYSIS_HOME}/yields.py \
    /global/homes/a/aup1075/scripts/ejecta/skynet/tabulated_nucsyn.h5 \
    /global/homes/a/aup1075/scripts/ejecta/skynet/solar_r.dat \
    ejecta.h5 || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_corr.py \
    corr_ye_entropy.h5 || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 1.0 --max 100.0 --log profile_entropy.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 1e-15 --max 1e-10 --log profile_flux.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    -r 6.17e17 --min 1e5 --max 1e8 --log profile_rho.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 0.0 --max 1.0 profile_temperature.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 0.0 --max 0.5 --cmap 'seismic' profile_ye.xg || exit 1
call cd ..

call mkdir -p outflow_1
call cd outflow_1
call ${ANALYSIS_HOME}/outflowed \
    ${ANALYSIS_HOME}/skynet/densmap.h5 \
    ${ANALYSIS_HOME}/skynet/grid.h5 \
    ../output-????/data/outflow_surface_det_1_fluxdens.asc || exit 1
call ${ANALYSIS_HOME}/yields.py \
    /global/homes/a/aup1075/scripts/ejecta/skynet/tabulated_nucsyn.h5 \
    /global/homes/a/aup1075/scripts/ejecta/skynet/solar_r.dat \
    ejecta.h5 || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_corr.py \
    corr_ye_entropy.h5 || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 1.0 --max 100.0 --log profile_entropy.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 1e-15 --max 1e-10 --log profile_flux.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    -r 6.17e17 --min 1e5 --max 1e8 --log profile_rho.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 0.0 --max 1.0 profile_temperature.xg || exit 1
call ${ANALYSIS_HOME}/outflowed_vis_profile.py \
    --min 0.0 --max 0.5 --cmap 'seismic' profile_ye.xg || exit 1
call cd ..

call ${ANALYSIS_HOME}/summary.py . || exit 1

call touch ejecta.done
