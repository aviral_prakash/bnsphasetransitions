#!/usr/bin/env python

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import h5py
import numpy as np
import sys

if len(sys.argv) < 2:
    print("Usage: {} /path/to/outflow/data/".format(sys.argv[0]))
    exit(1)
dpath = sys.argv[1]

ye_dfile = h5py.File("{}/corr_ye_theta.h5".format(dpath), "r")
vel_dfile = h5py.File("{}/corr_vel_inf_theta.h5".format(dpath), "r")

vel_v = np.array(vel_dfile["vel_inf"])
thf   = np.array(vel_dfile["theta"])
vel_M = np.array(vel_dfile["mass"])

ye_ye = np.array(ye_dfile["ye"])
ye_M = np.array(ye_dfile["mass"])

thc     = 0.5*(thf[1:] + thf[:-1])
velc    = 0.5*(vel_v[1:] + vel_v[:-1])
yec     = 0.5*(ye_ye[1:] + ye_ye[:-1])

M_of_th = np.sum(vel_M, axis=0)
vel_ave = np.sum(velc[:,np.newaxis]*vel_M, axis=0)/M_of_th
ye_ave  = np.sum(yec[:,np.newaxis]*ye_M, axis=0)/M_of_th

print("# 1:theta 2:M 3:vel 4:ye")
for i in range(1, thc.shape[0]-1):
    s = "{} {} {} {}".format(thc[i], M_of_th[i], vel_ave[i], ye_ave[i])
    print(s)

