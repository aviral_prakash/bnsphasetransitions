#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Computes the EM signal from the interaction of the outflow with the
# interstellar medium. (ISM)

import numpy as np
from math import *
import os
import argparse

usage = "%prog [options]"
desc="Computes the EM signal coming from the interaction of the ouflow" + \
     "material with the ISM."

parser = argparse.ArgumentParser(usage,description=desc)

parser.add_argument("-k", "--kinetic",  dest = "ek", type= float, default=0,
      help="kinetic energy of the ejecta [erg]")
parser.add_argument("-b", "--beta", dest = "beta", type= float, default=0,
      help="avg beta of the ejecta [c]")
parser.add_argument("-i", "--ism_nb", dest= "n_ism", type= float, default=0,
      help="baryon number density of the ISM [cm^-3]")
parser.add_argument("-m", "--mass", dest= "m_unb", type= float, default=0,
            help="unbound mass [M_sun]")

args = parser.parse_args()


def nuc_peak (m_unb, beta): #output expressed in days for t and erg/s for the
    # luminosity
    #see Eqs(1-2) in W. East, V. Paschalidis, F. Pretorius 1511.01093v1 (2015)
    # also in J. Barnes and D. Kasen, Astrophys. J.775, 18 (2013), 1303.5787
    t   = 0.25   * sqrt(m_unb * 1.e2) *  sqrt(0.3 / beta)
    lum = 2.0e41 * sqrt(m_unb * 1.e2) *  sqrt(beta / 0.3)
    return t,lum

def nuc_peak_gross(m_unb, beta, kappa, alpha):#output expressed in days for
    #t and erg/s the luminosity and the temperature in Kelvin
    #See Eqs(4) Grossman, Korobkin, Rosswog, Piran MNRAS 439 (2014)
    kappa_10 = kappa / 10.
    m_unb_m2 = m_unb / 0.01
    v_m1     = beta/0.1

    t    = 4.9e0 * pow((kappa_10 * m_unb_m2) / v_m1, 0.5)
    lum  = 2.5e40 * pow(m_unb_m2,(1.-alpha/2.)) * pow(v_m1/kappa_10,alpha/2.)
    temp = 2200 * pow(kappa_10, -(alpha+2)/8.) * pow(v_m1, (alpha-2.)/8.) * \
           pow(m_unb_m2, -alpha/8.)
    return t, lum, temp


def ism_flare (e_k0, beta0, n_ism0, dist, freq): #output in years for the time
    # and in mJy for the EM flux
    #see Eq(3) in W. East, V. Paschalidis, F. Pretorius 1511.01093v1 (2015)
    t = 6. * pow(e_k0 / 1.e51,1./3.) * pow(n_ism0*10,-1./3.) * \
        pow(beta/0.3,-5./3.)
    F = 0.6 * (e_k0 / 1.e51) * pow(n_ism0*10,7./8.) * pow(beta/0.3,11./4.) * \
        pow(freq, -3./4.) * pow(dist/ 100,-2.)
    return t, F

def decoupling(e_k0, beta0, n_ism0): # radius in cm and t in days
    # See Ehud Nakar and Tsvi Piran 1102.1020v1 (2011)
    radius =  1.e17 * pow(e_k0/1.e49, 1./3.) * pow(n_ism0,-1./3.) * \
              pow(beta0,-2./3.)
    t = 30. * pow(e_k0/1.e49, 1./3.) * pow(n_ism0,-1./3.) * pow(beta0,-5./3.)

    return t, radius

#Radio spectrum from Ehud Nakar and Tsvi Piran 1102.1020v1 (2011)
#Electron synchrotron frequency
def freq_m (n_ism0, eps_B, eps_e, beta0, r_dec, dist):
    #See discussion in Sec.3 of 1102.1020v1
    eps_B = eps_B * 10.
    eps_e = eps_e * 10.
    f_m = 1.0 * sqrt(n_ism0) * sqrt(eps_B) * pow(eps_e,2.) *  pow(beta0,5.)
    flux   = 0.5 * pow(r_dec/1.e17,3.0) * pow(n_ism0,3./2.) * pow(eps_B,-0.5) * \
            beta0 * pow(dist/1.e17,-2.) #mJy
    return f_m, flux

#Synchrotron self-absorption frequency
def freq_abs (n_ism0, eps_B, eps_e, beta0):
    #In the case that f_abs > f_m
    p = 2.5 # for mildly relativistic shocks p [2.1,2.5] while for Newtonian p [2.5,3.0]
    c1 = 2./(p+4.)
    c2 = (6.+p)/(2.*(p+4.))
    c3 = (2.+p)/(2.*(p+4.))
    c4 = 2.*(p-1.)/(p+4.)
    c5 = (5.*p-2.)/(p+4.)
    f_abs = pow(r_dec/1.e17,c1) * pow(n_ism0,c2)*pow(eps_B,c3)*pow(eps_e,c4) * \
            pow(beta0,c5) #GHz
    return f_abs

def flux_peak_obs (n_ism0, p, eps_B, eps_e, e_k0, beta0, dist, freq_obs):
    e_k0_49 = e_k0 / 1.e49
    eps_B_m1 = eps_B * 10.
    eps_e_m1 = eps_e * 10.
    d_27     = dist / 1.e27
    A = (p + 1.)/4.
    B = p - 1.
    C = (5.*p - 7) / 2.
    D = - ((p-1.)/2.)
    fl_obs = 0.3 * e_k0_49 * pow(n_ism0,A) * pow(eps_B_m1,A) * \
            pow(eps_e_m1,p-1.) * pow(beta0,C) * pow(d_27,-2.) * \
            pow(freq_obs/1.4,D)
    return fl_obs

#From Edo Berger's review on short GRBs http://arxiv.org/pdf/1311.2603v1.pdf
#Eqs. (2-5)
#t_d observer frame-time in days (dt_d~dt_c/2 Gamma) t_c comoving
# in other words t_d is the observed time post-burst in days
#z redshift

#Break frequency corresponding to self-absorption
def freq_self_abs (n_ism0, eps_B, eps_e, e_k0, p, z):
    eps_bar_e = eps_e * (p-2.)/(p-1.)
    e_k0_52   = e_k0 / 1.e52
    f_abs = 1.24e9 * (p-1.)/pow(3*p+2,0.6) * pow(1.+z,-1.) * pow(eps_bar_e,-1.) * \
            pow(eps_B, 0.2) * pow(n_ism0, 0.6) * pow(e_k0_52, 0.2)
    return f_abs #Hz
#Break frequency corresponding to the minimum Lorentz factor
def freq_min_lf (n_ism0, eps_B, eps_e, e_k0, p, z, t_d):
    eps_bar_e = eps_e * (p-2.)/(p-1.)
    e_k0_52   = e_k0 / 1.e52
    f_min =  3.73e15 * (p-0.67)* sqrt(1.+z) * pow(eps_bar_e,-0.5) * \
            sqrt(eps_B) * sqrt(e_k0_52) * pow(t_d,-1.5)
    return f_min #Hz
#Break frequency corresponding to synchrotron cooling
def freq_min_lf (n_ism0, eps_B, eps_e, e_k0, p, z, t_d):
    eps_bar_e = eps_e * (p-2.)/(p-1.)
    e_k0_52   = e_k0 / 1.e52
    f_cool = 6.73e13 * (p-0.46) * exp(-1.16*p) * pow(1. + z,-0.5) * \
            pow(eps_bar_e,-1.5) * 1./n_ism0 * pow(e_k0_52,-0.5) * \
            pow(t_d,-0.5)
    return f_cool #Hz

def flux_abs (n_ism0, eps_B, eps_e, e_k0, p, z, t_d, d_lum):
    eps_bar_e = eps_e * (p-2.)/(p-1.)
    e_k0_52   = e_k0 / 1.e52
    d_lum_28  = d_lum / 1.e28
    fl_abs    = 0.65 * pow(p-1.,1.2) / ((3.*p-1.)*pow(3*p+2,0.2)) * \
                sqrt(1.+z) * 1./eps_bar_e * pow(eps_B,0.4) * pow(n_ism0, 0.7) * \
                pow(e_k0_52,0.9) * pow(t_d,0.5) * pow(d_lum_28,-2.)
    return fl_abs #mJy

e_k = args.ek
beta = args.beta
n_ism = args.n_ism
m_unb = args.m_unb

t_nuc, lum_nuc = nuc_peak(m_unb, beta)
# The value of alpha accounts for time evolution of the nuclear energy
# generation rate as found by
# Korobkin O., Rosswog S., Arcones A., Winteler C., 2012, MNRAS, 426,1940
alpha  = 1.3
t_p, lum_p , temp_p= nuc_peak_gross(m_unb, beta, 10., alpha)

d = 100. # in Mpc
f_obs = 1. # in GHz
t_ism, em_flux = ism_flare(e_k, beta, n_ism, d, f_obs)
t_dec, r_dec  = decoupling(e_k, beta, n_ism)
eps_B = 0.1
eps_e = 0.1
p = 2.5
dist = 1e8 * 3.08e19 #100 Mpc in cm
f_m , flux_m   = freq_m(n_ism, eps_B, eps_e, beta, r_dec, dist)
f_a  = freq_abs(n_ism, eps_B, eps_e, beta)
dist_cm = 100. * 3.086e+24 #100 Mpc in cm
freq_obs = 1. #GHz
fl_obs = flux_peak_obs (n_ism, p, eps_B, eps_e, e_k, beta, dist_cm, freq_obs)

#Estimate by E. Berger's review with typical values of the
#remaining parameters
z = 0.5
eps_B = 0.1
eps_e = 0.1
p = 2.5
Gamma = 1. / sqrt(1-beta*beta)
theta = 0. #angle of observation
t_d = Gamma * (1.-beta*cos(theta))
d_lum = 100. * 3.086e+24 #100 Mpc in cm
fl_abs = flux_abs(n_ism, eps_B, eps_e, e_k, p, z, t_d, d_lum)

print "\n"
print "==================================================================="
print "from Grossman, Korobkin, Rosswog, Piran MNRAS 439 (2014)"
print "==================================================================="
print "r-proc peak luminosity                :", lum_p/1.e41, "10^41 erg/s"
print "r-proc peak time                      :", t_p, "days"
print "r-proc peak temperature               :", temp_p, "K"
print "==================================================================="
print "\n"
print "==================================================================="
print "from W. East, V. Paschalidis, F. Pretorius 1511.01093v1 (2015)"
print "==================================================================="
print "EM luminosity for the nuclear decay :", lum_nuc/1.e41, "10^41 erg/s"
print "Time of the r-process EM peak       :", t_nuc, "days"
print "==================================================================="
print "Max EM flux                        :", em_flux, "mJy"
print "Time of the second EM peak         :", t_ism, "years"
print "==================================================================="
print "\n"
print "==================================================================="
print "from Ehud Nakar and Tsvi Piran 1102.1020v1 (2011)"
print "==================================================================="
print "Decoupling time                    :", t_dec, "days"
print "Decoupling radius                  :", r_dec, "cm"
print "==================================================================="
print "Electron synchrotron freq           :", f_m, "GHz"
print "Synchrotron self-absorption freq    :", f_a, "GHz"
print "EM Flux                             :", fl_obs, "mJy"
print "==================================================================="
print "\n"
print "==================================================================="
print "from E. Berger (2013)"
print "==================================================================="
print "EM Flux                    :", fl_abs, "mJy"
print "t_d                        :", t_d, "days"
print "==================================================================="
