#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 simulation"
    exit 1
fi
target=$1
cd $target || exit 1

mkdir -p profiles/3d
for f in output-????/checkpoint; do
    if [ ! -f ${f}/profile.3D.done ]; then
        python -u ~/Runs/THC/AA/scripts/ExtractProfile3D.py \
            -p ${f} -d "$target" \
            -o profiles/3d/@ITER@.h5 || exit 1
        touch ${f}/profile.3D.done
    fi
done
touch profile.3D.done
