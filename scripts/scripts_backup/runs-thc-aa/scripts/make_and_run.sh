#!/usr/bin/env bash

SIMULATIONS=(
    'LS220_M135135_M0' \
    'LS220_M135135_M0_L5' \
    'LS220_M135135_M0_L25' \
    'LS220_M135135_M0_L50' \
    'LS220_M140120_M0_L5' \
    'LS220_M140120_M0_L25' \
    'LS220_M140120_M0_L50' \
)

function prepare_sim() {
    simulation=$1
    mkdir ${simulation} || exit 1
    cd ${simulation} || exit 1
    batchtools init \
        --parfile $HOME/Runs/THC/AA/runs/${simulation}.par \
        --exe $HOME/Runs/THC/AA/Cactus/exe/cactus_thc \
        --batch $HOME/Runs/THC/AA/Cactus/batchtools/templates/cactus/supermuc-ibmmpi.sub
    cat ../CONFIG.t \
        | sed "s|@NAME@|${simulation}|" \
        > BATCH/CONFIG || exit 1
}

function submit_sim() {
    simulation=$1
    cd $simulation || exit 1
    batchtools makesegment || exit 1
    batchtools submit || exit 1
}

for sim in ${SIMULATIONS[@]}; do
    (prepare_sim ${sim}) || exit 1
    (submit_sim ${sim})  || exit 1
done
