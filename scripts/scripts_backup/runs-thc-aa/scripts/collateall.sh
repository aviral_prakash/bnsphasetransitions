#!/usr/bin/env bash

function call {
    echo $*
    eval $*
}

ANALYSIS_HOME=$(cd $(dirname $0); pwd)

if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/simulation [tmax]"
    exit 1
fi
target=$1
shift

call cd $target

COLLATE_LIST="
    bnstrackergen-bns_positions..asc
    dens.norm1.asc
    dens_unbnd.norm1.asc
    dens_unbnd_bernoulli.norm1.asc
    dens_unbnd_garching.norm1.asc
    H.norm2.asc
    luminosity_nua.norm1.asc
    luminosity_nue.norm1.asc
    luminosity_nux.norm1.asc
    mp_Psi4_l0_m0_r400.00.asc
    mp_Psi4_l1_m0_r400.00.asc
    mp_Psi4_l1_m1_r400.00.asc
    mp_Psi4_l2_m0_r400.00.asc
    mp_Psi4_l2_m1_r400.00.asc
    mp_Psi4_l2_m2_r400.00.asc
    mp_Psi4_l3_m0_r400.00.asc
    mp_Psi4_l3_m1_r400.00.asc
    mp_Psi4_l3_m2_r400.00.asc
    mp_Psi4_l3_m3_r400.00.asc
    mp_Psi4_l4_m0_r400.00.asc
    mp_Psi4_l4_m1_r400.00.asc
    mp_Psi4_l4_m2_r400.00.asc
    mp_Psi4_l4_m3_r400.00.asc
    mp_Psi4_l4_m4_r400.00.asc
    outflow_det_0.asc
    outflow_det_1.asc
    outflow_det_2.asc
    outflow_det_3.asc
    rho.maximum.asc
    temperature.maximum.asc
    thc_leakagem0-thc_leakage_m0_flux..asc
"

mkdir -p collated
for name in $COLLATE_LIST; do
    segment=$(echo output-???? | cut -f 1 -d ' ')
    if [ -e ${segment}/data/${name} ]; then
        call python $ANALYSIS_HOME/collate.py -i -o collated/$name \
            output-????/data/$name $@ || exit 1
    fi
done

call touch collate.done
