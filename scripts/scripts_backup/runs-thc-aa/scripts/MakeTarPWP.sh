#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 [segment]"
    exit 1
fi

tar cvf $1.tar \
    $1/*.par \
    $1/*.out \
    $1/*.err \
    $1/data/bnstracker* \
    $1/data/alp.minimum.asc \
    $1/data/rho.xz.h5 \
    $1/data/rho.xy.h5 \
    $1/data/mp_Psi4_l?_m?_r*.asc \
    $1/data/H.norm2.asc \
    $1/data/dens.norm1.asc \
    $1/data/rho.maximum.asc \
    || exit 1
