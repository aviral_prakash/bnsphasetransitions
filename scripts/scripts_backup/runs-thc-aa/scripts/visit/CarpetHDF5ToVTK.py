#!/usr/bin/env python

# This requires PyEVTK to be insalled. You can get it with:
# $ hg clone https://bitbucket.org/pauloh/pyevtk PyEVTK

from __future__ import division

import argparse
import pyevtk
from pyevtk.hl import * 
import numpy as np
import os
import re
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator
from scidata.utils import locate
import time

def make_stretched_grid(xmin, xmax, xi, nlin, nlog):
    dx = xi/nlin
    x_lin = np.arange(0, xi, dx)
    x_log = 10.0**np.linspace(np.log10(xi), 0.0, nlog//2)
    x_grid = np.concatenate((x_lin, x_log))
    x_grid *= (xmax - xmin)/2.
    x_ave = (xmax + xmin)/2.
    return np.concatenate(((x_ave - x_grid)[::-1][:-1], x_grid + x_ave))

# Parse the CLI
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--outdir", dest="outdir", default=".",
        help="Output folder")
parser.add_argument("-p", "--path", dest="dpath", default='.',
        help="Path to the folder containing the CarpetIOHDF5 output")
parser.add_argument("-v", "--var", dest="vars", action="append", required=True,
        help="Include this variable in the VTK file")
parser.add_argument("--reflecting_xy", dest="reflecting_xy", action="store_true",
        help="Apply reflection symmetry across the xy-plane")
parser.add_argument("--xmin", type=float, default=-1.0,
        help="Include region with x >= xmin (default: -1)")
parser.add_argument("--xmax", type=float, default=1.0,
        help="Include region with x <= xmax (default:  1)")
parser.add_argument("--xix", type=float, default=0.2,
        help="Stretch factor for the grid in the x-direction (default: 0.2)")
parser.add_argument("--nlinx", type=int, default=40,
        help="Number of grid points in the linear portion of the x-grid (default: 40)")
parser.add_argument("--nlogx", type=int, default=80,
        help="Number of grid points in the log portion of the x-grid (default: 80)")
parser.add_argument("--ymin", type=float, default=-1.0,
        help="Include region with y >= ymin (default: -1)")
parser.add_argument("--ymax", type=float, default=1.0,
        help="Include region with y <= ymax (default:  1)")
parser.add_argument("--xiy", type=float, default=0.2,
        help="Stretch factor for the grid in the y-direction (default: 0.2)")
parser.add_argument("--nliny", type=int, default=40,
        help="Number of grid points in the linear portion of the y-grid (default: 40)")
parser.add_argument("--nlogy", type=int, default=80,
        help="Number of grid points in the log portion of the y-grid (default: 80)")
parser.add_argument("--zmin", type=float, default=-1.0,
        help="Include region with z >= zmin (default: -1)")
parser.add_argument("--zmax", type=float, default=1.0,
        help="Include region with z <= zmax (default:  1)")
parser.add_argument("--xiz", type=float, default=0.2,
        help="Stretch factor for the grid in the z-direction (default: 0.2)")
parser.add_argument("--nlinz", type=int, default=40,
        help="Number of grid points in the linear portion of the z-grid (default: 40)")
parser.add_argument("--nlogz", type=int, default=80,
        help="Number of grid points in the log portion of the z-grid (default: 80)")
args = parser.parse_args()

# Make sure arguemtns are consistent
if args.xix < 0 or args.xix > 1:
    raise argparse.ArgumentTypeError("The grid stretch factor has to be between 0 and 1")
if args.xiy < 0 or args.xiy > 1:
    raise argparse.ArgumentTypeError("The grid stretch factor has to be between 0 and 1")
if args.xiz < 0 or args.xiz > 1:
    raise argparse.ArgumentTypeError("The grid stretch factor has to be between 0 and 1")

# Locate files
print("Locating input files..."),
start_t = time.time()
flist = []
for var in args.vars:
    flist += locate(var + ".file_*.h5", root=args.dpath)
print("done! (%.2f sec)" % (time.time() - start_t))

# Read files
print("Parsing the metadata..."),
start_t = time.time()
dset = h5.dataset(flist)
vnames = list(sorted(set(dset.select_variables())))
print("done! (%.2f sec)" % (time.time() - start_t))

# Create output grid
print("Generating interpolation grid..."),
start_t = time.time()
xf = make_stretched_grid(args.xmin, args.xmax, args.xix, args.nlinx, args.nlogx)
yf = make_stretched_grid(args.ymin, args.ymax, args.xiy, args.nliny, args.nlogy)
zf = make_stretched_grid(args.zmin, args.zmax, args.xiz, args.nlinz, args.nlogz)
xc = 0.5*(xf[:-1] + xf[1:])
yc = 0.5*(yf[:-1] + yf[1:])
zc = 0.5*(zf[:-1] + zf[1:])
xc, yc, zc = np.meshgrid(xc, yc, zc, indexing='xy')
if args.reflecting_xy:
    xi = np.column_stack([xc.flatten(), yc.flatten(), np.abs(zc).flatten()])
else:
    xi = np.column_stack([xc.flatten(), yc.flatten(), zc.flatten()])
print("done! (%.2f sec)" % (time.time() - start_t))

# Create output folder
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)

# Process all iterations
for it in dset.iterations:
    start_t = time.time()
    ofname = str(args.outdir) + "/iter_" + str(it).zfill(10)
    if os.path.isfile(ofname):
        continue
    print("Processing iteration {}...".format(it)),

    grid = dset.get_grid(iteration=it)
    celldata = {}
    for vname in vnames:
        rawdata = dset.get_grid_data(grid, iteration=it,
                variable=vname, dtype=np.float32)
        interp = Interpolator(grid, rawdata, interp=1)
        celldata[str(vname)] = interp(xi).reshape(xc.shape)
        del rawdata

    gridToVTK(ofname, xf, yf, zf, cellData = celldata)
    print("done! (%.2f sec)" % (time.time() - start_t))
print("All done!")
