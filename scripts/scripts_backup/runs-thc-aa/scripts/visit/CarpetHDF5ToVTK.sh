PYTHON=python
DELTA=0.2
LEN=100.0

${PYTHON} -u \
	/global/homes/a/aup1075/runs-thc-aa/scripts/visit/CarpetHDF5ToVTK.py \
	--xmin=-${LEN} --xmax=${LEN} --nlinx=40 --nlogx=80 \
	--ymin=-${LEN} --ymax=${LEN} --nliny=40 --nlogy=80 \
	--zmin=-${LEN} --zmax=${LEN} --nlinz=40 --nlogz=80 \
	-v rho -v alp -v Y_e -v temperature \
	-o vtk
