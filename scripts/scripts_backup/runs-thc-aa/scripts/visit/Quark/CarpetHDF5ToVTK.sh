PYTHON=python
DELTA=0.2
LEN=20.0

${PYTHON} -u \
	/global/homes/a/aup1075/runs-thc-aa/scripts/visit/Quark/CarpetHDF5ToVTK.py \
	--xmin=-${LEN} --xmax=${LEN} --nlinx=80 --nlogx=80 \
	--ymin=-${LEN} --ymax=${LEN} --nliny=80 --nlogy=80 \
	--zmin=-${LEN} --zmax=${LEN} --nlinz=80 --nlogz=80 \
	--reflecting_xy \
	-v rho -v alp -v Y_e -v temperature \
	-o vtk
