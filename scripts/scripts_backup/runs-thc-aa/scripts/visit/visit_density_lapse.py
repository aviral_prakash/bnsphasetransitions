import sys
import os
from glob import glob
import os
import re
from visit_utils import *



# Options
log_rho_min = 7.
log_rho_max = 15.
log_rho_peaks_pos = [8., 10., 11., 12., 14., 15.]
log_rho_peaks_width = [0.025, 0.025, 0.025, 0.025, 0.025, 0.1]
log_rho_peaks_height = [0.05, 0.05, 0.1, 0.2, 0.4, 0.8]
log_rho_peaks_tmin = [14., 0., 0., 0., 0., 0.]
log_rho_peaks_dt = [1., 1., 1., 1., 1., 1.]
alp_displace = 30.0
it_to_time = 2.4/256.
outdir = "vis"



# Initialize stuff
TextObj = None
u_time = 1.0/203.0129158711296
if not os.path.isdir(outdir):
    os.mkdir(outdir)



# Main loop
for snapshot in sorted(glob("vtk/*.vtr")):
    # Read data
    itern = re.match("vtk/iter_(\d+).vtr", snapshot).group(1)
    time = int(itern) * it_to_time * u_time
    fname = outdir + "/iter_{}.png".format(itern)
    if os.path.isfile(fname):
        continue

    OpenDatabase(snapshot)

    DefineScalarExpression("lapse", "ADMBASE\:\:alp")
    DefineScalarExpression("rho", "HYDROBASE\:\:rho")
    DefineScalarExpression("elevation", "50.0*(lapse - 1.0)")
    DefineScalarExpression("dens", "6.176269145886162e17*rho")
    DefineScalarExpression("logdens", "log10(dens)")



    # Lapse pseudo-color / elevate
    AddPlot("Pseudocolor", "lapse")
    PColorAtts = PseudocolorAttributes()
    PColorAtts.legendFlag = 0
    PColorAtts.colorTableName = "Accent"
    PColorAtts.invertColorTable = 0
    PColorAtts.minFlag = 1
    PColorAtts.min = 0.0
    PColorAtts.maxFlag = 1
    PColorAtts.max = 1.0
    PColorAtts.smoothingLevel = 2
    SetPlotOptions(PColorAtts)





    # Volume rendering of density
    AddPlot("Volume", "dens", 0, 0)
    VolumeAtts = VolumeAttributes()
    VolumeAtts.samplesPerRay = 1500 #standard 500, need to use 1500
    VolumeAtts.rendererType = VolumeAtts.RayCasting  # Splatting, Texture3D, RayCasting, RayCastingIntegration
    VolumeAtts.smoothData = 1
    VolumeAtts.resampleFlag = 1
    VolumeAtts.resampleTarget = 50000
    VolumeAtts.lightingFlag = 1

    VolumeAtts.useColorVarMin = 1
    VolumeAtts.colorVarMin = 10.0**log_rho_min
    VolumeAtts.useColorVarMax = 1
    VolumeAtts.colorVarMax = 10.0**log_rho_max
    VolumeAtts.scaling = VolumeAtts.Log
    VolumeAtts.colorControlPoints.ClearControlPoints()
    p = ColorControlPoint()
    p.colors = (71, 71, 219, 255)
    p.position = 0.0
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (0, 0, 91, 255)
    p.position = 0.2
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (0, 255, 255, 255)
    p.position = 0.35
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (0, 127, 0, 255)
    p.position = 0.5
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (255, 255, 0, 255)
    p.position = 0.65
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (255, 96, 0, 255)
    p.position = 0.8
    VolumeAtts.colorControlPoints.AddControlPoints(p)
    p = ColorControlPoint()
    p.colors = (107, 0, 0, 255)
    p.position = 1.0
    VolumeAtts.colorControlPoints.AddControlPoints(p)

    VolumeAtts.opacityMode = VolumeAtts.GaussianMode  # FreeformMode, GaussianMode, ColorTableMode
    VolumeAtts.opacityVariable = "default"
    VolumeAtts.useOpacityVarMin = 1
    VolumeAtts.opacityVarMin = 10.0**log_rho_min
    VolumeAtts.useOpacityVarMax = 1
    VolumeAtts.opacityVarMax = 10.0**log_rho_max

    VolumeAtts.opacityControlPoints.ClearControlPoints()
    for i in range(len(log_rho_peaks_pos)):
        p = GaussianControlPoint()
        p.x = (log_rho_peaks_pos[i] - log_rho_min)/(log_rho_max - log_rho_min)
        p.width = log_rho_peaks_width[i]
        if time >= log_rho_peaks_tmin[i]:
            p.height = log_rho_peaks_height[i]
        elif time < log_rho_peaks_tmin[i] - log_rho_peaks_dt[i]:
            p.height = 0.0
        else:
            lam = (time - log_rho_peaks_tmin[i] + log_rho_peaks_dt[i])/log_rho_peaks_dt[i]
            p.height = log_rho_peaks_height[i]*lam
        VolumeAtts.opacityControlPoints.AddControlPoints(p)

    SetPlotOptions(VolumeAtts)



    # Lapse pseudo-color (proxy for BH horizon)
    AddPlot("Contour", "lapse")
    ContourAtts = ContourAttributes()
    ContourAtts.colorType = ContourAtts.ColorBySingleColor
    ContourAtts.legendFlag = 0
    ContourAtts.singleColor = (0, 0, 0, 255)
    ContourAtts.contourMethod = ContourAtts.Value
    ContourAtts.contourValue = (0.3)
    SetPlotOptions(ContourAtts)



    # Transformations
    SetActivePlots(0)

    AddOperator("Slice")
    SliceAtts = SliceAttributes()
    SliceAtts.axisType = SliceAtts.ZAxis
    SliceAtts.originIntercept = 0.0
    SetOperatorOptions(SliceAtts)

    AddOperator("Elevate")
    ElevateAtts = ElevateAttributes()
    ElevateAtts.variable = "elevation"
    SetOperatorOptions(ElevateAtts)

    AddOperator("Transform")
    TransformAtts = TransformAttributes()
    TransformAtts.doRotate = 0
    TransformAtts.doTranslate = 1
    TransformAtts.transformType = TransformAtts.Similarity  # Similarity, Coordinate, Linear
    TransformAtts.translateZ = -alp_displace
    SetOperatorOptions(TransformAtts)



    # Annotations
    AnnotationAtts = AnnotationAttributes()
    AnnotationAtts.backgroundColor = (66, 58, 53, 255)
    AnnotationAtts.axes3D.triadFlag = 0
    AnnotationAtts.axes3D.bboxFlag = 0
    AnnotationAtts.userInfoFlag = 0
    AnnotationAtts.databaseInfoFlag = 0
    AnnotationAtts.axes3D.visible = 0
    SetAnnotationAttributes(AnnotationAtts)

    pL = GetPlotList()
    legend = GetAnnotationObject(pL.GetPlots(1).plotName)
    legend.xScale = 1.4
    legend.yScale = 1.4
    legend.managePosition = 0
    legend.position = (0.02,0.98)
    legend.drawTitle = 0
    legend.useForegroundForTextColor = 0
    legend.drawMinMax = 0
    legend.numberFormat = "%1.1e"
    legend.textColor = (255, 255, 255, 255)


    MetaData = GetMetaData(GetWindowInformation().activeSource)
    if TextObj is None:
        TextObj = CreateAnnotationObject("Text2D")
    TextObj.height = 0.025
    TextObj.position = (0.8, 0.95)
    TextObj.textColor = (255, 255, 255, 255)
    TextObj.useForegroundForTextColor = 0
    TextObj.text = "t = %5.3f ms" % (time)



    # Set view
    DrawPlots()

    View3DAtts = View3DAttributes()
    View3DAtts.viewNormal = (1, 1, 1)
    View3DAtts.viewUp = (0, 0, 1)
    View3DAtts.focus = (0, 0, -10)
    View3DAtts.parallelScale = 40.0
    View3DAtts.nearPlane = -100.
    View3DAtts.farPlane = 100.
    View3DAtts.imageZoom = 1.0
    SetView3D(View3DAtts)
    DrawPlots()



    # Save the plots
    SaveWindowAtts = SaveWindowAttributes()
    SaveWindowAtts.outputToCurrentDirectory = 0
    SaveWindowAtts.outputDirectory = "."
    SaveWindowAtts.family = 0
    SaveWindowAtts.width = 1*1024
    SaveWindowAtts.height = 1*768
    SaveWindowAtts.resConstraint = SaveWindowAtts.NoConstraint
    SaveWindowAtts.screenCapture = 0
    SaveWindowAtts.fileName = fname
    SetSaveWindowAttributes(SaveWindowAtts)

    SaveWindow()

    DeleteActivePlots()
    CloseDatabase(snapshot)
sys.exit(0)
