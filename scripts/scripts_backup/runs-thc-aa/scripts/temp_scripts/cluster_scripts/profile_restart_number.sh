#!/bin/bash

prefix="./"
suffix="/data/mp_Psi4_l2_m2_r600.00.asc"


input="./profile_times.txt"
output="./profile_outputs.txt"

while IFS='' read -r var
do
  echo $var
  out=$(grep -Ril "$var" ./output-0???/data/mp_Psi4_l2_m2_r600.00.asc)
  out=${out#$prefix}
  out=${out%$suffix}
  echo $out >> $output
done < "$input"