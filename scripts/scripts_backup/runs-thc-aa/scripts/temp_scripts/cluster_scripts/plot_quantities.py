import os
import numpy as np
import matplotlib.pyplot as plt
import sys

sim_path = sys.argv[1]
list_quant = sys.argv[2]
list_quant = l.split(' ')

list_sim = os.listdir(sim_path)

for s in list_sim:
	list_out = os.listdir(sim_path + '/' + s + '/')
	list_out = [l for l in list_out if l.__contains__('output') and not l.__contains__('tar')]
	#for o in list_out:
	os.mkdir(sim_path + '/' + s + '/viz')
	for q in list_quant:
		os.system('cat ' sim_path + '/' + s + '/output-????/data/' + q ' >> /viz/' + q)	
		data = np.genfromtxt(sim_path + '/' + s + '/viz/' + q)
		t = data[:,1]
		dat = data[:,2]
		plt.plot(t, dat)
		plt.xlabel(r'$t$')
		plt.ylabel(r'$%s$' % (q.split('.asc')[0]))
		plt.savefig(sim_path + '/' + s + '/viz/' + q.split('.asc')[0] + '.png')
		
	
