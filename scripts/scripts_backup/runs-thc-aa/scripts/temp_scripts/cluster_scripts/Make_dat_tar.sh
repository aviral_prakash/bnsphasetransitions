#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 segment"
    exit 1
fi
target=$1

tar cf ${target}.dat.tar ${target} --exclude ${target}/checkpoint || exit 1
touch ${target}.dat.tar.done
