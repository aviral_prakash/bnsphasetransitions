#!/usr/bin/env python
#
# This scripts creates an XDMF file from a 3D profile.
#
# Known issues:
# * The x and z axis are swapped. There seems to be no way to fix this without
#   transposing the data in the HDF5 file.

import h5py
import numpy as np
import os
import re
import sys

XDMF_OPENING = """\
<?xml version="1.0" ?>
<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
<Xdmf Version="2.0">
  <Domain>
    <Grid Name="AMR Grid" GridType="Collection">
"""

XDMF_NEW_REFLEVEL = """\
      <Grid Name="reflevel={rlevel}" GridType="Uniform">
        <Time Value="{time}"/>
        <Topology TopologyType="3DRECTMesh" NumberOfElements="{nvx} {nvy} {nvz}"/>
        <Geometry GeometryType="ORIGIN_DXDYDZ">
          <DataItem Name="Origin" Dimensions="3" NumberType="Float" Precision="4" Format="XML">
            {z0} {y0} {x0}
          </DataItem>
          <DataItem Name="Spacing" Dimensions="3" NumberType="Float" Precision="4" Format="XML">
            {dz} {dy} {dx}
          </DataItem>
        </Geometry>
"""

XDMF_NEW_DATASET = """\
        <Attribute Name="{vname}" AttributeType="Scalar" Center="Cell">
          <DataItem Dimensions="{ncx} {ncy} {ncz}" NumberType="Float" Precision="4" Format="HDF">
            {profile}:/reflevel={rlevel}/{vname}
          </DataItem>
        </Attribute>
"""

XDMF_CLOSE_REFLEVEL = """\
      </Grid>
"""

XDMF_CLOSING = """\
   </Grid>
  </Domain>
</Xdmf>
"""

if len(sys.argv) != 2 or not os.path.isfile(sys.argv[1]):
  print("Usage: {} /path/to/profile.h5".format(sys.argv[0]))
  exit(0)
pfname = sys.argv[1]
dfile = h5py.File(pfname, "r")

xdmf = open(pfname.replace(".h5", "") + ".xmf", "w")
xdmf.write(XDMF_OPENING)

rlevels = []
for gname in dfile.keys():
  match = re.match("reflevel=(\d+)", gname)
  if match is None:
    continue
  else:
    rlevels.append(int(match.group(1)))
rlevels = list(sorted(rlevels))

for rl in rlevels[:-1]:
  gname = "reflevel={}".format(rl)
  gsubs = {}
  gsubs["rlevel"] = rl 

  gsubs["time"] = float(dfile[gname].attrs["time"])

  delta = np.array(dfile[gname].attrs["delta"])
  gsubs["dx"] = delta[0]
  gsubs["dy"] = delta[1]
  gsubs["dz"] = delta[2]

  extent = np.array(dfile[gname].attrs["extent"])
  gsubs["x0"] = extent[0] - 0.5*delta[0]
  gsubs["y0"] = extent[2] - 0.5*delta[1]
  gsubs["z0"] = extent[4] - 0.5*delta[2]

  ncx, ncy, ncz = dfile[gname][dfile[gname].keys()[0]].shape
  gsubs["nvx"] = ncx + 1
  gsubs["nvy"] = ncy + 1
  gsubs["nvz"] = ncz + 1

  xdmf.write(XDMF_NEW_REFLEVEL.format(**gsubs))
  for vname in dfile[gname].keys():
    vsubs = {}
    vsubs["rlevel"] = gsubs["rlevel"]
    vsubs["vname"] = vname
    vsubs["ncx"], vsubs["ncy"], vsubs["ncz"] = dfile[gname][vname].shape
    vsubs["profile"] = pfname

    xdmf.write(XDMF_NEW_DATASET.format(**vsubs))
  xdmf.write(XDMF_CLOSE_REFLEVEL)
xdmf.write(XDMF_CLOSING)

