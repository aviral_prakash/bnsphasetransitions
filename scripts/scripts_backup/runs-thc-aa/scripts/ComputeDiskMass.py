#!/usr/local/src/anaconda2/bin/python

import sys
sys.path.insert(0,"/global/homes/a/aup1075/scidata/scidata")

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import argparse
from glob import glob
import numpy as np
import os
import scidata.carpet.hdf5 as h5
import scidata.units as units
import time
#import units

# Hardcoded options: histograms ranges
rho_edges = 10.0**np.linspace(4.0, 16.0, 120)
temp_edges = 10.0**np.linspace(-2, 2, 50)
Ye_edges = np.linspace(0, 0.5, 50)

# Parse CLI
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", metavar=".", required=True,
        help="Path to the folder containing the 3D CarpetIOHDF5 data")
parser.add_argument("-o", "--outdir", default="Disk",
        help="Path to the output data folder")
parser.add_argument("-r", "--reflevel", type=int, default=6,
        help="Refinement level to analyze")
parser.add_argument("-w", "--weight", type=float, default=1.0,
        help="Multiply mass by this factor (to account for symmetries)")
parser.add_argument("-t", "--threshold", type=float, default=1.e13,
        help="Upper limit for the disk density")
args = parser.parse_args()

# Prepare output dir
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)

# Locate files
print("Locating input files..."),
start_t = time.time()
flist  = glob("%s/output-????/data/rho.file*.h5" % args.input)
flist += glob("%s/output-????/data/temperature.file*.h5" % args.input)
flist += glob("%s/output-????/data/Y_e.file*.h5" % args.input)
flist += glob("%s/output-????/data/volform.file*.h5" % args.input)
flist += glob("%s/output-????/data/w_lorentz.file*.h5" % args.input)
flist += glob("%s/output-????/data/alp.file*.h5" % args.input)
print("done! (%.2f sec)" % (time.time() - start_t))
assert(len(flist) > 0)

# Read the metadata
print("Parsing the metadata..."),
start_t = time.time()
dset = h5.dataset(flist)
print("done! (%.2f sec)" % (time.time() - start_t))

# --- --- ---
#flist = glob(args.input + "/rho.file_1.h5")
#tst = h5.dataset(flist)
#print(tst.metadata)

#print("iterations: {}".format(dset.get_time(dset.iterations[2]))) # ALLOWS YOU TO GET TIME OF THE ITERATION

# rlevel = dset.get_reflevel(iteration=dset.iterations[0], reflevel=2)

# print("rlevel: {}".format(rlevel))

# rho = tst.get_reflevel_data(rlevel, iteration=dset.iterations[-1],
#           variable="HYDROBASE::rho")
# print(rho)


# sys.exit(1)
# --- --- ---

# Analyze the data

it_dmass = np.empty(0,)

for it in dset.iterations:

    print("Processing iteration %d...\n" % it),
    rlevel = dset.get_reflevel(iteration=it, reflevel=args.reflevel)
    rho = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::rho")
    temp = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::temperature")
    Ye = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::Y_e")
    vol = dset.get_reflevel_data(rlevel, iteration=it,
            variable="THC_CORE::volform")
    W = dset.get_reflevel_data(rlevel, iteration=it,
            variable="HYDROBASE::w_lorentz")
    alp = dset.get_reflevel_data(rlevel, iteration = it, variable = "ADMBASE::alp")

    dens = rho*W*vol
    del W
    del vol

    # get rid of ghost data
    rho = rho[3:-3,3:-3,3:-3]
    temp = temp[3:-3,3:-3,3:-3]
    Ye = Ye[3:-3,3:-3,3:-3]
    dens = dens[3:-3,3:-3,3:-3]
    print('dens.shape: {}'.format(dens.shape))

    # convert rho to g/cm^3
    
    rho *= 6.176269145886162e+17

    rho_mask = 1.e13
    mask1 = rho < rho_mask
    mass_disk = args.weight*sum(dens[mask1])*np.prod(rlevel.delta)

    print("disk mass: (%.6f Msun)" % mass_disk)

    print("done! (%.2f sec)" % (time.time() - start_t))

    timestep = dset.get_time(it)
    it_dmass = np.append(it_dmass, [it, timestep, mass_disk])

print(it_dmass.shape)

it_dmass_reshaped = np.reshape(it_dmass, (len(dset.iterations), 3))

np.savetxt(args.outdir + '/disk_mass.asc', it_dmass_reshaped, '%.6f', '  ', '\n',
                   '\n rho_mask: {} , reflevel:{}  '
                   .format(rho_mask, args.reflevel), '', '# 1:iteration 2:time 3:data')
# with open('disk_mass.dat', 'wb') as f:
#     np.savetxt(f, it_dmass_reshaped)

