#!/usr/bin/env python

from __future__ import division, print_function

import argparse
import matplotlib as mpl
mpl.use('Agg')
from matplotlib.colors import LogNorm, Normalize
import matplotlib.pyplot as plt
import numpy as np
import os
import scidata.gizmo as gizmo
from scidata.utils import locate
import scidata.units as ut
import sys


# Conversion units
udens = ut.conv_dens(ut.cactus, ut.cgs, 1)       # g/cc
utime = ut.conv_time(ut.cactus, ut.cgs, 1)*1e3   # ms


# Parse CLI
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", default=".", help="Path to the tracer data")
parser.add_argument("-o", "--outdir", default="tracers", help="Output folder")
parser.add_argument("--cmap", default="jet", help="Color map")

parser.add_argument("--log-rho", dest="log_rho", action='store_true',
        help="Use log scale for density")
parser.add_argument("--rho-min", dest="rho_min", type=float, default=1e4,
        help="Minimum density (default: 10^4 g/cc)")
parser.add_argument("--rho-max", dest="rho_max", type=float, default=2e15,
        help="Maximum density (default: 10^15 g/cc)")
parser.add_argument("--nrho", type=int, default=50,
        help="Number of bins in density (default: 50)")

parser.add_argument("--log-temp", dest="log_temp", action='store_true',
        help="Use log scale for temperature")
parser.add_argument("--temp-min", dest="temp_min", type=float, default=1e-2,
        help="Minimum temperature (default: 0.01 MeV)")
parser.add_argument("--temp-max", dest="temp_max", type=float, default=1e2,
        help="Maximum temperature (default: 100 MeV)")
parser.add_argument("--ntemp", type=int, default=50,
        help="Number of bins in temperature (default: 50)")

parser.add_argument("--entr-min", dest="entr_min", type=float, default=0.0,
        help="Minimum entropy (default: 0)")
parser.add_argument("--entr-max", dest="entr_max", type=float, default=10.0,
        help="Maximum entropy (default: 10)")
parser.add_argument("--nentr", type=int, default=50,
        help="Number of bins in entropy (default: 50)")

parser.add_argument("--ye-min", dest="ye_min", type=float, default=0.0,
        help="Minimum Ye (default: 0)")
parser.add_argument("--ye-max", dest="ye_max", type=float, default=0.5,
        help="Maximum Ye (default: 0.5)")
parser.add_argument("--nye", type=int, default=25,
        help="Number of bins in Ye (default: 25)")

parser.add_argument("--mass-min", dest="mass_min", type=float, default=1e-5,
        help="Minimum scale for the histogram mass")
parser.add_argument("--mass-max", dest="mass_max", type=float, default=1e-1,
        help="Maximum scale for the histogram mass")

args = parser.parse_args()


# Locate data
dset = gizmo.dataset(locate("tracers.*.hdf5", root=args.input))
if len(dset.iterations) == 0:
    sys.exit("No tracer data found in \"{}\"".format(args.input))


# Create output folders
if not os.path.isdir(args.outdir):
    os.mkdir(args.outdir)
for subdir in ["temp", "entr", "ye"]:
    dpath = os.path.join(args.outdir, subdir)
    if not os.path.isdir(dpath):
        os.mkdir(dpath)


# Prepare histogram bins
if args.log_rho:
    rhof = np.linspace(np.log10(args.rho_min), np.log10(args.rho_max), args.nrho+1)
else:
    rhof = np.linspace(args.rho_min, args.rho_max, args.nrho+1)/1e15
if args.log_temp:
    tempf = np.linspace(np.log10(args.temp_min), np.log10(args.temp_max), args.ntemp+1)
else:
    tempf = np.linspace(args.temp_min, args.temp_max, args.ntemp+1)
entrf = np.linspace(args.entr_min, args.entr_max, args.nentr+1)
yef = np.linspace(args.ye_min, args.ye_max, args.nye+1)


# Read tracer masses (the factor 2 includes the reflection symmetry)
mass = dset.get_field_data(name="Masses")*2



# Loop over iterations and plot histograms 
itfill = len(str(dset.iterations[-1]))
for idx, it in enumerate(dset.iterations):
    print("Processing iteration {}/{}...".format(idx, len(dset.iterations)), end='')
    time = dset.get_time(it)*utime
    norm = LogNorm(args.mass_min, args.mass_max)
    
    rho = dset.get_field_data(it, name="Density")*udens
    if args.log_rho:
        rho = np.log10(rho)
    else:
        rho /= 1e15
    temp = dset.get_field_data(it, name="Temperature")
    if args.log_temp:
        temp = np.log10(temp)
    entr = dset.get_field_data(it, name="Entropy")
    ye = dset.get_field_data(it, name="Ye")

    fname = str(it).zfill(itfill) + ".png"

    ax = plt.axes()
    ax.set_title(r"$t = {:.3f}\ [{{\rm ms}}]$".format(time))
    _, _, _, im = ax.hist2d(rho, temp, bins=(rhof, tempf),
            weights=mass, norm=norm, cmap=args.cmap) 
    cbar = plt.colorbar(im)
    if args.log_rho:
        ax.set_xlabel(r"$\log \rho\ [{\rm g}\ {\rm cm}^{-3}]$")
    else:
        ax.set_xlabel(r"$\rho\ [10^{15}\ {\rm g}\ {\rm cm}^{-3}]$")
    if args.log_temp:
        ax.set_ylabel(r"$\log T\ [{\rm MeV}]$")
    else:
        ax.set_ylabel(r"$T\ [{\rm MeV}]$")
    cbar.set_label(r"$M\ [M_\odot]$")
    plt.savefig(os.path.join(args.outdir, "temp", fname))
    plt.close()

    ax = plt.axes()
    ax.set_title(r"$t = {:.3f}\ [{{\rm ms}}]$".format(time))
    _, _, _, im = ax.hist2d(rho, entr, bins=(rhof, entrf),
            weights=mass, norm=norm, cmap=args.cmap) 
    cbar = plt.colorbar(im)
    if args.log_rho:
        ax.set_xlabel(r"$\log \rho\ [{\rm g}\ {\rm cm}^{-3}]$")
    else:
        ax.set_xlabel(r"$\rho\ [10^{15}\ {\rm g}\ {\rm cm}^{-3}]$")
    ax.set_ylabel(r"$S\ [k_{\rm B}]$")
    cbar.set_label(r"$M\ [M_\odot]$")
    plt.savefig(os.path.join(args.outdir, "entr", fname))
    plt.close()

    ax = plt.axes()
    ax.set_title(r"$t = {:.3f}\ [{{\rm ms}}]$".format(time))
    _, _, _, im = ax.hist2d(rho, ye, bins=(rhof, yef),
            weights=mass, norm=norm, cmap=args.cmap) 
    cbar = plt.colorbar(im)
    if args.log_rho:
        ax.set_xlabel(r"$\log \rho\ [{\rm g}\ {\rm cm}^{-3}]$")
    else:
        ax.set_xlabel(r"$\rho\ [10^{15}\ {\rm g}\ {\rm cm}^{-3}]$")
    ax.set_ylabel(r"$Y_e$")
    cbar.set_label(r"$M\ [M_\odot]$")
    plt.savefig(os.path.join(args.outdir, "ye", fname))
    plt.close()

    print("done!")
