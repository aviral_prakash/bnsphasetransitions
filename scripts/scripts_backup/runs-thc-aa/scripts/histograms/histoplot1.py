#!/usr/bin/env python

""" Plot histogram from numpy npz file """

import os
import sys
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from collections import Counter

import units # units.py
from profiles2hist import default_setup # profiles2hist.py

def plot_hist(Z, xrng,nx, yrng,ny, xl,yl,time, fname, VMIN=1e-8,VMAX=1.):
    """ Histogram plot plot routine """
    Z = np.nan_to_num(Z)
    nor = np.nansum(Z)
    print('total',nor)
    print('max',np.nanmax(Z))
    print('min',np.nanmin(Z))
    x = 10.**np.linspace(xrng[0],xrng[1],nx)
    y = 10.**np.linspace(yrng[0],yrng[1],ny)
    X, Y = np.meshgrid(x, y)
    fig = plt.figure()  
    # colormap
    plt.pcolor(X,Y,(Z/nor).T,norm=LogNorm(vmin=VMIN, vmax=VMAX), cmap='jet' )
    plt.xlabel(xl)
    plt.ylabel(yl)
    plt.title("Time (ms) = "+str(time * 1e3))
    plt.colorbar()
    plt.xscale('log')
    plt.yscale('log')
    plt.savefig(fname+".png") 
    #plt.clf()
    #plt.show()
    return

def load_npz(filename, outputdir, use_edges_file=0, rm_tmp_file=1):
    """ Load and check *.npz """
    based, fname = os.path.split(filename)
    name, ext = fname.rsplit(".",1)
    if not ext == "npz":
        print("Need *.npz file")
        return sys.exit()
    npzfile = np.load(filename)
    # Field names of known data
    fieldn2 = ['dens_temp', 'setup', 'temp_ye', 'dens_temp_ye','dens_ye','time']
    fieldn3 = ['histogram','time']
    if Counter(npzfile.files) == Counter(fieldn2):
        # 2D data from profiles2hist.py
        return npzfile, npzfile['setup'][()]
    if Counter(npzfile.files) == Counter(fieldn3):
        # 3D data from David postprocessing ...
        # ... convert to 2D data
        hist3D = npzfile['histogram']
        hist2D_dt  = np.sum(hist3D,axis=2) 
        hist2D_dye = np.sum(hist3D,axis=1)         
        hist2D_tye = np.sum(hist3D,axis=0)
        # Setup info (Set/overwrite dimension)
        if use_edges_file:
            s = info_from_edges(based+"/edges.npz")
        else:
            s = default_setup()
        s["n_dens"] = np.shape(npzfile['histogram'])[0]+1
        s["n_temp"] = np.shape(npzfile['histogram'])[1]+1
        s["n_ye"] = np.shape(npzfile['histogram'])[2]+1
        # Extract iteration from filename, e.g. histogram.r5.i279040.npz
        aux, it = name.rsplit(".",1)
        it = it[1:]
        time = npzfile['time']
        time_cgs = units.conv_time(units.cactus,units.cgs,time)
        # Save a tmp file 
        np.savez(outputdir+'/'+ name+'_tmp', 
                 dens_temp_ye=hist3D,
                 dens_temp=hist2D_dt, 
                 dens_ye=hist2D_dye, 
                 temp_ye=hist2D_tye,
                 time=[int(it),time,time_cgs],
                 setup=s)        
        try:
            cnpzfile = np.load(outputdir+'/'+ name+'_tmp.npz')
        except:
            print("Cannot load "+outputdir+'/'+ name+'_tmp.npz')
            raise 
        if rm_tmp_file:
            # remove the temp file
            os.remove(outputdir+'/'+ name+'_tmp.npz')
        return cnpzfile, s
    # if you get here, data are unknown 
    print("Unknown data")
    raise 

def info_from_edges(fname):
    """  Read minimal info from 'edges.npz' file 
    use some default if not found
    """
    s = default_setup()
    if os.path.isfile(fname): 
        d = np.load(fname)
        if Counter(d.files) == Counter(['Ye','temp','rho']):
            s["n_ye"]=len(d['Ye'])
            s["n_temp"]=len(d['temp'])
            s["n_dens"]=len(d['rho'])
            s["ye_max"]=np.amax(d['Ye']) 
            s["ye_min"]=np.amin(d['Ye'])
            s["temp_max"]=np.amax(d['temp']) 
            s["temp_min"]=np.amin(d['temp']) 
            s["dens_max"]=np.amax(d['rho']) 
            s["dens_min"]=np.amin(d['rho']) 
    return s

def load_and_plot(filename, outputdir):
    """ Load *.npz data from 'filename' and output a histogram """
    # Load data 
    #
    # based, fname = os.path.split(filename)
    # name, ext = fname.split(".")
    # if not ext == "npz":
    #     return("Need *.npz file")
    # npzfile = np.load(filename)
    #
    try:
        npzfile, s = load_npz(filename, outputdir)    
    except:
        return "problem loading "+filename
    time = npzfile['time'][2]
    iter_stamp = "%09d" % npzfile['time'][0]
    # Set plotting options
    t = {}
    # -----------------------------------------------------
    t['dens_temp'] = {}
    t['dens_temp']['xl'] = r'Density ${\rm [g/cm^3]}$'
    t['dens_temp']['yl'] = r'Temperature ${\rm [MeV]}$'
    t['dens_temp']['xrng'] = [np.log10(s["dens_min"]),np.log10(s["dens_max"])]
    t['dens_temp']['yrng'] = [np.log10(s["temp_min"]),np.log10(s["temp_max"])]
    #t['dens_temp']['xrng'] = [4.,16.]
    #t['dens_temp']['yrng'] = [-2.,2.]
    t['dens_temp']['nx'] = s["n_dens"]
    t['dens_temp']['ny'] = s["n_temp"]
    t['dens_temp']['fname'] = "dens_temp"
    # -----------------------------------------------------
    t['dens_ye'] = {}
    t['dens_ye']['xl'] = r'Density ${\rm [g/cm^3]}$'
    t['dens_ye']['yl'] = r'Electron fraction'
    t['dens_ye']['xrng'] = [np.log10(s["dens_min"]),np.log10(s["dens_max"])]
    t['dens_ye']['yrng'] = [np.log10(s["ye_min"]),np.log10(s["ye_max"])]
    #t['dens_ye']['xrng'] = [4.,16.]
    #t['dens_ye']['yrng'] = [0.,0.64]
    t['dens_ye']['nx'] = s["n_dens"]
    t['dens_ye']['ny'] = s["n_ye"]
    t['dens_ye']['fname'] = "dens_ye"
    # -----------------------------------------------------
    t['temp_ye'] = {}
    t['temp_ye']['xl'] = r'Temperature ${\rm [MeV]}$'
    t['temp_ye']['yl'] = r'Electron fraction'
    t['temp_ye']['xrng'] = [np.log10(s["temp_min"]),np.log10(s["temp_max"])]
    t['temp_ye']['yrng'] = [np.log10(s["ye_min"]),np.log10(s["ye_max"])]
    #t['temp_ye']['xrng'] = [-2.,2.]
    #t['temp_ye']['yrng'] = [0.,0.64]
    t['temp_ye']['nx'] = s["n_temp"]
    t['temp_ye']['ny'] = s["n_ye"]
    t['temp_ye']['fname'] = "temp_ye"
    # Do it
    plot_type = ['dens_temp','dens_ye','temp_ye']
    for x in npzfile.files:
        if x in plot_type:
            print(t[x]['fname'])
            fname = outputdir+"/hist_"+t[x]['fname']+"_"+iter_stamp
            plot_hist(npzfile[x], 
                      t[x]['xrng'],t[x]['nx'],
                      t[x]['yrng'],t[x]['ny'],
                      t[x]['xl'],t[x]['yl'],
                      time, fname)
    return filename + "... done"

if __name__ == '__main__':

    parser = ArgumentParser(description="Plot histogram from numpy file")
    parser.add_argument("-i", dest="filename", 
                        required=True,
                        help="input numpy files (*.npz)", metavar="FILE")
    parser.add_argument("-o", dest="outputdir", 
                        nargs='?', default=os.getcwd(),
                        help="output directory") 
    args = parser.parse_args()    
    if not os.path.exists(args.outputdir):
        os.mkdir( args.outputdir, 0755 )
    load_and_plot(args.filename, args.outputdir)    

