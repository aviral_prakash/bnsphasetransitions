Date: Wed Sep 30 19:34:26 2020

===================================================================

Relativistic computation                           Irrotational
Star 1  EOS
EOS of class Eos_AkmalPR (Akmal, Pandharipande & Ravenhall 1998) : 
  composition :  n,p,e,mu 
  model : A18+dv+UIX* 

Omega = 1742.18 rad/s                Orbital frequency f = 277.276 Hz
Omega_kepler = 1911.94 rad/s
Coordinate separation : 45 km
1/2 ADM mass :        1.28819 Mo
Total angular momentum : 6.78789 G M_sol^2 / c

Number of steps : 154

===================================================================
       Star no. 1
===================================================================
Grid : 
------ 
Number of domains: 4
  Domain #0: nr = 33, RARE; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #1: nr = 33, FIN; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #2: nr = 33, FIN; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #3: nr = 33, UNSURR; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev



Physical characteristics : 
-------------------------

Relativistic star
-----------------
Number of domains occupied by the star : 1
Equation of state : 
Star 1  EOS
EOS of class Eos_AkmalPR (Akmal, Pandharipande & Ravenhall 1998) : 
  composition :  n,p,e,mu 
  model : A18+dv+UIX* 


Central enthalpy : 0.223183 c^2
Central proper baryon density : 4.67284 x 0.1 fm^-3
Central proper energy density : 5.12884 rho_nuc c^2
Central pressure : 0.712434 rho_nuc c^2

Regularization index of the gravitational potential : k_div = 0
Central lapse N :      0.623114
Central value of A^2 : 2.28763

Coordinate equatorial radius (phi=0) a1 =    9.96613 km
Coordinate equatorial radius (phi=pi/2) a2 = 9.71687 km
Coordinate equatorial radius (phi=pi):       9.99614 km
Coordinate polar radius a3 =                 9.75893 km
Axis ratio a2/a1 = 0.974989  a3/a1 = 0.97921

Baryon mass :        1.5362 M_sol
Gravitational mass : 1.3521 M_sol

Star in a binary system
-----------------------
irrotational configuration
Absolute abscidia of the stellar center: -20.7696 km
Absolute abscidia of the barycenter of the baryon density : -20.7643 km
d_tilde : 4.16478
Orientation with respect to the absolute frame : 0 rad
Central value of gam_euler : 1.00322
Central u_euler (U^X, U^Y, U^Z) [c] : 4.9177e-18  -0.0529258  0
Central d_psi (X, Y, Z) [c] :         1.41082e-17  -0.151836  0
Central vel. / co-orb. (W^X, W^Y, W^Z) [c] : 8.99325e-18  -5.75439e-05  0
Max vel. / co-orb. (W^X, W^Y, W^Z) [c] : 0.389538  0.550538  0.00261201
Min vel. / co-orb. (W^X, W^Y, W^Z) [c] : -0.389538  -0.212699  -0.00261201
Velocity at (r_surf,pi/4,pi/4) / co-orb. [c] : 0.0347304  -0.0334389  6.32774e-05
Central value of loggam : 3.86283e-09
Central value of log(N) auto, comp :         -0.43392  -0.0391057
Central value of beta=log(AN) auto, comp :   -0.0583834  -0.000884971
Central value of shift (N^X, N^Y, N^Z) [c] : -1.37866e-18  -0.0877546  0
  ... shift_auto part of it [c] :            1.18062e-18  -0.109607  0
  ... shift_comp part of it [c] :            0  0.0218521  0
  ... w_shift (NB: components in the star Cartesian frame) [c] :  
1.09558e-18  -0.118807  0
Central value of khi_shift [km c] : -1.78122e-17

Central value of (B^X, B^Y, B^Z)/N [c] : -2.21253e-18  0.0528688  0

Central (d/dX,d/dY,d/dZ)(logn_auto) [km^{-1}] : -0.0001429  -6.4718e-19  0
Central (d/dX,d/dY,d/dZ)(logn_comp) [km^{-1}] : -0.000897123  6.63675e-32  0

Central (d/dX,d/dY,d/dZ)(beta_auto) [km^{-1}] : -8.3298e-05  2.22305e-19  0
Central (d/dX,d/dY,d/dZ)(beta_comp) [km^{-1}] : -0.00010014  -3.44094e-33  0

Central A^2 K^{ij} [c/km] : 
  A^2 K^{xx} auto, comp : 3.38318e-15  -1.68387e-32
  A^2 K^{xy} auto, comp : -9.11402e-06  -0.000118099
  A^2 K^{xz} auto, comp : 0  0
  A^2 K^{yy} auto, comp : -6.76594e-15  1.84565e-32
  A^2 K^{yz} auto, comp : 0  0
  A^2 K^{zz} auto, comp : 3.38277e-15  -8.1356e-33

Central A^2 K_{ij} K^{ij} [c^2/km^2] : 
   A^2 K_{ij} K^{ij}  auto, comp : -4.32669e-10  4.9252e-09


===================================================================
       Star no. 2
===================================================================
Grid : 
------ 
Number of domains: 4
  Domain #0: nr = 33, RARE; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #1: nr = 33, FIN; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #2: nr = 33, FIN; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev
  Domain #3: nr = 33, UNSURR; nt = 25, SYM; np = 20, NONSYM; Collocation points type : Chebyshev



Physical characteristics : 
-------------------------

Relativistic star
-----------------
Number of domains occupied by the star : 1
Equation of state : 
Star 2  EOS
EOS of class Eos_AkmalPR (Akmal, Pandharipande & Ravenhall 1998) : 
  composition :  n,p,e,mu 
  model : A18+dv+UIX* 


Central enthalpy : 0.180479 c^2
Central proper baryon density : 4.07316 x 0.1 fm^-3
Central proper energy density : 4.39519 rho_nuc c^2
Central pressure : 0.483606 rho_nuc c^2

Regularization index of the gravitational potential : k_div = 0
Central lapse N :      0.668762
Central value of A^2 : 2.05338

Coordinate equatorial radius (phi=0) a1 =    10.4158 km
Coordinate equatorial radius (phi=pi/2) a2 = 9.99527 km
Coordinate equatorial radius (phi=pi):       10.4291 km
Coordinate polar radius a3 =                 10.0651 km
Axis ratio a2/a1 = 0.959623  a3/a1 = 0.966331

Baryon mass :        1.2958 M_sol
Gravitational mass : 1.15799 M_sol

Star in a binary system
-----------------------
irrotational configuration
Absolute abscidia of the stellar center: 24.2304 km
Absolute abscidia of the barycenter of the baryon density : 24.2235 km
d_tilde : 4.65091
Orientation with respect to the absolute frame : 3.14159 rad
Central value of gam_euler : 1.00537
Central u_euler (U^X, U^Y, U^Z) [c] : -2.05643e-18  0.072028  0
Central d_psi (X, Y, Z) [c] :         -5.08498e-18  0.178106  0
Central vel. / co-orb. (W^X, W^Y, W^Z) [c] : 7.48581e-18  9.47394e-05  0
Max vel. / co-orb. (W^X, W^Y, W^Z) [c] : 0.387075  0.187951  0.00259387
Min vel. / co-orb. (W^X, W^Y, W^Z) [c] : -0.387075  -0.577538  -0.00259387
Velocity at (r_surf,pi/4,pi/4) / co-orb. [c] : -0.0350254  0.0330022  7.77741e-05
Central value of loggam : 9.0207e-09
Central value of log(N) auto, comp :         -0.356897  -0.0454299
Central value of beta=log(AN) auto, comp :   -0.0417118  -0.000871509
Central value of shift (N^X, N^Y, N^Z) [c] : 5.74802e-18  0.0927022  0
  ... shift_auto part of it [c] :            2.0189e-19  0.114558  0
  ... shift_comp part of it [c] :            0  -0.0218557  0
  ... w_shift (NB: components in the star Cartesian frame) [c] :  
1.01431e-19  -0.124279  0
Central value of khi_shift [km c] : -4.02403e-17

Central value of (B^X, B^Y, B^Z)/N [c] : 8.59501e-18  -0.0719348  0

Central (d/dX,d/dY,d/dZ)(logn_auto) [km^{-1}] : 0.000134144  2.52984e-18  0
Central (d/dX,d/dY,d/dZ)(logn_comp) [km^{-1}] : 0.00103418  -1.19546e-31  0

Central (d/dX,d/dY,d/dZ)(beta_auto) [km^{-1}] : 7.48087e-05  3.21099e-19  0
Central (d/dX,d/dY,d/dZ)(beta_comp) [km^{-1}] : 9.91823e-05  1.11246e-32  0

Central A^2 K^{ij} [c/km] : 
  A^2 K^{xx} auto, comp : 3.28586e-16  -1.11639e-31
  A^2 K^{xy} auto, comp : -2.52398e-05  -0.000129715
  A^2 K^{xz} auto, comp : 0  0
  A^2 K^{yy} auto, comp : -6.48999e-16  1.40668e-31
  A^2 K^{yz} auto, comp : 0  0
  A^2 K^{zz} auto, comp : 3.20413e-16  -1.56862e-31

Central A^2 K_{ij} K^{ij} [c^2/km^2] : 
   A^2 K_{ij} K^{ij}  auto, comp : 3.23374e-09  1.34441e-08



===================================================================
Diff_ent :  star 1 : 9.3924e-08   star 2 : 1.15859e-07
Relative difference between the baryon masses of the two stars : -0.15649
dH/dx at r = 0 :  star 1 : 1.46444e-07   star 2 : -1.70771e-07
Relative error on the virial theorem : 
   VE(M)= -3.46752e-05   VE(GB)= 6.06039e-05   VE(FUS)= 7.63125e-05

================================================================
	    PARAMETERS USED FOR THE COMPUTATION : 
================================================================
# Parameters for the binary equilibrium computation by coal
###################################################################
ini.d
0.45     fact_separ : Factor by which the initial separation is multiplied
1.5362         mbar_voulue[0] : Baryon mass required for star 1 [M_sol]
1.2958         mbar_voulue[1] : Baryon mass required for star 2 [M_sol]
###################################################################
1000      mermax : Maximum number of steps in the main iteration
0.4       relax :  Relaxation factor in the main iteration
1 mermax_eqb : Maximum number of steps in Etoile_bin::equilibrium
1 prompt : 1 if no pause during the computation
0 graph : 1 if graphical outputs during the computation
1.E-7     seuil : Threshold on the enthalpy relative change for ending the computation
2 fmer_stop : Step interval between pauses in the main iteration
5 fmer_save : Step interval between safeguards of the whole configuration
4 mermax_poisson : Maximum number of steps in Map_et::poisson
1.5       relax_poisson :  Relaxation factor in Map_et::poisson
4         mermax_potvit : Maximum number of steps in Map_radial::poisson_compact
0.5       relax_potvit :  Relaxation factor in Map_radial::poisson_compact
20        mer_masse : Step from which the baryon mass is forced to converge
0.25      aexp_masse : Exponent for the increase factor of the central enthalpy
8 fmer_udp_met : Step interval between metric updates
1 ind_rel_met : 1 if relaxation of the metric, 0 if not
0.9       relax_met : Relaxation factor of the metric (used only if ind_rel_met=1)
0.75      relax_omeg : Relaxation factor on Omega (orbital angular velocity)
0.7       fact_omeg_min : fact_omeg_min * omega = low bound in the omega search
1.3       fact_omeg_max : fact_omeg_max * omega = high bound in the omega search
0.        thres_adapt1 : threshold on dH/dr for the adaptation of the mapping in star 1
0.        thres_adapt2 : threshold on dH/dr for the adaptation of the mapping in star 2
0.1       reduce_shift : factor by which the initial analytical shift is reduced
================================================================
	    IDENTIFICATION OF THE CODE : 
================================================================
coal:
     $Header: /cvsroot/Lorene/C++/Source/Non_class_members/Utilities/misc.C,v 1.2 2019/12/02 14:51:37 j_novak Exp $
     $Header: /cvsroot/Lorene/C++/Source/Tbl/dim_tbl.C,v 1.7 2014/10/13 08:53:41 j_novak Exp $
     $Header: /cvsroot/Lorene/C++/Source/Etoile/et_bin_nsbh_kinema.C,v 1.4 2014/10/13 08:52:56 j_novak Exp $
     $Header: /cvsroot/Lorene/C++/Source/Non_class_members/PDE/sol_elliptic_fixe_der_zero.C,v 1.5 2014/10/13 08:53:30 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/poisson2d.f,v 1.8 2013/09/04 14:12:10 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/gr2p3s.f,v 1.3 2014/03/26 10:44:19 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fciq3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/gr2p1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/poisson2di.f,v 1.8 2013/09/04 14:12:10 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fcir3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/city3s.f,v 1.3 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fcez3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fuce3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dircms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/leqt1s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/derfms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/citxms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cirx3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dir2ms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfmxs.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfmys.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfmzs.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/extm1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/prims.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cirxms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fcer3s.f,v 1.3 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cey23s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/gpar2s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/grgp2s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/extm2s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cheyms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/exrm1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd1mrs.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/extr1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/fuci3s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/pegpjs.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfiyms.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/divq1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cerx3s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chezms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfixms.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd2xms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfizms.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/ilgprs.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dimras.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dixr1s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chiyms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/ciy23s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/titi.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chexms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/disr3s.f,v 1.3 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chixms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cety3s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/ciy2ms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chizms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cey2ms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/glagos.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd2mrs.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/priq1s.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cetams.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd2zms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd2yms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chins.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/desfms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfins.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfm2s.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/fax.f,v 1.3 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd1zms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd1yms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfm3s.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dir1ms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfm1s.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cirams.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cd1xms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/dira2s.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/di2ras.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/ilggos.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/magl3s.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cerams.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/decfms.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/disems.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cheles.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/intras.f,v 1.2 2012/03/30 12:12:43 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cerars.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chi2ms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/tf9s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/cera2s.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/chinms.f,v 1.2 2012/03/30 12:12:42 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/tfinms.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $
     $Header: /cvsroot/Lorene/F77/Source/Poisson2d/lege1.f,v 1.2 2012/03/30 12:12:44 j_novak Exp $

================================================================
	    CPU TIME and MEMORY infos : 
================================================================

